export default {
  namespaced: true,
  state: () => ({
    // 当前选中的地址
    curAddress: {},
    // 所有地址列表
    addressList: []
  }),
  mutations: {
    // 选择地址
    setCurAddress (state, payload) {
      state.curAddress = { ...payload }
    },
    // 添加地址列表
    addList (state, payload) {
      const { province, city, county, addressDetail } = payload
      const address = (province === city ? city : province + city) + county + addressDetail
      state.addressList.push({
        ...payload,
        id: Date.now() + '',
        address
      })
    },
    // 编辑地址
    updateAddress (state, payload) {
      // 根据id查找要修改地址的下标
      const index = state.addressList.findIndex(v => v.id === payload.id)
      const { province, city, county, addressDetail } = payload
      const address = (province === city ? city : province + city) + county + addressDetail
      // 用传入的地址替换之前的地址
      state.addressList.splice(index, 1, { ...payload, address })
      // 如果更新的地址 id === 当前选中的地址id，更新当前选中的地址
      if (state.curAddress.id === payload.id) {
        state.curAddress = { ...payload, address }
      }
    },
    // 删除地址
    delAddress (state, id) {
      const index = state.addressList.findIndex(v => v.id === id)
      state.addressList.splice(index, 1)
      if (state.curAddress.id === id) {
        state.curAddress = {}
      }
    }
  }
}
