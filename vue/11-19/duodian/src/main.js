import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {
  Button,
  Tabbar,
  TabbarItem,
  NavBar,
  Icon,
  Search,
  AddressList,
  Toast,
  AddressEdit,
  Empty,
  Grid,
  GridItem,
  Image,
  Sticky,
  ContactCard,
  SubmitBar,
  CellGroup,
  Cell,
  Stepper,
  Dialog
} from 'vant'

Vue.use(Button)
Vue.use(Dialog)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(NavBar)
Vue.use(Icon)
Vue.use(Search)
Vue.use(AddressList)
Vue.use(Toast)
Vue.use(AddressEdit)
Vue.use(Empty)
Vue.use(Grid)
Vue.use(GridItem)
Vue.use(Image)
Vue.use(Sticky)
Vue.use(ContactCard)
Vue.use(SubmitBar)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Stepper)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
