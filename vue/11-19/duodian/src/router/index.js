import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Detail from '../views/detail/Detail'
import Address from '../views/address/Address'
import Create from '../views/create/Create'
import Login from '../views/login/Login'
import GoHome from '../views/home/gohome/Gohome'
import Cart from '../views/home/cart/Cart'
import Mine from '../views/home/mine/Mine'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/gohome',
    children: [
      {
        path: '/gohome',
        name: 'gohome',
        component: GoHome
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      },
      {
        path: '/mine',
        name: 'mine',
        component: Mine
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail
  },
  {
    path: '/address',
    name: 'address',
    component: Address
  },
  {
    path: '/create',
    name: 'create',
    component: Create
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
