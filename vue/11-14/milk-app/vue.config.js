const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      const zh = require('./data/zh.json')
      const xl = require('./data/xl.json')
      const sx = require('./data/sx.json')
      const data = { zh, xl, sx }
      devserver.app.get('/milk/list', (req, res) => {
        const { type } = req.query
        if (data[type]) {
          res.send({
            code: 0,
            msg: '成功',
            data: data[type]
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      // 搜索接口
      const dataAll = [...zh.items, ...xl.items, ...sx.items]
      let all = []
      dataAll.forEach(item => {
        if (!all.find(v => v.item_id === item.item_id)) {
          all.push(item)
        }
      })
      devserver.app.get('/milk/search', (req, res) => {
        const { keywords } = req.query
        if (keywords.trim() === '') {
          res.send({
            code: 0,
            msg: '成功',
            data: []
          })
          return
        }
        const list = all.filter(v => v.title.includes(keywords))
        res.send({
          code: 0,
          msg: '成功',
          data: list
        })
      })
      return middlewares
    }
  }
})
