import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Detail from '../views/Detail'
import Search from '../views/Search'
import List from '../views/List'
import NotFound from '../views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/list/zh',
    children: [
      {
        path: '/list/:id',
        name: 'list',
        component: List
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/404',
    component: NotFound
  },
  {
    path: '*',
    component: NotFound
  }
]

const router = new VueRouter({
  // mode: 'history' // 地址栏不带"#"，利用 h5 的新特性，history 对象中的 pushState 实现的
  // mode: 'hash' // 地址栏带"#"，利用 hashchange事件监听"#"后的参数变化展示对应的组件
  mode: 'history', // 路由模式（路由的实现原理）
  base: '', // 所有路由地址前的基础路径
  routes
})

export default router
