const express = require('express')
const path = require('path')
const fs = require('fs')
const Multiparty = require('multiparty') // 处理图片
const app = express()

app.use(express.static('./image'))

// 上传图片的接口
app.post('/api/upload', (req, res) => {
  // 实例化对象，接收前端传过来的图片
  const form = new Multiparty.Form();
  // 解析前端传过来的图片
  form.parse(req, function(err, fields, files) {
    // 前端传过来的文件
    console.log(files)
    // 读取图片的名字
    const originalFilename = files.file[0].originalFilename
    // 读取前端传过来的图片在服务器内存的地址
    const imgagePath = files.file[0].path
    // 把图片从内存中复制到 images 文件夹中
    const output = path.join(__dirname, './image', originalFilename)
    fs.copyFileSync(imgagePath, output)
    res.send({
      code: 200,
      msg: '成功',
      data: {
        url: `http://localhost:3000/${originalFilename}`
      }
    });
  });
})

app.listen(3000, () => {
  console.log('服务启动成功 http://localhost:3000');
})