const tab = [
  {
    title: '早餐',
    list: ['豆浆', '油条', '包子', '豆腐脑']
  },
  {
    title: '午餐',
    list: ['蛋炒饼', '西红柿鸡蛋面', '鱼香肉丝', '宫保鸡丁']
  },
  {
    title: '晚餐',
    list: ['火锅', '麻辣烫', '烧烤', '大盘鸡']
  }
]
console.log(tab)