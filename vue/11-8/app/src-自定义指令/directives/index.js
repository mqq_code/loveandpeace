import load from './load'
import test from './test'
import log from './log'
import border from './border'

const directives = {
  load,
  log,
  test,
  border
}

export default (Vue) => {
  // 注册所有指令
  Object.keys(directives).forEach(key => {
    Vue.directive(key, directives[key])
  })
}
