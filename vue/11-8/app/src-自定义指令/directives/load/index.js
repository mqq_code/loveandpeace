import loading from '../../assets/loading.gif'
import fail from '../../assets/fail.webp'

function load (el, value) {
  // 加载中
  el.src = loading
  el.parentNode.style.background = 'orange'
  // 加载图片
  const img = new Image()
  img.src = value
  img.onload = () => {
    setTimeout(() => {
      el.src = value
      el.parentNode.style.background = 'green'
    }, 2000)
  }
  // 加载失败
  img.onerror = () => {
    setTimeout(() => {
      el.src = fail
      el.parentNode.style.background = 'red'
    }, 2000)
  }
}

export default {
  bind (el, binding) { // 指令绑定到元素的时候执行，只执行一次，可以做初始化配置
    // console.log('bind 执行了')
    // console.log(el) // 使用指令的元素
    // console.log(binding.value) // 指令的数据
  },
  inserted (el, binding) { // 元素插入到父节点时执行，可以获取父节点
    // console.log('inserted 执行了')
    load(el, binding.value)
  },
  update (el, { value, oldValue }) { // 所在的组件数据更新时执行
    // console.log('update 执行了')
    if (value !== oldValue) {
      load(el, value)
    }
  },
  componentUpdated (el) { // 所在的组件页面刚更新完成之后执行
    // console.log('componentUpdated 执行了')
  },
  unbind () { // 所在的元素销毁时执行，清除异步任务
    // console.log('unbind 执行了')
  }
}
