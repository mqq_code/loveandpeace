import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
Vue.directive('copy', {
  bind (el, binding) {
    el.setAttribute('data-copy', binding.value)
    el.addEventListener('click', () => {
      const copyText = el.getAttribute('data-copy')
      const textarea = document.createElement('textarea')
      textarea.value = copyText
      document.body.appendChild(textarea)
      textarea.select()
      // 把当前页面选中的文本复制到剪切板
      document.execCommand('copy')
      document.body.removeChild(textarea)
      alert('复制成功')
    })
  },
  update (el, binding) {
    if (binding.oldValue !== binding.value) {
      el.setAttribute('data-copy', binding.value)
    }
  }
})

new Vue({
  render: h => h(App)
}).$mount('#app')
