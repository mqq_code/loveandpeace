import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {
  Button,
  Cell,
  CellGroup,
  Popup,
  Calendar,
  Search,
  Skeleton,
  Grid,
  GridItem,
  Image
} from 'vant'

// 按需加载
Vue.use(Button)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Popup)
Vue.use(Calendar)
Vue.use(Search)
Vue.use(Skeleton)
Vue.use(Grid)
Vue.use(GridItem)
Vue.use(Image)
// 完整引入，会增加代码体积
// import Vant from 'vant'
// import 'vant/lib/index.css'
// Vue.use(Vant)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
