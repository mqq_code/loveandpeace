import Vue from 'vue'
import Vuex from 'vuex'
import home from './modules/home'
import about from './modules/about'
import login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    rootTitle: '跟数组'
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    home,
    about,
    login
  }
})
