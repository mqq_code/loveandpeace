export default {
  // 命名空间
  namespaced: true,
  state: state => ({
    list: [1, 2, 3, 4, 5, 6, 7]
  }),
  mutations: {
    add (state) {
      console.log('home.js ========> add')
      const index = state.list.length - 1
      if (index === -1) {
        state.list.push(0)
      } else {
        state.list.push(state.list[index] + 1)
      }
    },
    removeList (state, index) {
      state.list.splice(index, 1)
    }
  },
  actions: {},
  getters: {}
}
