export default {
  state: state => ({
    username: '小小',
    age: 20,
    sex: '男'
  }),
  mutations: {
    random (state) {
      state.age = Math.floor(Math.random() * 90 + 11)
      state.sex = Math.random() >= 0.5 ? '男' : '女'
      state.username = '小小' + Math.random()
    }
  },
  actions: {},
  getters: {}
}
