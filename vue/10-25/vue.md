## Vue
### 核心：双向绑定，数据驱动视图改变

### 声明式渲染
```html
<div class="app">
  <h1>{{ title }}</h1>
  <p>年龄: {{ age }}</p>
</div>

<script>
  let vm = new Vue({
    el: '.app', // vue挂载的元素
    data: { // 定义数据
      title: '开始学习vue',
      age: 20
    }
  })
</script>
```

### 指令：标签上以 v- 开头的属性
1. v-text: js中的 innerText
2. v-html: js中的 innerHTML
3. v-if: 条件判断，会添加删除dom元素
4. v-show: 条件判断，不会添加删除dom元素，会添加删除 display: none
5. v-for: 列表渲染，可以渲染数组、字符串、数字、对象
6. v-on: 绑定事件，简写 @
```html
<button v-on:click="num --">-</button>
<button @click="sub">+</button>
<button @click="changeCount(-2)">-2</button>
```