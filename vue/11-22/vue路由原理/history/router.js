class Router {
  constructor ({ routerView, routes }) {
    this.routes = routes
    this.routerView = routerView
    this.routerLink = [...document.querySelectorAll('.router-link')]
    this.bindEvent()
    // 监听历史记录改变
    window.addEventListener('popstate', (e) => {
      // e.state 可以获取到 pushstate 存的数据
      if (!e.state) return
      const { to, title } = e.state
      const curRoute = this.routes.find(v => v.path === to)
      this.routerView.innerHTML = curRoute.component
      document.title = title
      console.log('pushState存的数据', e.state)
      // 添加高亮
      const active = document.querySelector('.router-link-active')
      active && active.classList.remove('router-link-active')
      this.routerLink.forEach(a => {
        const href = a.getAttribute('data-to')
        if (href === to) {
          a.classList.add('router-link-active')
        }
      })
    })
  }
  bindEvent () {
    this.routerLink.forEach(a => {
      a.addEventListener('click', () => {
        const to = a.getAttribute('data-to')
        // 使用 h5 新增的 pushstate 或者 replacestate 修改历史及路，跳转页面
        history.pushState({ to: to, title: a.innerHTML }, '', to)
        history.back()
        setTimeout(() => {
          history.forward()
        }, 10)
      })
    })
  }
}