// 组件中 script 标签中的所有选项
export default {
  data () {
    return {
      test: 123,
      obj: {
        a: 100,
        b: 200
      },
      arr: [1, 2, 3, 4],
      screen: 'small' // small | middle | big
    }
  },
  methods: {
    getScreen () {
      const clientWidth = document.documentElement.clientWidth
      console.log(clientWidth)
      if (clientWidth >= 1000) {
        this.screen = 'big'
      } else if (clientWidth < 1000 && clientWidth >= 500) {
        this.screen = 'middle'
      } else {
        this.screen = 'small'
      }
    }
  },
  created () {
    this.getScreen()
    window.addEventListener('resize', this.getScreen)
    console.log('mixin中的created', this.obj, this.arr)
  },
  beforeDestroy () {
    window.removeEventListener('resize', this.getScreen)
  }
}
