import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存数据
  state: {
    name: '刘小明',
    age: 30,
    sex: '女'
  },
  // 修改仓库数据的唯一方法，mutations中的函数必须是同步函数，不要做异步操作
  mutations: {
    setName (state, payload) {
      // state: 仓库中的原数据
      // payload: 调用该方法时传入的参数
      state.name = payload
    },
    addAge (state, payload) {
      state.age += payload
    },
    setSex (state, payload) {
      state.sex = payload
    },
    setInfo (state, payload) {
      state.name = payload.name
      state.age = payload.age
      state.sex = payload.sex
    }
  },
  // 类似组件中的计算属性，有缓存，函数中的依赖项更新时会自动重新计算结果
  getters: {
    userDetail (state) {
      return `我叫${state.name}, 今年${state.age}岁,性别${state.sex}`
    }
  }
})
