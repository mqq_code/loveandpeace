const Mock = require('mockjs')
const fs = require('fs')

// 生成随机数据
const data = Mock.mock({
  "list|1000": [{
    "id": "@id",
    "num|+1": 1,
    "firstName": "@cfirst()",
    "lastName": "@clast()",
    "age|18-40": 18,
    "sex|0-1": 0,
    "image": "@image(100x100, @color, @color, @cword)",
    "email": "@email",
    "address": "@county(true)"
  }]
})

fs.writeFileSync('./data.json', JSON.stringify(data.list))

