const { defineConfig } = require('@vue/cli-service')
const express = require('express')
const fs = require('fs')
const path = require('path')
const dataPath = path.join(__dirname, './data/data.json')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middleWares, devserver) {
      devserver.app.use(express.json())
      devserver.app.post('/api/login', (req, res) => {
        const { name, password } = req.body
        if (name && password) {
          res.send({
            code: 0,
            msg: '登陆成功',
            token: name + password
          })
        } else {
          res.send({
            code: -1,
            msg: '用户名或密码错误'
          })
        }
      })

      devserver.app.post('/api/list', (req, res) => {
        const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'))
        // 从请求头获取token
        const token = req.headers.authorization
        const {
          pagenum,
          pagesize,
          keyword
        } = req.body
        if (!token) {
          res.send({
            code: 401,
            msg: '登陆信息失效，请重新登陆',
          })
          return
        }
        if (pagenum && pagenum > 0 && pagesize && pagesize > 0) {
          let newData = data
          if (keyword) {
            newData = data.filter(v => (v.firstName + v.lastName).includes(keyword))
          }
          const list = newData.slice(pagenum * pagesize - pagesize, pagenum * pagesize)
          res.send({
            code: 0,
            msg: '成功',
            data: {
              list,
              total: newData.length,
              pagenum,
              pagesize
            }
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })

      devserver.app.delete('/api/del', (req, res) => {
        const { id } = req.query
        const token = req.headers.authorization
        if (!token) {
          res.send({
            code: 401,
            msg: '登陆信息失效，请重新登陆',
          })
          return
        }
        const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'))
        const index = data.findIndex(v => v.id === id)
        if (index > -1) {
          data.splice(index, 1)
          fs.writeFileSync(dataPath, JSON.stringify(data))
          res.send({
            code: 0,
            msg: '成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误，删除的数据不存在'
          })
        }
      })

      devserver.app.post('/api/add', (req, res) => {
        const { firstName, lastName, sex, age, email, address, image } = req.body
        const token = req.headers.authorization
        if (!token) {
          res.send({
            code: 401,
            msg: '登陆信息失效，请重新登陆',
          })
          return
        }
        if (!firstName || !lastName || sex === '' || !age || !email || !address) {
          res.send({
            code: -1,
            msg: '参数错误'
          })
          return
        }
        const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'))
        data.push({
          firstName,
          lastName,
          sex,
          age,
          email,
          address,
          id: Date.now() + '',
          image,
          num: data[data.length - 1].num + 1
        })
        fs.writeFileSync(dataPath, JSON.stringify(data))
        res.send({
          code: 0,
          msg: '成功'
        })
      })

      devserver.app.get('/api/detail', (req, res) => {
        const { id } = req.query
        const token = req.headers.authorization
        if (!token) {
          res.send({
            code: 401,
            msg: '登陆信息失效，请重新登陆',
          })
          return
        }
        const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'))
        const index = data.findIndex(v => v.id === id)
        if (index > -1) {
          res.send({
            code: 0,
            msg: '成功',
            data: data[index]
          })
        } else {
          res.send({
            code: -1,
            msg: '数据不存在'
          })
        }
      })

      devserver.app.post('/api/update', (req, res) => {
        const { id } = req.body
        const token = req.headers.authorization
        if (!token) {
          res.send({
            code: 401,
            msg: '登陆信息失效，请重新登陆',
          })
          return
        }
        const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'))
        const index = data.findIndex(v => v.id === id)
        if (index > -1) {
          data[index] = {
            ...data[index],
            ...req.body
          }
          fs.writeFileSync(dataPath, JSON.stringify(data))
          res.send({
            code: 0,
            msg: '成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })

      return middleWares
    },
    proxy: {
      '/mqq': {
        target: 'http://localhost:3000',
        pathRewrite: { '^/mqq': '' }
      }
    }
  }
})
