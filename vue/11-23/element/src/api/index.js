import axios from 'axios'
import { Message } from 'element-ui'
import router from '@/router'
// 统一管理项目中的接口请求，方便后续修改接口地址

axios.defaults.baseURL = '/'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前给接口传入公共参数
  const token = localStorage.getItem('token')
  if (token) {
    // 把 token 通过请求头传给后端
    config.headers.Authorization = localStorage.getItem('token')
  }
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 处理公共错误，比如登陆信息失效
  if (response.data.code === 401) {
    Message.error('登陆信息失效，请重新登陆')
    router.push({
      path: '/login',
      query: {
        redirect: encodeURIComponent(router.currentRoute.fullPath)
      }
    })
    return Promise.reject(Error('登陆信息失效'))
  }
  return response
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error)
})

export const login = (params = {}) => {
  return axios.post('/api/login', params)
}

export const getList = (params = {}) => {
  return axios.post('/api/list', params)
}

export const delUser = (id) => {
  return axios.delete('/api/del', { params: { id } })
}

export const addUser = (params = {}) => {
  return axios.post('/api/add', params)
}

export const updateUser = (params = {}) => {
  return axios.post('/api/update', params)
}

export const getDetail = (id) => {
  return axios.get('/api/detail', { params: { id } })
}
