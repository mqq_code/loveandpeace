import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/Home'
import Dashboard from '@/views/home/dashboard/Dashboard'
import Edit from '@/views/home/edit/Edit'
import List from '@/views/home/list/List'
import Login from '@/views/login/Login'
import Detail from '@/views/home/detail/Detail'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      isAuth: true
    },
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: '/list',
        name: 'list',
        component: List
      },
      {
        path: '/edit',
        name: 'edit',
        component: Edit
      },
      {
        path: '/list/detail/:id',
        name: 'detail',
        component: Detail
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next({
        path: '/login',
        query: {
          redirect: encodeURIComponent(to.fullPath)
        }
      })
      return
    }
  }
  next()
})

export default router
