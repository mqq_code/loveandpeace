const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      // 模拟接口
      const data = require('./data.json')
      devServer.app.get('/api/list', (req, res) => {
        res.send(data)
      })

      return middlewares;
    },
  }
})
