import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list: []
  },
  mutations: {
    addMemo (state, payload) {
      state.list.push({
        ...payload,
        time: Date.now()
      })
    },
    del (state, payload) {
      state.list = state.list.filter(v => v.time !== payload)
    },
    edit (state, payload) {
      state.list.forEach(item => {
        if (item.time === payload.time) {
          item.bjBack = payload.bjBack
          item.yjBack = payload.yjBack
        }
      })
    }
  }
})
