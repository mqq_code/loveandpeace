import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Memo from '../views/home/memo/Memo'
import Create from '../views/home/create/Create'
import Mine from '../views/home/mine/Mine'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/memo',
    children: [
      {
        path: '/memo',
        name: 'memo',
        component: Memo
      },
      {
        path: '/create',
        name: 'create',
        component: Create
      },
      {
        path: '/mine',
        name: 'mine',
        component: Mine
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('../views/detail/Detail')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
