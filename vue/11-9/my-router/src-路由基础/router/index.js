import Vue from 'vue'
import VueRouter from 'vue-router'
import Movie from '../pages/Movie'
import Cinema from '../pages/Cinema'
import Mine from '../pages/Mine'

// 安装路由插件
Vue.use(VueRouter)

// 创建路由实例对象
const router = new VueRouter({
  routes: [ // 配置路由表，配置页面地址对应需要展示的组件
    {
      path: '/movie', // 当页面的地址为 /abc 时显示 Movie组件
      component: Movie
    },
    {
      path: '/cinema',
      component: Cinema
    },
    {
      path: '/mine',
      component: Mine
    },
    {
      path: '/',
      redirect: '/movie' // 重定向，当路径匹配到 path 时自动跳转到 /movie
    }
  ]
})

export default router
