import Vue from 'vue'
import VueRouter from 'vue-router'
import City from '../pages/City'
// import Detail from '../pages/Detail'
import Home from '../pages/Home'
import Movie from '../pages/Movie'
import Cinema from '../pages/Cinema'
import Mine from '../pages/Mine'
import News from '../pages/News'
import Hot from '../pages/Hot'
import Coming from '../pages/Coming'
import Login from '../pages/Login'

// 安装路由插件
Vue.use(VueRouter)

// 创建路由实例对象
const router = new VueRouter({
  routes: [ // 配置路由表，配置页面地址对应需要展示的组件
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: '登陆'
      }
    },
    {
      // 动态路由，在path中定义参数
      path: '/detail/:filmId',
      name: 'detail',
      // 异步组件，组件懒加载
      component: () => import('../pages/Detail.vue'),
      meta: {
        // 把是否需要登陆拦截存在路由配置中
        isAuth: true,
        title: '详情'
      },
      // 路由独享守卫：只有跳转到 /city 时才会执行此函数
      beforeEnter: (to, from, next) => {
        console.log('beforeEnter ===> 要访问 /detail 路由了')
        next()
      }
    },
    {
      path: '/city',
      name: 'city',
      component: City,
      meta: {
        title: '城市'
      },
      // 路由独享守卫：只有跳转到 /city 时才会执行此函数
      beforeEnter: (to, from, next) => {
        console.log('要访问 /city 路由了')
        next()
      }
    },
    {
      path: '/news',
      name: 'news',
      component: News,
      meta: {
        isAuth: true,
        title: '咨讯'
      }
    },
    {
      path: '/',
      component: Home,
      name: 'home',
      redirect: '/movie',
      meta: {
        isAuth: true,
        title: '首页'
      },
      children: [
        {
          path: '/movie',
          component: Movie,
          name: 'movie',
          redirect: '/movie/hot',
          meta: {
            title: '电影'
          },
          children: [
            {
              path: '/movie/hot',
              name: 'hot',
              component: Hot,
              meta: {
                title: '正在热映'
              }
            },
            {
              path: '/movie/coming',
              name: 'coming',
              component: Coming,
              meta: {
                title: '即将上映'
              }
            }
          ]
        },
        {
          path: '/cinema',
          name: 'cinema',
          component: Cinema,
          meta: {
            title: '影院'
          }
        },
        {
          path: '/mine',
          name: 'mine',
          component: Mine,
          meta: {
            title: '个人中心'
          }
        }
      ]
    }
  ]
})

// 全局前置守卫：所有路由跳转之前执行
router.beforeEach((to, from, next) => {
  if (to.name === 'detail') {
    console.log('beforeEach ====> 要访问 /detail 路由了')
  }
  // to: 要跳转的目标路由
  // from: 要离开的路由
  // next: 是否允许跳转
  // 从路由元信息（配置信息）中获取当前路由是否需要拦截登陆\
  document.title = to.meta.title
  // to.matched: 当前路由的所有层级
  // 判断所有父级路由只要有一项 isAuth === true 执行登陆拦截
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next({
        path: '/login',
        query: {
          redirect: encodeURIComponent(to.fullPath)
        }
      })
      return
    }
  }
  next()
})

// 全局解析守卫，功能和beforeEach类似，区别是此守卫在所有组件内守卫和异步路由组件被解析之后
// router.beforeResolve((to, from, next) => {
//   console.log(to)
//   console.log(from)
//   next()
// })

// 全局后置钩子，路由跳转完成之后执行，无法拦截路由，可以做数据统计
router.afterEach((to, from) => {
  // console.log(to)
  // console.log(from)
  // 可以执行访问数据上报的方法
})

export default router
