const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middleWares, devserver) {

      devserver.app.get('/list', (req, res) => {
        res.send([1, 2, 3, 4, 5, 6, 7, 8, 9, 100])
      })

      return middleWares
    }
  }
})
