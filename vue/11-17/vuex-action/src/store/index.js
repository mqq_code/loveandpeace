import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存储数据
  state: {
    num: 0,
    arr: []
  },
  // 修改数据的唯一方式，必须是同步函数
  mutations: {
    setNum (state, payload) {
      state.num += payload
    },
    setArr (state, payload) {
      state.arr = payload
    }
  },
  // 计算属性
  getters: {
    total (state) {
      return state.arr.reduce((prev, next) => prev + next, 0)
    }
  },
  // 处理异步
  actions: {
    getlist (context, payload) {
      axios.get('/list').then(res => {
        // console.log(payload, res.data)
        // 调用 mutations 的setArr方法
        context.commit('setArr', res.data)
        // console.log(context) // 类似组件中 this.$store 的对象
      })
    }
  }
})
