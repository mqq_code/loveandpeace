const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js|ts|tsx)$/i,
        // 跳过node_modules下的文件解析
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html'
    })
  ],
  devServer: {
    port: 3000,
    open: true,
    hot: true,
    setupMiddlewares: (middlewares, devServer) => {

      devServer.app.get('/banner', (req, res) => {
        res.send([
          'https://img2.baidu.com/it/u=1003272215,1878948666&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800',
          'https://img2.baidu.com/it/u=617579813,2960860841&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800',
          'https://img2.baidu.com/it/u=2969169350,1551254405&fm=253&fmt=auto&app=120&f=PNG?w=1280&h=800',
          'https://img1.baidu.com/it/u=3113841445,1425021781&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800',
          'https://img1.baidu.com/it/u=1960110688,1786190632&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=281'
        ])
      })

      
      return middlewares;
    },
  }
}