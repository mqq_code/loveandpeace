import './scss/style.scss'
import Swiper from './swiper'
import axios from 'axios'

axios.get('/banner').then(res => {
  const wrapper = document.querySelector('.swiper1 .swiper-wrapper')
  wrapper!.innerHTML = res.data.map((item: string) => `
    <div class="swiper-slide">
      <img src="${item}" />
    </div>
  `).join('')


  new Swiper({
    el: document.querySelector('.swiper1')!,
    pagination: document.querySelector('.swiper-pagination') as HTMLElement,
    prev: document.querySelector('.swiper-prev')! as HTMLElement,
    next: document.querySelector('.swiper-next')! as HTMLElement,
    autoplay: {
      delay: 3000
    }
  })
})




new Swiper({
  el: document.querySelector('.swiper2')!,
  pagination: document.querySelector('.pagination1') as HTMLElement,
  autoplay: {
    delay: 2000
  }
})

