const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const fs = require('fs')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js|ts|tsx)$/i,
        // 跳过node_modules下的文件解析
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html'
    })
  ],
  devServer: {
    port: 3000,
    open: true,
    hot: true,
    setupMiddlewares: (middlewares, devServer) => {
      

      devServer.app.get('/api/list', (req, res) => {
        const { page, pagesize } = req.query
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, 'mock/data.json')))
        const list = data.slice(page * pagesize - pagesize, page * pagesize)
        res.send({
          list,
          total: data.length
        })
      })

      devServer.app.get('/api/del', (req, res) => {
        const { id } = req.query
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, 'mock/data.json')))
        const index = data.findIndex(v => v.id === id)
        if (index > -1) {
          data.splice(index, 1)
          fs.writeFileSync(path.join(__dirname, 'mock/data.json'), JSON.stringify(data))
          res.send({
            code: 0,
            msg: '删除成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })

      return middlewares;
    },
  }
}