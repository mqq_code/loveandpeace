const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')

const data = Mock.mock({
  "list|200": [
    {
      "id": "@id",
      "index|+1": 0,
      "name": "@cname",
      "age|18-30": 18,
      "sex|0-1": 0,
      "address": "@county(true)",
      "email": "@email"
    }
  ]
})

fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data.list))
