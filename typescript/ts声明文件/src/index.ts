/*
/// <reference path="./types/index.d.ts" /> // 表明这个文件使用了 ./types/index.d.ts 里面声明的名字
/// <reference types="node" /> // 表明这个文件使用了 @types/node/index.d.ts里面声明的名字
*/

// import axios from 'axios' // 自带 ts 类型声明文件
import Mock from 'mockjs' // 没有 ts 类型声明文件，尝试使用 `npm i --save-dev @types/包名` 下载对应的声明文件
// import { sum, formatDate, addZero } from './js/util' // 自定义js包，需要手动添加 .d.ts 类型声明文件
import $ from "jquery";

// console.log(AAAA)
// console.log(window.__UTIL__)

// const a: IObj123 = {
//   name: 'aaa',
//   age: 100,
//   sex: '未知'
// }

// const a = sum(100, 300)
// formatDate(1000000000)
// addZero(8)


// console.log(window.$aa)
// window.$tip('123')

// console.log(MQQ_version)
// setVersion('2.0.0')
// console.log(getVersion())

// MQQ_version = 'aaaa'

// console.log(ABC)

// const obj: IObj = { name: '123' }





