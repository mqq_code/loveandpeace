// 类型声明文件
// .ts 既可以写类型，也可以写具体逻辑
// .d.ts 只能定义类型
// .d.ts 中如果使用了 import 或者 export，那么此声明文件就是局部声明文件, 没有使用就是全局的声明文件

export const sum: (a: number, b: number) => number
export const formatDate: (n: number) => string
export const addZero: (n: number) => string

// 声明全局环境, 此作用域内可以声明全局变量
// 在局部文件中定义全局类型
declare global {

  // 声明全局环境, 此作用域内可以声明全局变量
  interface Window {
    __UTIL__: string
  }
  declare const AAAA: string
  
}

