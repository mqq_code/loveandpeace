
// 没有使用 import 或者 export，当前文件是全局声明文件，ts会自动读取全局文件中的类型
// 声明全局变量
declare const ABC: string

type IObj = { name: string }
