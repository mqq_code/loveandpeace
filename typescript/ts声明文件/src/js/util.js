export const sum = (a, b) => a + b

export const formatDate = (n) => new Date(n).toLocaleString()

export const addZero = (n) => n > 9 ? '0' + n : n

window.__UTIL__ = 'util.js'
