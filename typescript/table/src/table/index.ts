import './index.scss'

export interface IColumn<T> {
  title: string;
  key?: keyof T;
  render?: (record: T) => string
}

interface IOptions<T> {
  el: HTMLElement;
  column: IColumn<T>[];
  data: T[];
}

class Table<T> {
  private el: IOptions<T>['el']
  private column: IOptions<T>['column']
  private data: IOptions<T>['data']
  private head: HTMLTableSectionElement
  private tbody: HTMLTableSectionElement
  
  constructor ({ el, column, data }: IOptions<T>) {
    this.el = el
    this.column = column
    this.data = data
    this.init()
    this.head = this.el.querySelector('thead')!
    this.tbody = this.el.querySelector('tbody')!
    this.renderHead()
    this.renderBody()
  }
  init () {
    this.el.innerHTML = `
      <table border="1">
        <thead style="background: skyblue"></thead>
        <tbody></tbody>
      </table>
    `
  }
  renderHead () {
    this.head.innerHTML = `
      <tr>
        ${this.column.map(item => `<th>${item.title}</th>`).join('')}
      </tr>
    `
  }
  renderBody () {
    this.tbody.innerHTML = this.data.map(item => `
      <tr>
        ${this.column.map(val => {
          if (val.render) {
            return `<td>${val.render(item)}</td>`
          } else if (val.key) {
            return `<td>${item[val.key]}</td>`
          }
          return ''
        }).join('')}
      </tr>
    `).join('')
  }
  changeData (data: T[]) {
    this.data = data
    this.renderBody()
  }
}

export default Table
