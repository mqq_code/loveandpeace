import './scss/style.scss'
import Table, { IColumn } from './table'
import axios, { AxiosResponse } from 'axios'
import Pagination from './pagination'

interface Row {
  name: string;
  age: number;
  sex: 1 | 0;
  address: string;
  email: string;
  index: number;
  id: string;
}
// 总条数
let total = 0
let page = 1
// 表格数据
let data: Row[] = []
// 表头数据
const column: IColumn<Row>[] = [
  {
    title: 'id',
    key: 'id'
  },
  {
    title: '下标',
    key: 'index'
  },
  {
    title: '姓名',
    key: 'name'
  },
  {
    title: '年龄',
    key: 'age'
  },
  {
    title: '性别',
    key: 'sex',
    render: record => {
      return `<b>${record.sex ? '男生' : '女生'}</b>`
    }
  },
  {
    title: '地址',
    key: 'address'
  },
  {
    title: '邮箱',
    key: 'email'
  },
  {
    title: '操作',
    render: record => {
      return `<button class="del" data-record='${encodeURIComponent(JSON.stringify(record))}'>删除</button>`
    }
  }
]
// 实例化table
const table = new Table({
  el: document.querySelector('.table-wrap') as HTMLDivElement,
  column,
  data
})
// 实例化分页
const pagination = new Pagination({
  el: document.querySelector('.pagination')!,
  total,
  pagesize: 10,
  onChange: (current) => {
    console.log('当前页数', current)
    page = current
    getList()
  }
})

const remove = async (id: string) => {
  const res = await axios.get('/api/del', { params: { id } })
  if (res.data.code === 0) {
    getList()
    alert('删除成功')
  } else {
    alert(res.data.msg)
  }
}

const getList = async () => {
  // 调接口获取数据
  const res = await axios.get<any, AxiosResponse<{ list: Row[], total: number }>>('/api/list', {
    params: {
      page,
      pagesize: 10
    }
  })
  // 总数
  pagination.changeTotal(res.data.total)
  // 给表格添加数据
  table.changeData(res.data.list)
  // 获取删除按钮
  const dels = [...document.querySelectorAll('.del')]
  dels.forEach(btn => {
    btn.addEventListener('click', e => {
      const record = JSON.parse(decodeURIComponent(btn.getAttribute('data-record')!))
      remove(record.id)
    })
  })
}

getList()












