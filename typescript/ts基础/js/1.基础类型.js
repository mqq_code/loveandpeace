"use strict";
// js的基础类型: number、string、boolean、null、undefined、symbol、array、object、function
// ts中的类型: number、string、boolean、array、null、undefined、symbol、object、function、any、void、never、unknown
{
    // 类型注解：定义变量前需要先定义类型
    let flag = true;
    flag = false;
    let num = 100;
    let str = 'abc';
    // 定义数组方式一：
    let arr = [1, 2, 3, 4, 5];
    // 定义数组方式二：泛型方式
    let arr1 = ['a', 'b', 'c'];
    // 元组(已知元素数量和类型的数组)
    let x = ['a', 10];
    let sym = Symbol('a');
    // any：任意类型，相当于放弃了类型校验(尽量不要使用)
    let aa = 100;
    // void：没有任何类型，函数没有返回值时使用
    function sum(a, b) {
        console.log(a, b);
    }
    // undefined 和 null
    let n = null;
    let un = undefined;
    // never: 永不存在的值
    // 返回never的函数必须存在无法达到的终点
    // 返回never的函数必须存在无法达到的终点
    // function error(message: string): never {
    //   throw new Error(message);
    // }
    // // 推断的返回值类型为never
    // function fail(a: number) {
    //   return error("Something failed");
    // }
    // let aaa = fail(100)
    // object: 表示非原始类型，也就是除number，string，boolean，symbol，null或undefined之外的类型。
    let o1 = [];
    let o2 = {};
    let o3 = function () { };
    // unknown: 暂时不确定时什么类型
    let kk;
    function test(a) {
        if (a < 10) {
            kk = '0' + a;
        }
        else {
            kk = a;
        }
    }
    let a = 11;
    test(a);
    // 使用类型断言确定变量的类型
    if (a < 10) {
        console.log(kk.indexOf('0'));
    }
    else {
        console.log(kk.toFixed(2));
    }
}
