"use strict";
{
    // 对象
    let obj = {
        name: '小明',
        age: 200,
        hobby: ['吃饭'],
        detail: {
            a: 100,
            b: 'bbb'
        }
    };
    function getInfo(info) {
        return `我叫${info.name}, 今年${info.age}岁`;
    }
    console.log(getInfo(obj));
    let obj1 = {
        name: '小明',
        age: 100,
        hobby: ['吃饭'],
        say(text) {
            console.log(text);
        }
    };
    let obj2 = {
        name: '小明',
        hobby: ['吃饭']
    };
    obj1.a = 100;
    obj1.b = 200;
    let routes = [
        {
            path: '/',
            component: '首页',
            children: [
                {
                    path: '/home',
                    component: '首页1',
                },
                {
                    path: '/home',
                    component: '首页2',
                    children: [
                        {
                            path: '/home/child1',
                            component: '首页1-1',
                        },
                        {
                            path: '/home/child2',
                            component: '首页2-2',
                        }
                    ]
                }
            ]
        },
        {
            path: '/detail',
            component: '详情',
        }
    ];
    function renderRoutes(routes) {
        console.log(routes);
    }
    renderRoutes(routes);
}
