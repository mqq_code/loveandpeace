"use strict";
{
    // let a: IReturn<string>
    function fn(a) {
        if (typeof a === 'string') {
            return a.length;
        }
        else {
            return a.toFixed(2);
        }
    }
    let a = fn('aaaaaaaaa');
    let b = fn(100);
}
