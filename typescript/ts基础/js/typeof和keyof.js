"use strict";
{
    // typeof
    let obj = {
        name: '小明',
        age: 100
    };
    function fn(a) {
        if (typeof a === 'string') {
            return a.length;
        }
        else {
            return a.toFixed(2);
        }
    }
    let a = fn('1111');
    const arr = [1, 2, 3, 4, 5, 6];
    // type Ikeys = 'name' | 'age' | 'sex' | 'hobby'
    let k = 'hobby';
    let xm = {
        address: '北京',
        age: 200,
        sex: true,
        info: {
            a: 100,
            b: 'ssss'
        }
    };
    function getkey(obj, key) {
        return obj[key];
    }
    console.log(getkey(xm.info, 'b'));
}
