"use strict";
{
    // 联合类型： 由多个类型组成的叫联合类型
    let title = document.querySelector("h1");
    // 可选链: 如果 title 存在就返回 title.innerHTML,不存在就返回 undefined
    // console.log(title?.innerHTML)
    // ! 非空断言: 确定此变量不是 null 或者 undefined
    console.log(title.innerHTML);
    // 类型推论：ts会根据 = 后的内容推断出变量的类型
    let inp = document.querySelector('input');
    inp.addEventListener('input', (e) => {
        title.innerHTML = e.target.value;
    });
    // let box = document.querySelector('td')
}
