"use strict";
{
    function fn(a) {
        // 联合类型的变量只能使用所有类型都有的方法和属性
        // a.split('') // 报错
        if (typeof a === 'string') {
            // 类型保护： 确定此判断中 a 是字符串类型
            console.log(a.indexOf);
        }
    }
    fn('abc');
    let a = '李';
    function setSex(sex) {
        console.log('性别', sex);
    }
    let obj = {
        name: 'aaaa',
        hobby: 'aaaaaaa',
        sex: 1,
        age: 100
    };
}
