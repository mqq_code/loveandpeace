"use strict";
{
    function fn(person) {
        console.log(person);
    }
    let xm = {
        name: '王小明',
        age: 100
    };
    xm.aaa = 100;
    fn(xm);
    let fn1 = function (a, b) {
        return true;
    };
    let fn2 = function (a, b) {
        return true;
    };
    let arr = ['a', 'b', 'c', 'd'];
    let p1 = {
        name: 'p1',
        sex: '女'
    };
    // 接口可以继承class
    class IPerson1 {
        constructor(name) {
            this.name = name;
        }
    }
    let doctor1 = {
        age: 30,
        job: '外科医生',
        name: 'aaa'
    };
    // 泛型接口
}
