"use strict";
{
    // class
    // class Person {
    //   // 定义实例属性的类型
    //   name: string;
    //   age: number;
    //   getAge: () => void;
    //   constructor(name: string, age: number) {
    //     this.name = name
    //     this.age = age
    //     this.getAge = function () {
    //       console.log(this.age)
    //     }
    //   }
    //   // 原型方法
    //   say() {
    //     console.log(this.name + this.age)
    //   }
    // }
    // let xm = new Person('小明', 20)
    // console.log(xm)
    // // 继承
    // class Doctor extends Person {
    //   job: string;
    //   constructor(name: string, age: number, job: string) {
    //     // super: 父类的构造函数
    //     super(name, age)
    //     this.job = job
    //   }
    //   getJob () {
    //     console.log(this.job)
    //   }
    // }
    // let doc = new Doctor('小刚', 200, '外科医生')
    // console.log(doc)
    // 修饰符
    // class Person {
    //   // readonly: 只读属性，只能在 constructor 内赋值，后续不可以更改
    //   public readonly name: string; // 公有属性: class 内部、实例对象、子类都可以访问，默认不写
    //   private age: number; // 私有属性：只能在当前类内部使用，实例对象和子类都不可以访问
    //   protected height: number;  // 受保护的属性：可以在class内部或者子类内访问，实例化对象不可以访问
    //   getAge: () => void;
    //   constructor(name: string, age: number) {
    //     this.name = name
    //     this.age = age
    //     this.height = 100
    //     this.getAge = function () {
    //       console.log(this.age)
    //     }
    //     console.log(this.height)
    //   }
    //   say() {
    //     console.log(this.name + this.age)
    //   }
    // }
    // let xm = new Person('小明', 20)
    // class Doctor extends Person {
    //   // 实例属性：实例化对象的属性
    //   job: string;
    //   constructor(name: string, age: number, job: string) {
    //     super(name, age)
    //     this.job = job
    //     console.log(this.height)
    //   }
    //   getJob () {
    //     console.log(this.job)
    //   }
    //   // 静态成员: 类本身的属性，一般当作工具函数使用
    //   static abc: string = 'abc'
    //   static getAbc() {
    //     // 静态方法内不能使用实例属性，this指向类本身
    //     console.log(this.abc)
    //   }
    // }
    // let doc = new Doctor('小刚', 200, '外科医生')
    // console.log(doc)
    // console.log(Doctor.abc)
    // 使用过的静态方法
    // Date.now()
    // Array.isArray()
    // Array.from()
    // Object.keys
    // class Base {
    //   base = 'base'
    // }
    // // 使用接口规范class类型
    // // interface IPerson0 {
    // //   name: string;
    // //   age: number;
    // // }
    // // interface IPerson1 {
    // //   sex: '男' | '女';
    // //   hobby: string[];
    // // }
    // type IPerson0 = {
    //   name: string;
    //   age: number;
    // }
    // type IPerson1 = {
    //   sex: '男' | '女';
    //   hobby: string[];
    // }
    // // class实现接口的功能，可以实现多个接口，用逗号分隔
    // class Person extends Base implements IPerson0, IPerson1 {
    //   name: string = '小明';
    //   age: number;
    //   sex: '男' | '女';
    //   hobby: string[];
    //   constructor() {
    //     super()
    //     this.age = 100
    //     this.sex = '女'
    //     this.hobby = ['吃饭']
    //   }
    // }
    // 抽象类
    // 抽象类不能被实例化，只能被继承
    // 子类必须实现抽象类的抽象属性和抽象方法
    // 抽象类既可以约束子类的实现，也可以定义具体内容
    class Base {
        constructor(sex) {
            this.age = 20;
            this.sex = sex;
        }
        getSex() {
            console.log(this.sex);
        }
    }
    class Person extends Base {
        constructor(name, sex) {
            super(sex);
            this.name = name;
        }
        say(text) {
            console.log(text);
        }
    }
    let xm = new Person('小明', '女');
    console.log(xm);
}
