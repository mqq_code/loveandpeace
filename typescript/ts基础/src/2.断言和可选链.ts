{

  
// 联合类型： 由多个类型组成的叫联合类型
let title: HTMLHeadingElement | null = document.querySelector("h1")

// 可选链: 如果 title 存在就返回 title.innerHTML,不存在就返回 undefined
// console.log(title?.innerHTML)

// ! 非空断言: 确定此变量不是 null 或者 undefined
console.log(title!.innerHTML)

// 类型推论：ts会根据 = 后的内容推断出变量的类型
let inp: HTMLInputElement = document.querySelector('input')!

inp.addEventListener('input', (e: Event) => {
  // 类型断言方式一：
  title!.innerHTML = (e.target as HTMLInputElement).value
  // 类型断言方式二：
  title!.innerHTML = (<HTMLInputElement>e.target).value
})


// let box = document.querySelector('td')


}