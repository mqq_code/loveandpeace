{

  // type 和 interface 的区别
  // 1. interface 重名会合并，通过继承扩展
  // 2. type 重名会报错，通过交叉类型扩展


  // interface 如果有重名的会进行合并
  // interface IIPerson {
  //   name: string;
  //   sex: '男' | '女';
  //   say: (a: string) => void;
  // }
  // interface IIPerson {
  //   hobby: string[];
  // }

  // type 不可以重名
  // type IIPerson = {
  //   name: string;
  //   sex: '男' | '女';
  //   say: (a: string) => void;
  // }
  // type IIPerson = {
  //   hobby: string[];
  // }
  // let p1: IIPerson = {
  //   name: 'p1',
  //   sex: '女',
  //   say(a: string) {
  //     console.log(a)
  //   },
  //   hobby: ['aaa']
  // }



  // 接口通过继承实现扩展
  // interface IIPerson {
  //   name: string;
  //   sex: '男' | '女';
  // }
  // interface IDoctor extends IIPerson {
  //   age: number;
  //   job: string;
  // }
  
  // type 通过交叉类型进行扩展
  type IIPerson = {
    name: string;
    sex: '男' | '女';
  }
  type IDoctor = IIPerson & {
    age: number;
    job: string;
  }

  let doctor1: IDoctor = {
    age: 30,
    job: '外科医生',
    name: '医生1',
    sex: '男'
  }





}