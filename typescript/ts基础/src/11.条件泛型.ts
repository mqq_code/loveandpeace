{

  // 泛型约束
  // function fn<T extends { length: number }>(a: T): number {
  //   return a.length
  // }

  // fn({ a: 100, b: 'aaaa', length: 10 })

  // function fn<T extends 0 | 1>(a: T): string {
  //   return a === 1 ? '男' : '女'
  // }
  // fn(0)
  // 条件泛型
  type IReturn<T> = T extends string ? number : string
  // let a: IReturn<string>


  function fn<T extends string | number>(a: T): IReturn<T>  {
    if (typeof a === 'string') {
      return a.length as IReturn<T>
    } else {
      return a.toFixed(2) as IReturn<T>
    }
  }
  let a = fn('aaaaaaaaa')
  let b = fn(100)

    






}