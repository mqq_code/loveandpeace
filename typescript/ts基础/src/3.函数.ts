{

  // 函数
  function fn(a: number, b: number): string {
    return (a + b).toFixed(2)
  }
  // 类型推论：根据赋值结果推出变量的类型
  let fn2 = function (a: string): string {
    return a
  }
  // 函数的完整写法
  let fn1: (a: number, b: number) => string = function (a: number, b: number): string {
    return (a + b).toFixed(2)
  }
  const sum = (a: string): string => a

  // 参数默认值
  function request(url: string, methods = 'get') {
  }

  // 函数可选参数
  function add(a: number, b?: number) {
    console.log(a, b)
  }
  // console.log(add(1))

  // 函数剩余参数
  function add1 (...rest: number[]) {
    console.log('rest', rest)
    return rest.reduce((prev, val) => prev + val, 0)
  }
  console.log(add1(1, 2, 3, 4, 5, 7))

  // function fn4 () {
  //   console.log('arguments', arguments)
  // }
  // console.log(fn4('a', 'b', 'c', 'd'))


  // 函数重载
  function fn6(a: any[], b: number): number;
  function fn6(a: number, b: string): string;
  function fn6(a: any[] | number, b: number | string) {
    if (typeof a === 'number') {
      return b + a.toFixed(2)
    }
    if (Array.isArray(a) && typeof b === 'number') {
      return b
    }
  }
  // 根据传入的参数类型去查找对应的重载签名进行校验
  console.log(fn6(100, '200'))


}