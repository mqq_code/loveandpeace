{
  // typeof

  let obj = {
    name: '小明',
    age: 100
  }
  type IReturn<T> = T extends string ? number : string
  function fn<T extends string | number>(a: T): IReturn<T>  {
    if (typeof a === 'string') {
      return a.length as IReturn<T>
    } else {
      return a.toFixed(2) as IReturn<T>
    }
  }
  let a = fn('1111')
  const arr = [1, 2, 3, 4, 5, 6]

  // 类型中的typeof，返回某个变量的类型
  type IFn = typeof fn
  type IA = typeof a
  type IObj = typeof obj
  type IArr = typeof arr


  // keyof
  interface Iobj {
    name: string;
    age: number;
    sex: 0 | 1;
    hobby: []
  }

  type ITest = Iobj['age']

  // 返回对象类型中的所有 key 组成的字面量联合类型
  type Ikeys = keyof Iobj
  // type Ikeys = 'name' | 'age' | 'sex' | 'hobby'

  let k: Ikeys = 'hobby'

  let xm = {
    address: '北京',
    age: 200,
    sex: true,
    info: {
      a: 100,
      b: 'ssss'
    }
  }
  // 获取对象 xm 所有的 key 组成的类型
  type Ixm = keyof typeof xm


  function getkey<T, K extends keyof T> (obj: T, key: K): T[K] {
    return obj[key]
  }

  console.log(getkey(xm.info, 'b'));
  





}