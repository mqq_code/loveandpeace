{


  // type IPerson = {
  //   name: string;
  //   age: number;
  //   hobby: []
  // }

  // 定义对象类型
  interface IPerson {
    readonly name: string;
    age: number;
    hobby?: string[];
    [k: string]: any;
  }

  function fn(person: IPerson) {
    console.log(person)
  }

  let xm: IPerson = {
    name: '王小明',
    age: 100
  }
  xm.aaa = 100
  fn(xm)



  // 定义函数类型
  interface IFn {
    (a: string, b: number): boolean;
  }

  let fn1: IFn = function (a, b): boolean {
    return true
  }
  let fn2: (a: string, b: number) => boolean  = function (a, b): boolean {
    return true
  }

  // 可索引的类型
  interface StringArray {
    [i: number]: string;
  }
  let arr: StringArray = ['a', 'b', 'c', 'd']


  // 接口继承
  interface IIPerson {
    name: string;
    sex: '男' | '女';
  }

  let p1: IIPerson = {
    name: 'p1',
    sex: '女'
  }

  interface IDoctor extends IIPerson {
    age: number;
    job: string;
  }





  // 接口可以继承class
  class IPerson1 {
    name: string;
    constructor (name: string) {
      this.name = name
    }
  }

  interface IDoctor1 extends IPerson1 {
    age: number;
    job: string;
  }

  let doctor1: IDoctor1 = {
    age: 30,
    job: '外科医生',
    name: 'aaa'
  }




  // 泛型接口





}