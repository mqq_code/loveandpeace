{
  // ts 中常用的内置泛型
  // Partial
  // Required
  // Readonly
  // Pick
  // Omit
  // Record
  // Exclude
  // Extract
  // ReturnType

  interface IObj {
    name: string;
    age: number;
    sex: 0 | 1;
    hobby: string[];
    desc?: boolean;
  }
  let obj: IObj = {
    name: '小明',
    age: 20,
    sex: 0,
    hobby: ['吃饭'],
    desc: true
  }
 

  type PartialObj = Partial<IObj> // 把对象类型中的所有属性改成可选属性
  type ReadonlyObj = Readonly<IObj> // 把对象类型中的所有属性改成只读
  type RequiredObj = Required<IObj> // 把对象类型中的所有属性改成必传
  type PartialReadonlyObj = Readonly<Partial<IObj>> // 把对象类型中的所有属性改成可选并且只读

  type PickObj = Pick<IObj, 'name' | 'age'> // 根据对象中的某些属性组成的新类型
  type OmitObj = Omit<IObj, 'name' | 'age'> // 删除对象中的某些属性组成的新类型

  type RecordObj = Record<'a' | 'b' | 'c', string> // 返回所有属性类型一致的对象
  type RecordObj1 = Record<keyof typeof obj, string>

  type Exclude1 = Exclude<number | string | boolean, number> // 返回排除第二个参数后的类型
  type Extract1 = Extract<'a' | 'b' | 'c', 'a' | 'c'> // 返回包涵第二个参数的类型


  const sum = (a: number, b: number): number => a + b

  type fnReturn = ReturnType<typeof sum> // 获取函数返回值类型




}