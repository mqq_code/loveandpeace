{

  // 定义类型别名
  type IDetail = {
    a: number;
    b: string;
  }
  type IObj = {
    name: string;
    age: number;
    hobby: string[];
    detail: IDetail;
  }

  // 对象
  let obj: IObj = {
    name: '小明',
    age: 200,
    hobby: ['吃饭'],
    detail: {
      a: 100,
      b: 'bbb'
    }
  }
  
  function getInfo(info: IObj) {
    return `我叫${info.name}, 今年${info.age}岁`
  }

  console.log(getInfo(obj))


  type IObj1 = {
    readonly name: string; // 只读属性
    age?: number; // 可选属性
    hobby: string[];
    say?: (text: string) => void; // 方法类型
    [k: string]: any; // 对象允许添加额外的属性
  }

  let obj1: IObj1 = {
    name: '小明',
    age: 100,
    hobby: ['吃饭'],
    say(text: string): void {
      console.log(text)
    }
  }
  let obj2: IObj1 = {
    name: '小明',
    hobby: ['吃饭']
  }

  obj1.a = 100
  obj1.b = 200


  type IRoute = {
    path: string;
    component: string;
    children?: IRoute[]
  }

  let routes: IRoute[] = [
    {
      path: '/',
      component: '首页',
      children: [
        {
          path: '/home',
          component: '首页1',
        },
        {
          path: '/home',
          component: '首页2',
          children: [
            {
              path: '/home/child1',
              component: '首页1-1',
            },
            {
              path: '/home/child2',
              component: '首页2-2',
            }
          ]
        }
      ]
    },
    {
      path: '/detail',
      component: '详情',
    }
  ]

  function renderRoutes(routes: IRoute[]) {
    console.log(routes)
  }
  renderRoutes(routes)




}