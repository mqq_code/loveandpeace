import './index.scss'
import { $, get } from '../utils'
import axios from 'axios'

// tab切换
get('nav p').forEach((p, i) => {
  p.addEventListener('click', () => {
    $('.active').classList.remove('active')
    p.classList.add('active')
    $('.show').classList.remove('show')
    get('.content')[i].classList.add('show')
  })
})

axios.get('/api/personalized?limit=9')
.then(res => {
  $('.list').innerHTML = res.data.result.map(item => `
    <div class="list-item" data-id="${item.id}">
      <img src="${item.picUrl}">
      <p>${item.name}</p>
    </div>
  `).join('')

  get('.list .list-item').forEach(item => {
    item.addEventListener('click', () => {
      location.href = '/playlist.html?id=' + item.getAttribute('data-id')
    })
  })
})

axios.get('/api/personalized/newsong').then(res => {
  $('.songs').innerHTML = res.data.result.map(item => {
    const alias = item.song.alias.length > 0 ? `<b style="color: #999">(${item.song.alias[0]})</b>` : ''
    const artists = item.song.artists.map(v => v.name).join('/')
    return `
      <div class="songs-item" data-id="${item.id}">
        <h4>${item.name}${alias}</h4>
        <p>${artists} - ${item.song.album.name}</p>
        <svg t="1666332739456" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2520" width="30" height="30"><path d="M675.328 117.717333A425.429333 425.429333 0 0 0 512 85.333333C276.352 85.333333 85.333333 276.352 85.333333 512s191.018667 426.666667 426.666667 426.666667 426.666667-191.018667 426.666667-426.666667c0-56.746667-11.093333-112-32.384-163.328a21.333333 21.333333 0 0 0-39.402667 16.341333A382.762667 382.762667 0 0 1 896 512c0 212.074667-171.925333 384-384 384S128 724.074667 128 512 299.925333 128 512 128c51.114667 0 100.8 9.984 146.986667 29.12a21.333333 21.333333 0 0 0 16.341333-39.402667zM456.704 305.92C432.704 289.152 405.333333 303.082667 405.333333 331.797333v360.533334c0 28.586667 27.541333 42.538667 51.370667 25.856l252.352-176.768c21.76-15.253333 21.632-43.541333 0-58.709334l-252.373333-176.768z m-8.597333 366.72V351.466667l229.269333 160.597333-229.269333 160.597333z" fill="#3D3D3D" p-id="2521"></path></svg>
      </div>
    `
  }).join('')

  get('.songs-item').forEach(item => {
    item.addEventListener('click', () => {
      location.href = '/player.html?id=' + item.getAttribute('data-id')
    })
  })
})
