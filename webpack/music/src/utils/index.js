export const $ = (el, parent = document) => {
  return parent.querySelector(el)
}
export const get = (el, parent = document) => {
  return [...parent.querySelectorAll(el)]
}
export const query = (search) => {
  let arr = decodeURIComponent(search).slice(1).split('&')
  let obj = {}
  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key] = val
  })
  return obj
}