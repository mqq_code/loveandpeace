import './index.scss'
import axios from 'axios'
import { $, query, get } from '../utils'

// 获取url参数
const urlParams = query(location.search)

// 播放
const audio = $('audio')
$('.song-dist').addEventListener('click', () => {
  if (audio.paused) {
    audio.play()
    $('.playbtn').style.display = 'none'
    $('.song-area').classList.remove('pause')
  } else {
    audio.pause()
    $('.playbtn').style.display = 'block'
    $('.song-area').classList.add('pause')
  }
})

// 调用歌曲详情接口
axios.get('/api/song/detail', {
  params: {
    ids: urlParams.id,
    aa: 'aaaa',
    bb: 'asdfas'
  }
})
.then(res => {
  const ar = res.data.songs[0].ar.map(v => v.name).join('/')
  $('.bg').style.backgroundImage = `url(${res.data.songs[0].al.picUrl})`
  $('.img').src = res.data.songs[0].al.picUrl
  $('.song-name').innerHTML = `${res.data.songs[0].name} - ${ar}`
})


// 获取歌曲播放地址
axios.get('/api/song/url', {
  params: {
    id: urlParams.id
  }
})
.then(res => {
  audio.src = res.data.data[0].url
})

const formatTime = t => {
  const [m, s] = t.split(':')
  return Math.floor(m * 60 + s * 1)
}
let lyric = [] // 歌词
let lyricW = [] // 歌词元素
let lyrIndex = 0 // 歌词高亮下标
// 获取歌词
axios.get('/api/lyric', {
  params: {
    id: urlParams.id
  }
})
.then(res => {
  // 转换格式
  lyric = res.data.lrc.lyric.split('\n').map(item => {
    const [time, text] = item.slice(1).split(']')
    return {
      time: formatTime(time),
      text
    }
  }).filter(v => v.text)
  // 渲染歌词
  $('.lrc').innerHTML = lyric.map(item => `<li>${item.text}</li>`).join('')
  lyricW = get('.lrc li')
})

// 监听歌曲位置改变
audio.addEventListener('timeupdate', () => {
  const currentTime = Math.floor(audio.currentTime)
  lyric.forEach((item, i) => {
    if (currentTime >= item.time && currentTime < lyric[i + 1].time) {
      changeActive(i)
    }
  })
})

function changeActive (i) {
  if (i !== lyrIndex) {
    $('.active') && $('.active').classList.remove('active')
    lyricW[i] && lyricW[i].classList.add('active')
    lyrIndex = i
    $('.lrc').style.transform = `translateY(${i * -36 + 36}px)`
  }
}



// axios.get('/api/comment/music?id=186016')
//   .then(res => {
//     console.log(res.data)
//   })