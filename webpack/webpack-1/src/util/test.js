
const num = 100

// es6的模块化规范： es module
export const add = (a, b) => {
  return a + b
}

export const addZero = (n) => {
  return n < 10 ? '0' + n : n
}

console.log('test.js')