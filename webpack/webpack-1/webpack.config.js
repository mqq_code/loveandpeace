const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")


// CommonJS规范：node中使用的规范
module.exports = {
  // 配置开发模式：production ｜ development
  mode: 'production',
  // 打包的入口文件
  entry: './src/main.js',
  // 出口
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'js/index.js',
    assetModuleFilename: 'images/[name]-[hash:8][ext][query]', // 资源模块输出的位置
    clean: true // 打包时清除文件夹
  },
  // loader: 加载器，让js可以解析其他类型的文件
  module: {
    rules: [
      {
        test: /\.(scss|css)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpe?g|png|gif|svg|webp|woff|woff2|eot|ttf|otf)$/i,
        type: 'asset' // 会自动判断文件大小，大文件导入文件地址，小文件转换成dataurl
      }
    ]
  },
  // 插件：扩展webpack功能
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'css/style.css'
    })
  ],
  // 配置开发服务器
  devServer: {
    port: 3000,
    open: true,
    hot: true
  }
}