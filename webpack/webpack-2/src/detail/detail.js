import './detail.scss'
import axios from 'axios'

// 同源策略：浏览器的保护机制，只能访问和当前页面 协议、域名、端口 一致的接口
// 跨域：发起 xhr 请求时调用的接口和当前页面地址的 协议、域名、端口 其中一个不一致就会跨域
/*
  解决跨域问题：
    1. jsonp
        - 只能解决get请求跨域
        - 需要后端配合
      原理：利用 script 请求不会跨域的特点，动态创建 script 标签向后端发送请求，只能发送给get请求，
        后端需要返回一个函数调用的js代码，把数据通过函数的参数传给前端
    2. 服务器代理
        - 本地开发可以使用 webpack 中的 devServer.proxy 代理
        - 生产环境可以使用 nginx 代理
      原理：页面请求向本机的服务发起请求，本机的服务向目标服务器发送请求，接收目标服务器的数据返回给页面
    3. 跨域资源共享
        - 后端设置允许跨域访问的响应头
        - Access-Control-Allow-Origin 允许跨域访问的地址
        - Access-Control-Allow-Methods 允许跨域访问的请求方式
*/ 
// axios.get('/api/address/cityList')
// .then(res => {
//   console.log(res.data)
// })

// axios.get('http://192.168.0.108:5500/list')
// .then(res => {
//   console.log(res.data)
// })


window.aaaa = function (res) {
  console.log('jsonp的返回值', res)
}

function jsonp (url) {
  let script = document.createElement('script')
  script.src = url
  document.body.appendChild(script)
  script.onload = () => {
    script.remove()
  }
}

jsonp('http://192.168.0.108:5500/list?resCallback=aaaa')



