module.exports = {
  // 预设：常用插件的集合
  presets: ['@babel/preset-env']
  // 插件：配置需要babel转换的语法
  // plugins: [
  //   '@babel/plugin-transform-arrow-functions',
  //   '@babel/plugin-transform-block-scoping',
  //   '@babel/plugin-transform-classes'
  // ]
}