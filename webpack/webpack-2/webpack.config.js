const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

// CommonJS规范：node中使用的规范
module.exports = {
  // 配置开发模式：production ｜ development
  mode: 'production',
  // 添加原始代码和打包后的代码的映射关系
  devtool: 'source-map',
  // 打包的入口文件
  entry: {
    app: './src/index/main.js',
    detail: './src/detail/detail.js'
  },
  // 出口
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'js/[name]_[hash:8].js',
    clean: true // 打包时清除文件夹
  },
  // loader: 加载器，让js可以解析其他类型的文件
  module: {
    rules: [
      {
        test: /\.(scss|css)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpe?g|png|gif|svg|webp|woff|woff2|eot|ttf|otf)$/i,
        type: 'asset' // 会自动判断文件大小，大文件导入文件地址，小文件转换成dataurl
      },
      {
        test: /\.js$/i,
        // 编译时跳过此目录下的文件
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        // 解析 html 内使用的图片
        test: /\.html$/i,
        use: ['html-loader']
      }
    ]
  },
  // 插件：扩展webpack功能
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index/index.html',
      filename: 'index.html',
      chunks: ['app'] // 页面中引入的模块
    }),
    new HtmlWebpackPlugin({
      template: './src/detail/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    })
  ],
  // 配置开发服务器
  devServer: {
    port: 3000,
    open: true,
    hot: true,
    proxy: {
      // /api/address/cityList => https://gatewx.dmall.com/api/address/cityList
      // '/api': 'https://gatewx.dmall.com',

      // /api/address/cityList => https://gatewx.dmall.com/address/cityList
      '/api': {
        target: 'https://gatewx.dmall.com',
        pathRewrite: { '^/api': '' },
        changeOrigin: true
      }
    }
  },
  // 配置解析
  resolve: {
    // 配置路径别名
    alias: {
      // 配置文件夹别名
      '@': path.resolve(__dirname, 'src'),
      '@utils': path.resolve(__dirname, 'src/utils'),
      // 配置具体文件别名，必须以 $ 结尾
      xyz$: path.resolve(__dirname, 'src/utils/index.js'),
    },
    // 配置可以忽略的文件后缀名
    extensions: ['.js', '.json', '.scss'],
  }
}