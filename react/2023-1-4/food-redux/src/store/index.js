import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const init = {
  list: [],
  selected: []
}

const reducer = (state = init, action) => {
  if (action.type === 'SET_LIST') {
    return { ...state, list: action.payload }
  } else if (action.type === 'CHANGE_CHECKED') {
    const list = [...state.list]
    let selected = [...state.selected]
    list.forEach(item => {
      item.list.forEach(food => {
        if (food.name === action.payload) {
          food.checked = !food.checked
          if (food.checked) {
            selected.push(food)
          } else {
            selected = selected.filter(v => v.name !== action.payload)
          }
        }
      })
    })
    return { ...state, list, selected }
  }
  return state
}

const store = createStore(reducer, applyMiddleware(logger))

export default store