import {
  Switch,
  Route
} from 'react-router-dom'
import Home from './pages/Home'
import Estimate from './pages/Estimate'
import NotFound from './pages/404'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/estimate" component={Estimate} />
      <Route component={NotFound} />
    </Switch>
  )
}

export default App