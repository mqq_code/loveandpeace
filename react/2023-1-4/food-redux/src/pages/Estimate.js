import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import classNames from 'classnames'

const Estimate = () => {
  const tablist = useSelector(s => {
    return [
      {
        title: '建议少吃',
        color: 'tomato',
        tip: '以下食物升糖指数偏高，建议少吃',
        child: s.selected.filter(v => v.glycemicIndex > 60)
      },
      {
        title: '适量食用',
        color: 'orange',
        tip: '以下食物升糖指数适中，建议适量食用',
        child: s.selected.filter(v => v.glycemicIndex <= 60 && v.glycemicIndex > 30)
      },
      {
        title: '放心食用',
        color: 'green',
        tip: '以下食物升糖指数偏低，可以放心食用',
        child: s.selected.filter(v => v.glycemicIndex <= 30)
      }
    ]
  })
  const [curIndex, setIndex] = useState(0)

  return (
    <div className="page estimate">
      <nav>
        {tablist.map((item, index) =>
          <p
            key={item.color}
            className={classNames({ active: curIndex === index })}
            onClick={() => setIndex(index)}
          >
            <b>{item.child.length}</b>
            <span>{item.title}</span>
          </p>
        )}
      </nav>
      <div className="content">
        <div className="tip" style={{ color: tablist[curIndex].color }}>{tablist[curIndex].tip}</div>
        <div className="food-list">
          {tablist[curIndex].child.map(item =>
            <div className="food-item" key={item.name}>
              <h3>{item.name}</h3>
              <p>升糖指数: {item.glycemicIndex}</p>
              <img style={{ width: 60 }} src={item.imgUrl} alt="" />
              <p>{item.content}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default Estimate