import React, { useEffect, useState } from 'react'
import axios from 'axios'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

const Home = () => {
  const list = useSelector(s => s.list)
  const selected = useSelector(s => s.selected)
  const dispatch = useDispatch()
  const [curIndex, setIndex] = useState(0)

  const getList = async () => {
    const res = await axios.get('/api/list')
    dispatch({
      type: 'SET_LIST',
      payload: res.data.data
    })
  }

  useEffect(() => {
    if (list.length === 0) {
      getList()
    }
  }, [])

  const changeChecked = name => {
    dispatch({
      type: 'CHANGE_CHECKED',
      payload: name
    })
  }

  return (
    <div className="page home">
      <header>
        <input type="text" placeholder="请输入要搜索的食物" />
      </header>
      <main>
        <div className="left">
          {list.map((item, index) =>
            <p key={item.id} className={classNames({ active: curIndex === index })} onClick={() => setIndex(index)}>{item.title}</p>
          )}
        </div>
        <div className="right">
          {list[curIndex]?.list.map(item =>
            <div
              className={classNames("food", { active: item.checked })}
              key={item.name}
              onClick={() => changeChecked(item.name)}
            >
              <div className="img">
                <img src={item.imgUrl} alt="" />
              </div>
              <p>{item.name}</p>
            </div>
          )}
        </div>
      </main>
      <footer className={classNames({ show: selected.length > 0 })}>
        <div className="footer-title">
          <span>已选食物（{selected.length}）</span>
          <Link to="/estimate">去测评</Link>
        </div>
        <ul className="list">
          {selected.map(item =>
            <li key={item.name}>
              {item.name}
              <span onClick={() => changeChecked(item.name)}>x</span>
            </li>
          )}
        </ul>
      </footer>
    </div>
  )
}

export default Home
