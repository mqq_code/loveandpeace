const express = require('express')

const app = express()

app.get('/api/list', (req, res) => {
  res.send({
    code: 0,
    msg: '成功',
    data: [
      {
        title: '豆奶制品',
        id: 'milk',
        list: require('./data/豆奶制品.json')
      },
      {
        title: '蔬菜类',
        id: 'vegetable',
        list: require('./data/蔬菜类.json')
      },
      {
        title: '主食',
        id: 'staple',
        list: require('./data/主食.json')
      }
    ]
  })
})

app.listen(5001, () => {
  console.log('服务启动成功 http://localhost:5001')
})



// json格式数据类型：字符串、数字、boolean、数组、对象、null
// let json = '[{ "name": "aaaa", "age": 200, "sex": true }]'