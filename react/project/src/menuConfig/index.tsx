import { Link } from 'umi'
import {
  HomeOutlined,
  ReadOutlined,
  FileSearchOutlined,
  FileAddOutlined,
  AppstoreAddOutlined,
  PlusSquareOutlined,
  AppstoreOutlined,
  ApartmentOutlined,
  TeamOutlined,
  MenuOutlined,
  UsergroupAddOutlined,
  SettingOutlined,
  UserOutlined,
  UserSwitchOutlined,
} from '@ant-design/icons'

export const IconObj = {
  HomeOutlined,
  ReadOutlined,
  FileSearchOutlined,
  FileAddOutlined,
  AppstoreAddOutlined,
  PlusSquareOutlined,
  AppstoreOutlined,
  ApartmentOutlined,
  TeamOutlined,
  MenuOutlined,
  UsergroupAddOutlined,
  SettingOutlined,
  UserOutlined,
  UserSwitchOutlined
}

export type ImenuItems = {
  key: string;
  icon: React.ReactElement;
  label: JSX.Element | string;
  children?: ImenuItems;
}[]

export const getMenus = (menu: ImenuItems, authlist: string[]): ImenuItems => {
  const res: ImenuItems = []
  menu.forEach(item => {
    if (authlist.includes(item.key)) {
      res.push(item)
    }
    if (item.children) {
      let childlist = getMenus(item.children, authlist)
      if (childlist.length > 0) {
        item.children = childlist
        if (!authlist.includes(item.key)) {
          res.push(item)
        }
      }
    }
  })
  return res
}


const menuItems: ImenuItems = [
  {
    key: '/dashboard',
    icon: <UserOutlined />,
    label: <Link to="/dashboard">监控页</Link>,
  },
  {
    key: '/article',
    icon: <ReadOutlined />,
    label: '文章管理',
    children: [
      {
        key: '/article/list',
        icon: <FileSearchOutlined />,
        label: <Link to="/article/list">文章列表</Link>,
      },
      {
        key: '/article/add',
        icon: <FileAddOutlined />,
        label: <Link to="/article/add">添加文章</Link>,
      },
    ]
  },
  {
    key: '/classify',
    icon: <AppstoreAddOutlined />,
    label: '分类管理',
    children: [
      {
        key: '/classify/list',
        icon: <PlusSquareOutlined />,
        label: <Link to="/classify/list">文章分类列表</Link>,
      },
      {
        key: '/classify/add',
        icon: <AppstoreOutlined />,
        label: <Link to="/classify/add">添加文章分类</Link>,
      },
    ]
  },
  {
    key: '/auth',
    icon: <ApartmentOutlined />,
    label: '权限管理',
    children: [
      {
        key: '/auth/userlist',
        icon: <TeamOutlined />,
        label: <Link to="/auth/userlist">用户管理</Link>,
      },
      {
        key: '/auth/menulist',
        icon: <MenuOutlined />,
        label: <Link to="/auth/menulist">菜单管理</Link>,
      },
      {
        key: '/auth/rolelist',
        icon: <UsergroupAddOutlined />,
        label: <Link to="/auth/rolelist">角色管理</Link>,
      },
    ]
  },
  {
    key: '/setting',
    icon: <SettingOutlined />,
    label: '系统设置',
    children: [
      {
        key: '/setting/userinfo',
        icon: <UserOutlined />,
        label: <Link to="/setting/userinfo">个人中心</Link>,
      },
      {
        key: '/setting/setuser',
        icon: <UserSwitchOutlined />,
        label: <Link to="/setting/setuser">修改信息</Link>,
      },
    ]
  },
]

export default menuItems