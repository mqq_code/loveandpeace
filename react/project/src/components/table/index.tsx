import { useEffect, useState, forwardRef, useImperativeHandle } from 'react'
import { Table } from 'antd';
import type { ColumnsType, TableProps } from 'antd/es/table';

interface IQuery {
  page: number;
  pagesize: number;
}

interface IProps {
  getListApi: (params: any) => Promise<any>;
  pageSizeOptions?: number[];
  query?: IQuery
}

export interface IRef {
  getList: () => Promise<any>;
  setQuery: (query: any) => void;
  resetQuery: () => void;
}
const List: React.ForwardRefRenderFunction<IRef, IProps & TableProps<any>> = (props, ref) => {
  const [data, setData] = useState<any[]>([])
  const [query, setQuery] = useState(props.query || { page: 1, pagesize: 3 })
  const [total, setTotal] = useState(0)
  const getList = async () => {
    const res = await props.getListApi(query)
    if (res.values.page > res.values.totalPage || (res.values.page === 0 && res.values.totalPage > 0)) {
      setQuery({
        ...query,
        page: res.values.totalPage
      })
    } else {
      setData(res.values.list)
      setTotal(res.values.total)
    }
  }
  useEffect(() => {
    getList()
  }, [query])

  useImperativeHandle(ref, () => {
    return {
      getList,
      setQuery: (params: any) => {
        setQuery({
          ...query,
          ...params
        })
      },
      resetQuery: () => {
        setQuery({
          page: 1,
          pagesize: query.pagesize
        })
      }
    }
  }, [query])

  return (
    <Table
      {...props}
      columns={props.columns}
      dataSource={data}
      rowKey={props.rowKey}
      pagination={{
        current: query.page,
        pageSize: query.pagesize,
        pageSizeOptions: props.pageSizeOptions || [2, 3, 4, 5],
        total,
        showSizeChanger: true,
        onChange (page, pagesize) {
          console.log(page, pagesize)
          setQuery({ page, pagesize })
        }
      }}
    />
  )
}

export default forwardRef(List)