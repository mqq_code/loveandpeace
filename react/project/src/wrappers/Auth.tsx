import React, { useEffect } from 'react'
import { useSelector, UserModelState, Redirect, useDispatch } from 'umi'
import { RouteComponentProps } from 'react-router-dom'
import Loading from '@/components/loading'

const Auth: React.FC<RouteComponentProps> = (props) => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch({
      type: 'user/getUserInfo'
    })
  }, [])
  // 从仓库读取允许访问的菜单
  const menulist = useSelector((state: { user: UserModelState }) => state.user.userInfo.menulist)
  if (menulist.length === 0) {
    return <Loading />
  }
  const pathname = props.location.pathname === '/' ? '/dashboard' : props.location.pathname
  // 判断当前访问的地址是否存在接口中
  if (menulist.includes(pathname)) {
    return <>{props.children}</>
  } else {
    return <Redirect to="/403" />
  }
}

export default Auth