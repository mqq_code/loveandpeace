import { Effect, ImmerReducer, Subscription } from 'umi';
import { getUserInfo } from '@/services'

export interface UserModelState {
  userInfo: UserInfoResponse["values"];
  test: string;
}

export interface UserModelType {
  namespace: 'user';
  state: UserModelState;
  effects: {
    getUserInfo: Effect;
  };
  reducers: {
    // 启用 immer 之后
    setUserInfo: ImmerReducer<UserModelState>;
  };
  subscriptions: { setup: Subscription };
}

const IndexModel: UserModelType = {
  namespace: 'user',

  state: {
    userInfo: {
      username: '',
      account: '',
      email: '',
      menulist: [],
      role: [],
      avatar: ''
    },
    test: ''
  },

  effects: {
    *getUserInfo({ payload }, { call, put }) {
      try {
        const res = yield call(getUserInfo)
        yield put({
          type: 'setUserInfo',
          payload: res.values
        })
      } catch (e) {
        console.log(e)
      }
    },
  },
  
  reducers: {
    // 启用 immer 之后
    setUserInfo(state, action) {
      state.userInfo = action.payload;
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
    },
  },
};

export default IndexModel;