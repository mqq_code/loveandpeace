import request from './request'

export const login = (params: LoginRequest) => {
  return request.post<any, LoginResponse>('/api/user/login', params)
}

export const logout = () => {
  return request.post<any, ResponseBase<any>>('/api/user/logout')
}

// 用户信息
export const getUserInfo = () => {
  return request.post<any, UserInfoResponse>('/api/user/info')
}

// 添加文章类型
export const addArticleType  = (params: ArticleTypeRequest) => {
  return request.post<any, ResponseBase<any>>('/api/article/type/add', params)
}
// 文章类型列表
export const getArticleTypeList = (params: ArticleListRequest = {}) => {
  return request.get<any, ArticleTypeListResponse>('/api/article/type/list', { params })
}

// 添加文章
export const addArticle  = (params: addArticleRequest) => {
  return request.post<any, ResponseBase<any>>('/api/article/add', params)
}
// 文章列表
export const getArticleList = (params: ArticleListRequest = {}) => {
  return request.get<any, ArticleListResponse>('/api/article/list', { params })
}
// 文章详情
export const getArticleDetail = (id: string) => {
  return request.get<any, ArticleDetailResponse>('/api/article/detail', { params: { id } })
}

// 角色列表
export const getRoleList = (params: ArticleListRequest = {}) => {
  return request.get<any, RoleListResponse>('/api/role/list', { params })
}

// 菜单列表
export const getMenuList = (params: ArticleListRequest = {}) => {
  return request.get<any, MenuListResponse>('/api/permissions/menulist', { params })
}

// 用户列表
export const getUserList = (params: UserListRequest = {}) => {
  return request.get<any, UserListResponse>('/api/user/list', { params })
}

// 添加用户
export const addUser = (params: addUserRequest) => {
  return request.post<any, ResponseBase<any>>('/api/user/add', params)
}

// 删除用户
export const removeUser = (uid: string) => {
  return request.delete<any, ResponseBase<any>>('/api/user/remove', { params: { uid } })
}

// 编辑用户
export const editUser = (params: editRequest) => {
  return request.put<any, ResponseBase<any>>('/api/user/edit', params)
}