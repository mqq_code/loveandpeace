
interface ResponseBase<T> {
  code: number;
  msg: string;
  values: T extends { [key: string]: any } ? T : {}
}

// 登陆接口
interface LoginRequest {
  username: string;
  password: string;
}
type LoginResponse = ResponseBase<{
  token: string;
}>

// 用户信息接口
type UserInfoResponse = ResponseBase<{
  username: string;
  account: string;
  email: string;
  menulist: string[];
  role: string[];
  avatar: string;
}>

// 添加文章接口
interface addArticleRequest {
  title: string;
  keyCode: string[];
  desc: string;
  content: string;
}
// 文章详情
type ArticleDetailResponse = ResponseBase<{
  id: string;
  title: string;
  desc: string;
  content: string;
  author: string;
  createTime: string;
  keyCode: {
    title: string;
    keyCode: string;
    color: string;
  }[];
}>
// 文章列表接口
interface ArticleListRequest {
  page?: number;
  pagesize?: number;
}
interface ArticleKeyCode {
  title: string;
  keyCode: string;
  color: string;
}
interface ArticleItem {
  id: string;
  title: string;
  keyCode: ArticleKeyCode[],
  author: string;
}
type ArticleListResponse = ResponseBase<{
  total: number;
  list: ArticleItem[],
  page: number;
  pagesize: number;
  totalPage: number;
}>

// 文章类型列表接口
interface ArticleTypeRequest {
  title: string;
  keyCode: string;
}
interface ArticleListRequest {
  page?: number;
  pagesize?: number;
}
interface ArticleTypeItem {
  title: string;
  keyCode: string;
  color: string;
  count: number;
}
type ArticleTypeListResponse = ResponseBase<{
  total: number;
  list: ArticleTypeItem[],
  page: number;
  pagesize: number;
  totalPage: number;
}>

// 角色列表
interface RoleItem {
  id: number;
  role: string;
  roleName: string;
  note: string;
  menuList: string[]
}

type RoleListResponse = ResponseBase<{
  total: number;
  list: RoleItem[],
  page: number;
  pagesize: number;
  totalPage: number;
}>

// 菜单列表
interface MenuItem {
  id: number;
  path: string;
  title: string;
  icon: string;
  parentId?: number;
  children?: MenuItem[]
}

type MenuListResponse = ResponseBase<{
  list: RoleItem[],
  total: number;
  page: number;
  pagesize: number;
  totalPage: number;
}>


// 用户列表
interface UserListRequest {
  page?: number;
  pagesize?: number;
  account?: string;
  status?: number;
  email?: number;
  adduser?: number;
}
interface UserItem {
  uid: number;
  username: string;
  avatar: string;
  account: string;
  email: string;
  status: 0 | 1;
  addTime: string;
  lastOnlineTime: string;
  adduser: string;
  disabled: boolean;
}
type UserListResponse = ResponseBase<{
  total: number;
  list: UserItem[],
  page: number;
  pagesize: number;
  totalPage: number;
}>
// 添加用户
interface addUserRequest {
  username: string;
  account: string;
  status: number;
  email: string;
  password: string;
}
// 编辑用户
type editRequest = Partial<addUserRequest> & {
  uid: number;
  role?: string[];
}