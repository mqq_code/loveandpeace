import { useState, useEffect } from 'react'
import { Button, Select, Form, Input, message } from 'antd';
import type { SelectProps } from 'antd';
import { getArticleTypeList, addArticle } from '@/services'
import style from './index.less'
import { history } from 'umi'
import classNames from 'classnames'
// 引入编辑器组件
import BraftEditor from 'braft-editor'
// 引入编辑器样式
import 'braft-editor/dist/index.css'

const Add = () => {
  const onFinish = async (values: any) => {
    console.log('Success:', values);
    const res = await addArticle({
      ...values,
      content: values.content.toHTML()
    })
    if (res.code === 200) {
      message.success('添加成功')
      history.push('/article/list')
    } else {
      message.error(res.msg)
    }
  };

  const [options, setOptions] = useState<SelectProps['options']>([])
  useEffect(() => {
    getArticleTypeList().then(res => {
      const options: SelectProps['options']  = []
      res.values.list.forEach(item => {
        if (!options.find(v => v.value === item.keyCode)) {
          options.push({
            label: item.title,
            value: item.keyCode
          })
        }
      })
      setOptions(options)
    })
  }, [])

  // 创建一个空的editorState作为初始值
  const [editorState, setEditorState] = useState(BraftEditor.createEditorState(null))
  const handleEditorChange = (editorState: any) => {
    setEditorState(editorState )
  }
  // 记录正文是否检验失败
  const [failContent, setFailContent] = useState(false)

  return (
    <div>
      <Form
        layout="vertical"
        onFinish={onFinish}
      >
        <Form.Item
          label="标题"
          name="title"
          rules={[{ required: true, message: '请输入标题!' }]}
        >
          <Input style={{ width: 500 }} />
        </Form.Item>

        <Form.Item
          label="文章类型"
          name="keyCode"
          rules={[{ required: true, message: '请选择文章类型!' }]}
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: 500 }}
            placeholder="请选择标签"
            options={options}
          />
        </Form.Item>

        <Form.Item
          label="文章简介"
          name="desc"
          rules={[{ required: true, message: '请输入文章简介!' }]}
        >
          <Input.TextArea style={{ width: 500, resize: 'none' }} />
        </Form.Item>

        <Form.Item
          label="正文"
          name="content"
          rules={[
            { required: true, message: '请填写正文！' },
            ({ getFieldValue }) => ({
              // getFieldValue: 获取表单其他input内容
              validator(rule, value) {
                // rule: 当前项的校验规则
                // value: 当前校验项的value
                if (value?.isEmpty()) {
                  setFailContent(true)
                  return Promise.reject(new Error('请输入正文'));
                }
                setFailContent(false)
                return Promise.resolve()
              },
            })
          ]}
        >
          <BraftEditor
            className={classNames(style.editor, { [style.fail]: failContent })}
            value={editorState}
            onChange={handleEditorChange}
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">提交</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Add