import React, { useEffect, useState } from 'react'
import { getArticleDetail } from '@/services'
import { useParams } from 'umi'
import { Button, Spin, Tag } from 'antd'
import style from './index.less'
import 'braft-editor/dist/output.css'

const Detail = () => {
  const [info, setInfo] = useState<ArticleDetailResponse['values'] | null>(null)
  const params = useParams<{ id: string }>()
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    getArticleDetail(params.id)
      .then(res => {
        if (res.code === 200) {
          setInfo(res.values)
        }
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])
  if (loading) return <Spin />
  if (info === null) {
    return <div style={{ height: '100%', textAlign: 'center'}}>
      <h2>此文章不存在</h2>
      <Button>去首页</Button>
    </div>
  }
  return (
    <div>
      <div className={style.row}>
        <p>发布者：{info.author}</p>
        <p>发布时间：{info.createTime}</p>
        <p>标签：{info.keyCode.map(item => <Tag key={item.keyCode} color={item.color}>{item.title}</Tag>)}</p>
        <p>简介：{info.desc}</p>
      </div>
      <div className={style.content}>
        <div className="braft-output-content" dangerouslySetInnerHTML={{ __html: info.content }}></div>
      </div>
    </div>
  )
}

export default Detail