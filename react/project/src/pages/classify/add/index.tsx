import { Button, Form, Input, message } from 'antd';
import { addArticleType } from '@/services'
import { history } from 'umi'

const Add = () => {
  const onFinish = async (values: ArticleTypeRequest) => {
    const res = await addArticleType(values)
    if (res.code === 200) {
      message.success('添加分类成功')
      history.push('/classify/list')
    } else {
      message.error(res.msg)
    }
  };

  return (
    <div>
      <Form
        style={{ width: '500px' }}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item label="分类名称" name="title" rules={[{ required: true, message: '请输入分类名称!' }]}>
          <Input />
        </Form.Item>
        <Form.Item label="分类关键字" name="keyCode" rules={[{ required: true, message: '请输入分类关键字!' }]}>
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
          <Button type="primary" htmlType="submit">提交</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Add