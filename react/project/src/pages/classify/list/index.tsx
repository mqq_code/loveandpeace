import { getArticleTypeList } from '@/services'
import { Space, Tag, Button } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import Table from '@/components/table'

const List = () => {
 
  const columns: ColumnsType<ArticleTypeItem> = [
    {
      title: '分类名称',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: '分类关键字',
      dataIndex: 'keyCode',
      key: 'keyCode',
    },
    {
      title: '颜色',
      dataIndex: 'color',
      key: 'color',
      render: (color, record) => {
        // _: dataIndex在当前行中的数据
        // record: 当前行的所有数据
        return <Tag color={color}>{color}</Tag>
      }
    },
    {
      title: '文章数量',
      dataIndex: 'count',
      key: 'count',
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => {
        return <Space>
          <Button>编辑</Button>
          <Button danger>删除</Button>
        </Space>
      }
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        getListApi={getArticleTypeList}
        rowKey={item => item.color + item.keyCode}
      />
    </div>
  )
}

export default List