import { Button, Result } from 'antd';
import { history } from 'umi'

const Forbidden = () => {
  return (
    <Result
      status="403"
      title="403"
      subTitle="抱歉您没有权限访问此页面"
      extra={<Button type="primary" onClick={() => history.push('/')}>回到首页</Button>}
    />
  )
}

export default Forbidden