import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { history } from 'umi'
import { login } from '@/services'

interface IFormValues {
  remember: boolean;
  username: string;
  password: string;
}

const Account = () => {
  const [form] = Form.useForm() // 获取表单组件实例
  const onFinish = async (values: IFormValues) => {
    const { remember, ...params } = values
    if (remember) {
      localStorage.setItem('username', params.username)
    } else {
      localStorage.removeItem('username')
    }
    try {
      const res = await login(params)
      console.log(res)
      if (res.code === 200) {
        message.success('登陆成功')
        localStorage.setItem('token', res.values.token)
        history.push('/')
      } else {
        message.error(res.msg)
      }
    } catch (e) {
      message.error('网络错误，请稍后重试！')
    }
  };

  return <Form
    initialValues={{
      username: localStorage.getItem('username') || '',
      remember: !!localStorage.getItem('username')
    }}
    form={form}
    onFinish={onFinish}
  >
    <Form.Item
      name="username"
      rules={[{ required: true, message: '请输入用户名!' }]}
    >
      <Input prefix={<UserOutlined />} placeholder="账号" />
    </Form.Item>
    <Form.Item
      name="password"
      rules={[{ required: true, message: '请输入密码!' }]}
    >
      <Input prefix={<LockOutlined />} type="password" placeholder="密码"/>
    </Form.Item>
    <Form.Item>
      <Form.Item name="remember" valuePropName="checked" noStyle>
        <Checkbox>记住账号</Checkbox>
      </Form.Item>
      <a style={{ float: 'right' }} href="">忘记密码</a>
    </Form.Item>
    <Form.Item>
      <Button type="primary" block onClick={form.submit}>登陆</Button>
      {/* <Button type="primary" block htmlType='submit'>登陆</Button> */}
    </Form.Item>
  </Form>
}

export default Account