import { Tabs } from 'antd'
import style from './login.less'
import Account from './components/Account'
import Tel from './components/Tel'

const Login = () => {

  return (
    <div className={style.wrap}>
      <div className={style.content}>
        <div className={style.logo}>
          <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="" />
          <b>被水淹没不知所措</b>
        </div>
        <p className={style.desc}>你也想学习游泳吗</p>
        <Tabs
          defaultActiveKey="1"
          centered
          items={[
            { label: '账号密码登陆', key: '1', children: <Account /> },
            { label: '手机号登陆', key: '2', children: <Tel /> },
          ]}
        />
      </div>
    </div>
  )
}

export default Login