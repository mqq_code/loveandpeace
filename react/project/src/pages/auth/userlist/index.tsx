import { useEffect, useState, useRef } from 'react'
import { getUserList, addUser, removeUser, editUser, getRoleList } from '@/services'
import Table, { IRef } from '@/components/table'
import moment, { Moment } from 'moment'
import { Switch, Space, Button, Modal, Radio, Form, Input, message, Popconfirm, Spin, Row, Col, Select, DatePicker } from 'antd'
import type { ColumnsType } from 'antd/es/table';
import { DownOutlined, UpOutlined } from '@ant-design/icons'
import style from './index.less'
const { RangePicker } = DatePicker;

const UserList = () => {
  const [openModal, setOpenModal] = useState(false)
  const [form] = Form.useForm()
  const [searchForm] = Form.useForm()
  const [roleForm] = Form.useForm()
  const tableRef = useRef<IRef>(null)
  const [editInfo, setEditInfo] = useState<UserItem | null>(null)
  const [loading, setLoading] = useState(false)
  const [showMore, setShowMore] = useState(false)
  const [showRole, setShowRole] = useState(false)
  // 添加
  const addOk = async () => {
    setLoading(true)
    try {
      // 校验表单
      const values = await form.validateFields()
      const { confirmPassword, ...other } = values
      // 掉接口
      const res = await addUser(other)
      if (res.code === 200) {
        message.success('添加成功')
        setOpenModal(false)
        // 调用子组件方法
        await tableRef.current?.getList()
      } else {
        message.error(res.msg)
      }
      setLoading(false)
    } catch (e) {
      console.log(e)
      setLoading(false)
    }
  }
  // 编辑用户
  const confirmEdit = async (values: editRequest) => {
    setLoading(true)
    try {
      const res = await editUser({
        ...values,
        uid: editInfo?.uid || values.uid
      })
      if (res.code === 200) {
        message.success('编辑成功')
        await tableRef.current?.getList()
        setOpenModal(false)
      } else {
        message.error(res.msg)
      }
      setLoading(false)
    } catch (e) {
      console.log(e)
      setLoading(false)
    }
  }
  // 点击弹窗确定按钮
  const onOk = () => {
    if (editInfo) {
      form.validateFields().then(values => {
        confirmEdit(values)
      })
    } else {
      addOk()
    }
  }
  useEffect(() => {
    if (!openModal) {
      form.resetFields()
      setEditInfo(null)
    }
  }, [openModal])
  // 删除用户
  const remove = async (uid: string) => {
    setLoading(true)
    const res = await removeUser(uid)
    if (res.code === 200) {
      message.success('删除成功！')
      await tableRef.current?.getList()
    } else {
      message.error(res.msg)
    }
    setLoading(false)
  }
  // 显示编辑弹窗
  const showEdit = (row: UserItem) => {
    setOpenModal(true)
    setEditInfo(row)
    form.setFieldsValue(row)
  }

  const columns: ColumnsType<any> = [
    {
      title: '姓名',
      dataIndex: 'account',
      key: 'account',
      fixed: 'left',
      width: 100
    },
    {
      title: '账号',
      dataIndex: 'username',
      key: 'username',
      fixed: 'left',
      width: 100
    },
    {
      title: '账号状态',
      dataIndex: 'status',
      key: 'status',
      width: 150,
      render: (checked, record) => {
        return <Switch checked={checked} disabled={record.disabled} onChange={checked => {
          confirmEdit({
            uid: record.uid,
            status: Number(checked)
          })
        }}></Switch>
      }
    },
    {
      title: '邮箱',
      width: 200,
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: '创建人',
      width: 150,
      dataIndex: 'adduser',
      key: 'adduser',
    },
    {
      title: '创建时间',
      dataIndex: 'addTime',
      key: 'addTime',
      width: 220,
      render: (_) => moment(_).format('YYYY-MM-DD kk:mm:ss')
    },
    {
      title: '最后登陆时间',
      dataIndex: 'lastOnlineTime',
      key: 'lastOnlineTime',
      width: 220,
      render: (_) => _ ? moment(_).format('YYYY-MM-DD kk:mm:ss') : '-'
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: (_, record) => {
        return <Space>
          <Button type="primary" size="small" disabled={record.disabled} onClick={() => {
            setShowRole(true)
            setEditInfo(record)
            roleForm.setFieldsValue(record)
          }}>分配角色</Button>
          <Button size="small" disabled={record.disabled} onClick={() => showEdit(record)}>编辑</Button>
          <Popconfirm
            title="确定要删除此用户吗？"
            onConfirm={() => remove(record.uid)}
            okText="删除"
            cancelText="取消"
            okButtonProps={{ danger: true, type: 'ghost' }}
            disabled={record.disabled}
          >
            <Button danger type="primary" size="small" disabled={record.disabled}>删除</Button>
          </Popconfirm>
        </Space>
      }
    },
  ]
  
  const renderPassword = (
    <>
      <Form.Item
        label="密码"
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input.Password onChange={() => {
          if (form.getFieldValue('confirmPassword')) {
            form.validateFields(['confirmPassword'])
          }
        }} />
      </Form.Item>
      <Form.Item
        label="确认密码"
        name="confirmPassword"
        rules={[
          { required: true, message: '请确认密码!' },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (value !== getFieldValue('password')) {
                return Promise.reject(new Error('两次密码不一致'))
              }
              return Promise.resolve()
            }
          })
        ]}
      >
        <Input.Password />
      </Form.Item>
    </>
  )
  const search = () => {
    const values = searchForm.getFieldsValue()
    console.log(values.createTime.map((time: Moment) => time.format('YYYY-MM-DD kk:mm:ss')))
    tableRef.current?.setQuery(values)
  }
  const resetSearch = () => {
    searchForm.resetFields()
    tableRef.current?.resetQuery()
  }

  const [roleOptions, setRoleOptions] = useState<RoleItem[]>([])
  useEffect(() => {
    getRoleList().then(res => {
      setRoleOptions(res.values.list)
    })
  }, [])
  useEffect(() => {
    if (!showRole) {
      setEditInfo(null)
    }
  }, [showRole])
  return (
    <div>
      <div className={style.row}>
        <Form labelCol={{ span: 6 }} form={searchForm}>
          <Row gutter={20}>
            <Col span={8}>
              <Form.Item label="账号/姓名" name="account">
                <Input placeholder='请输入账号/姓名' />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="启用状态" name="status">
                <Select 
                  placeholder="选择启用状态"
                  options={[
                    {
                      label: '全部',
                      value: ''
                    },
                    {
                      label: '启用',
                      value: 1
                    },
                    {
                      label: '禁用',
                      value: 0
                    }
                  ]}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="邮箱" name="email" rules={[{ type: 'email', message: '邮箱格式错误' }]}>
                <Input placeholder='邮箱' />
              </Form.Item>
            </Col>
          </Row>
          {showMore &&
            <Row gutter={20}>
              <Col span={8}>
                <Form.Item label="创建人" name="adduser">
                  <Input placeholder='创建人' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="创建时间" name="createTime">
                  <RangePicker showTime />
                </Form.Item>
              </Col>
            </Row>
          }
        </Form>
        <Space>
          <Button type="primary" onClick={search}>搜索</Button>
          <Button onClick={resetSearch}>重置</Button>
          <Button
            type='link'
            icon={showMore ? <UpOutlined /> : <DownOutlined />}
            onClick={() => setShowMore(!showMore)}
          >{showMore ? '收起' : '展开'}</Button>
        </Space>
      </div>
      <div className={style.row}>
        <Button type="primary" onClick={() => setOpenModal(true)}>添加用户</Button>
      </div>
      <Table
        ref={tableRef}
        loading={loading}
        columns={columns}
        getListApi={getUserList}
        rowKey="uid"
        scroll={{ x: 1300 }}
        query={{
          page: 1,
          pagesize: 5
        }}
      />
      <Modal
        title={editInfo ? '编辑用户' : '添加用户'}
        centered
        open={openModal}
        cancelText="取消"
        okText="确定"
        onOk={onOk}
        onCancel={() => setOpenModal(false)}
      >
        
        <Spin spinning={loading}>
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 19 }} form={form}>
            <Form.Item
              label="账号"
              name="username"
              rules={[{ required: true, message: '请输入账号!' }]}
            >
              <Input disabled={!!editInfo} />
            </Form.Item>
            <Form.Item
              label="姓名"
              name="account"
              rules={[{ required: !editInfo, message: '请输入姓名!' }]}
            >
              <Input />
            </Form.Item>
            {!editInfo && renderPassword}
            <Form.Item
              label="邮箱"
              name="email"
              rules={[{ required: !editInfo, message: '请输入邮箱!' }, { type: 'email', message: '邮箱格式错误!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="状态"
              name="status"
              rules={[{ required: !editInfo, message: '请选择账号状态!' }]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
      <Modal
        title='分配角色'
        centered
        open={showRole}
        cancelText="取消"
        okText="确定"
        onOk={async () => {
          const values = await roleForm.validateFields()
          await confirmEdit({
            ...values,
            uid: editInfo?.uid
          })
          setShowRole(false)
        }}
        onCancel={() => setShowRole(false)}
      >
        <Spin spinning={loading}>
          <Form form={roleForm}>
            <Form.Item name="role" rules={[{ required: true, message: '请选择角色!' }]}>
              <Select
                placeholder="请选择用户角色"
                mode="multiple"
                fieldNames={{
                  label: 'roleName',
                  value: 'role'
                }}
                options={roleOptions}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </div>
  )
}

export default UserList