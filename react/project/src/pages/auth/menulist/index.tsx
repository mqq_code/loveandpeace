import React from 'react'
import Table from '@/components/table'
import { getMenuList } from '@/services'
import type { ColumnsType } from 'antd/es/table';
import { IconObj } from '@/menuConfig'

const MenuList = () => {
  const colums: ColumnsType<MenuItem> = [
    {
      title: '菜单id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '菜单路径',
      dataIndex: 'path',
      key: 'path',
    },
    {
      title: '菜单图标',
      dataIndex: 'icon',
      key: 'icon',
      render: (iconName: keyof typeof IconObj) => {
        return React.createElement(IconObj[iconName])
      }
    },
    {
      title: '菜单标题',
      dataIndex: 'title',
      key: 'title',
    }
  ]
  return (
    <div>
      <Table
        columns={colums}
        getListApi={getMenuList}
        rowKey="id"
      />
    </div>
  )
}

export default MenuList
