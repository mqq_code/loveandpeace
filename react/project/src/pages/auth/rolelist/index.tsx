import Table from '@/components/table'
import { getRoleList } from '@/services'
import type { ColumnsType } from 'antd/es/table';
import { Button } from 'antd'

const RoleList = () => {
  const colums: ColumnsType<RoleItem> = [
    {
      title: '角色id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '角色key',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: '角色名称',
      dataIndex: 'roleName',
      key: 'roleName',
    },
    {
      title: '角色描述',
      dataIndex: 'note',
      key: 'note',
    },
    {
      title: '操作',
      key: 'action',
      render: () => {
        return <Button type="primary">分配角色</Button>
      }
    },
  ]
  return (
    <div>
      <Table
        columns={colums}
        getListApi={getRoleList}
        rowKey="id"
      />
    </div>
  )
}

export default RoleList