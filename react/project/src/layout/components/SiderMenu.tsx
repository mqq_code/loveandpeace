import { Menu } from 'antd';
import { useMemo } from 'react';
import { useLocation } from 'umi'
import menuItems, { getMenus } from '@/menuConfig'
import { useSelector, UserModelState } from 'umi'

const SiderMenu = () => {
  const location = useLocation()
  const menulist = useSelector((state: { user: UserModelState }) => state.user.userInfo.menulist)
  // 根据接口的权限数据从所有菜单中查找数据
  const authMenulist = useMemo(() => getMenus(menuItems, menulist), [menulist])

  const defaultOpenKeys = useMemo(() => {
    const openKeys: string[] = []
    menuItems.forEach(item => {
      if (item.children) {
        item.children.forEach(val => {
          if (val.key === location.pathname) {
            openKeys.push(item.key)
          }
        })
      }
    })
    return openKeys
  }, [])

  return (
    <Menu
      theme="light"
      mode="inline"
      defaultOpenKeys={defaultOpenKeys}
      defaultSelectedKeys={[location.pathname === '/' ? '/dashboard' : location.pathname]}
      items={authMenulist}
    />
  )
}

export default SiderMenu