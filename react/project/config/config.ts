import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { exact: true, path: '/403', component: '@/pages/403' },
    { exact: true, path: '/404', component: '@/pages/404' },
    { exact: true, path: '/login', component: '@/pages/login' },
    { exact: true, path: '/detail/:id', component: '@/pages/detail' },
    {
      path: '/',
      component: '@/layout',
      wrappers: ['@/wrappers/Auth'],
      routes: [
        { exact: true, path: '/dashboard', component: '@/pages/dashboard' },
        { exact: true, path: '/article/add', component: '@/pages/article/add' },
        { exact: true, path: '/article/list', component: '@/pages/article/list' },
        { exact: true, path: '/classify/add', component: '@/pages/classify/add' },
        { exact: true, path: '/classify/list', component: '@/pages/classify/list' },
        { exact: true, path: '/auth/userlist', component: '@/pages/auth/userlist' },
        { exact: true, path: '/auth/menulist', component: '@/pages/auth/menulist' },
        { exact: true, path: '/auth/rolelist', component: '@/pages/auth/rolelist' },
        { exact: true, path: '/setting/userinfo', component: '@/pages/setting/userinfo' },
        { exact: true, path: '/setting/setuser', component: '@/pages/setting/setuser' },
        { exact: true, path: '/', redirect: '/dashboard' },
        { redirect: '/404' }
      ]
    },
  ],
  fastRefresh: {},
  dva: {
    immer: true,
    hmr: false,
  },
});
