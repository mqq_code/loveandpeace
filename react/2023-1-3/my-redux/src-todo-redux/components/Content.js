import React, { Component, useState, useEffect } from 'react'
import store from '../store'


const Content = () => {
  const [num, setNum] = useState(store.getState().num)

  useEffect(() => {
    const unsub = store.subscribe(() => {
      setNum(store.getState().num)
    })

    return unsub
  }, [])

  const add = () => {
    store.dispatch({
      type: 'ADD_NUM'
    })
  }

  return (
    <div className='content'>
      <p>Content: {num}</p>
      <button onClick={add}>+</button>
    </div>
  )
}

export default Content




// class Content extends Component {
//   state = {
//     // 获取仓库数据
//     num: store.getState().num
//   }
//   add = () => {
//     // 调用 store.dispatch 触发 reducer 函数执行修改仓库数据
//     // 发送 action 给 reducer 函数
//     // action 是一个对象，必须有type属性，用来描述本次如何更新
//     store.dispatch({
//       type: 'ADD_NUM'
//     })
//   }
//   sub = () => {
//     // action 是一个对象，必须有type属性，用来描述本次如何更新
//     store.dispatch({
//       type: 'SUB_NUM'
//     })
//   }
//   componentDidMount () {
//     // 监听仓库数据改变,更新页面
    // this.unsub = store.subscribe(() => {
    //   this.setState({
    //     num: store.getState().num
    //   })
    // })
//   }

//   componentWillUnmount() {
//     // 组件销毁清除监听
//     this.unsub()
//   }

//   render() {
//     return (
//       <div className="content">
//         <button onClick={this.add}>+</button>
//         <p>Content: {this.state.num}</p>
//         <button onClick={this.sub}>-</button>
//       </div>
//     )
//   }
// }


// export default Content
