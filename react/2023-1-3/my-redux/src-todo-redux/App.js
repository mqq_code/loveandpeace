import Header from "./components/Header"
import Content from "./components/Content"
import Todo from './components/Todo'


function App() {
  return (
    <div className="App">
      <Header />
      <Todo />
      <hr />
      <Content />
    </div>
  );
}

export default App;
