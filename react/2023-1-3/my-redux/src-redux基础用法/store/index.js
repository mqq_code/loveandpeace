import { createStore } from 'redux'

// 初始值
const init = {
  num: 100
}

// 修改和返回state
const reducer = (state, action) => {
  // state: 当前仓库数据，reducer函数上一次的返回值
  console.log(state, action)

  if (action.type === 'ADD_NUM') {

    return {...state, num: state.num + 1}
  } else if (action.type === 'SUB_NUM') {
    
    return {...state, num: state.num - 1}
  }
  // 组件中 store.getState() 能够获取到此返回值
  return state
}

// 创建仓库
const store = createStore(reducer, init)

export default store
