import React from 'react'
import store from '../store'

const Header = () => {
  return (
    <div className="header">
      <button onClick={() => {
        store.dispatch({
          type: 'ADD_NUM'
        })
      }}>+</button>
    </div>
  )
}

export default Header