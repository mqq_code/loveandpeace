import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

// 初始值
const init = {
  num: 100,
  list: [
    {
      id: '0',
      text: '第一条数据',
      done: false
    }
  ]
}

// 修改和返回state
const reducer = (state = init, action) => {

  if (action.type === 'ADD_NUM') {
    return {...state, num: state.num + 1}
  } else if (action.type === 'SUB_NUM') {
    return {...state, num: state.num - 1}
  } else if (action.type === 'ADD_LIST') {
    return {
      ...state,
      list: [
        {
          id: Date.now(),
          text: action.payload,
          done: false
        },
        ...state.list
      ]
    }
  } else if (action.type === 'CHANGE_DONE') {
    const list = [...state.list]
    for (let item of list) {
      if (item.id === action.payload) {
        item.done = !item.done
      }
    }
    return {
      ...state,
      list
    }
  } else if (action.type === 'REMOVE_ITEM') {
    return {
      ...state,
      list: state.list.filter(v => v.id !== action.payload)
    }
  }
  return state
}

// applyMiddleware: 添加redux中间件
// 中间件作用：增强dispatch
const store = createStore(reducer, applyMiddleware(logger))

export default store
