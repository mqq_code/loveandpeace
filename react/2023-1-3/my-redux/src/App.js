import Header from "./components/Header"
import Content from "./components/Content"
import Todo from './components/Todo'
import { useDispatch, useSelector, useStore } from 'react-redux'
import { useEffect } from "react"

const App = () => {
  const store = useStore() // 获取store对象

  const dispatch = useDispatch() // 获取dispatch函数
  const num = useSelector(s => s.num) // 获取仓库数据
  const list = useSelector(s => {
    // s === store.getState()
    return s.list.filter(v => v.done)
  })
  const state = useSelector(s => s)
  // console.log(state)

  // useEffect(() => {
  //   console.log('list 改变了', list)
  // }, [list])

  return (
    <div className="App">
      {num}
      <button onClick={() => dispatch({ type: 'ADD_NUM' })}>+</button>
      <hr />
      <Header />
      <Todo />
      <hr />
      <Content />
    </div>
  );
}

export default App;
