import React, { useRef } from 'react'
import { connect } from 'react-redux'

const Header = (props) => {

  const inp = useRef()

  const addList = () => {
    props.addList(inp.current.value)
    inp.current.value = ''
  }

  return (
    <div className="header">
      <div className="todo-header">
        <input type="text" ref={inp} />
        <button onClick={addList}>add</button>
      </div>
      <button onClick={props.addNum}>+</button>
    </div>
  )
}

const mapDispatch = dispatch => {
  return {
    addList (payload) {
      dispatch({
        type: 'ADD_LIST',
        payload
      })
    },
    addNum() {
      dispatch({ type: 'ADD_NUM' })
    }
  }
}
export default connect(null, mapDispatch)(Header)