import React, { Component, useState, useEffect } from 'react'
import { connect } from 'react-redux'

// class Content extends Component {
//   render() {
//     return (
//       <div className="content">
//         <button onClick={this.props.add}>+</button>
//         <p>Content: {this.props.num}</p>
//         <button onClick={this.props.sub}>-</button>
//       </div>
//     )
//   }
// }



const Content = (props) => {
  return (
    <div className='content'>
      <p>Content: {props.num}</p>
      <button onClick={props.add}>+</button>
      <button onClick={props.sub}>-</button>
    </div>
  )
}

const mapState = state => {
  // state: store中的数据
  // console.log(state)
  // return 的对象会和 this.props 进行合并
  return {
    num: state.num
  }
}

const mapDispatch = dispatch => {
  // 把 return 的内容传到组件的props中
  return {
    add() {
      dispatch({ type: 'ADD_NUM' })
    },
    sub() {
      dispatch({
        type: 'SUB_NUM'
      })
    }
  }
}
// connect： 高阶组件
// 把 store 中的数据传给组件的 props
// 如果不穿 mapDispatch 可以在组件中通过 this.props.dispatch 发起action修改数据
export default connect(mapState, mapDispatch)(Content)



