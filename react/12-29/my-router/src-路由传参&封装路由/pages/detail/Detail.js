import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'

const query = str => {
  const arr = str.slice(1).split('&')
  const obj = {}
  for (let val of arr) {
    const [key, v] = val.split('=')
    obj[key] = v
  }
  return obj
}

const Detail = (props) => {
  // props.location.search 获取？拼接的参数
  // const qs = query(props.location.search) // 把参数转成对象

  // 接收params参数
  // console.log(props.match.params)
  const params = useParams()
  // console.log(params)

  // 获取state参数
  console.log(props.location.state)

  const [info, setInfo] = useState({})

  const getDetail = async () => {
    const res = await axios.get('/hot/detail', {
      params: {
        // filmId: qs.filmId
        filmId: params.id
      }
    })
    if (res.data.status === 0) {
      setInfo(res.data.data)
    } else {
      alert(res.data.msg)
    }
  }

  useEffect(() => {
    getDetail()
  }, [])


  return (
    <div>
      <img width="200" src={info.poster} alt="" />
      <h2>{info.name}</h2>
      <p>{info.category}</p>
      <p>{info.synopsis}</p>
      <ul>
        {info.actors && info.actors.map(item =>
          <li key={item.name}>
            <h3>{item.name}</h3>
            <p>{item.role}</p>
            <img width="100" src={item.avatarAddress} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Detail