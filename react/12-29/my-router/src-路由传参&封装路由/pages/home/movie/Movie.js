import React, { useState } from 'react'
import style from './movie.module.scss'
import classNames from 'classnames'
import { NavLink } from 'react-router-dom'
import { Provider } from '../../../context/movieCtx'

const Movie = (props) => {
  const [isDown, setIsDown] = useState(false)

  const scroll = (e) => {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    if (scrollTop + clientHeight + 1 >= scrollHeight) {
      setIsDown(true)
    }
  }

  return (
    <Provider value={{ isDown, setIsDown }}>
      <div className={classNames('page', style.movie)} onScroll={scroll}>
        <nav>
          <NavLink activeClassName={style.active} exact to="/movie/hot">正在热映</NavLink>
          <NavLink activeClassName={style.active} exact to="/movie/coming">即将上映</NavLink>
        </nav>
        {props.children}
      </div>
    </Provider>
  )
}

export default Movie