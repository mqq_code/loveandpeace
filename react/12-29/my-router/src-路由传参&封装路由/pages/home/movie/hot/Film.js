import {
  withRouter,
  useHistory,
  useLocation
} from 'react-router-dom'

const Film = (props) => {

  // 使用路由包中提供的自定义hook获取路由信息
  const history = useHistory()
  // const location = useLocation()

  const goDetail = () => {
    console.log('跳详情', props.film)
    // 跳转方式一：search传参
    // history.push(`/detail?filmId=${props.film.filmId}&a=100&b=200`)

    // 跳转方式二：params传参,必须先配置
    // history.push(`/detail/${props.film.filmId}`)

    // 跳转方式三：state传参
    history.push({
      pathname: `/detail/${props.film.filmId}`,
      state: {
        a: 100,
        b: 200,
        c: [1,2,3,4,5]
      }
    })

  }

  return (
    <dl onClick={goDetail}>
      <dt><img src={props.film.poster} alt="" /></dt>
      <dd>
        <h3>{props.film.name}</h3>
        <p>{props.film.category}</p>
      </dd>
    </dl>
  )
}

export default Film

// withRouter: 高阶组件
// 给组件的 props 添加路由信息
// export default withRouter(Film)