import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {
  BrowserRouter, // 路由根组件，history 模式， 根组件一个项目中只需要使用一次
  HashRouter // 路由根组件，hash 模式
} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    <App />
  </HashRouter>
);

