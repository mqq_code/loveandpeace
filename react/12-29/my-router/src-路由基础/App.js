import React from 'react'
import './App.css'
import {
  Switch, // 只渲染匹配到的第一个路由
  Route, // 配置路由视图
  Redirect, // 重定向
  Link, // 跳转路由
  NavLink // 跳转路由,有高亮
} from 'react-router-dom'
import Home from './pages/Home'
import City from './pages/City'
import Detail from './pages/Detail'
import NotFound from './pages/404'

const App = () => {
  return (
    <div>
      <header>
        <NavLink activeClassName='checked' activeStyle={{ fontSize: '30px' }} exact to="/">首页</NavLink>
        <NavLink activeClassName='checked' activeStyle={{ fontSize: '30px' }} exact to="/city">城市</NavLink>
        <NavLink activeClassName='checked' activeStyle={{ fontSize: '30px' }} exact to="/detail">详情</NavLink>
      </header>
      <Switch>
        <Route path="/city" component={City} />
        <Route path="/detail" component={Detail} />
        <Route exact path="/" component={Home} />
        <Route path="/404" component={NotFound} />
        <Redirect exact from='/abc' to="/" />
        <Redirect to="/404" />
      </Switch>
    </div>
  )
}

export default App