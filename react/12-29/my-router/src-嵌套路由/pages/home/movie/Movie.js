import React from 'react'
import style from './movie.module.scss'
import classNames from 'classnames'

import {
  NavLink
} from 'react-router-dom'

const Movie = (props) => {
  return (
    <div className={classNames('page', style.movie)}>
      <nav>
        <NavLink activeClassName={style.active} exact to="/movie/hot">正在热映</NavLink>
        <NavLink activeClassName={style.active} exact to="/movie/coming">即将上映</NavLink>
      </nav>
      {props.children}
    </div>
  )
}

export default Movie