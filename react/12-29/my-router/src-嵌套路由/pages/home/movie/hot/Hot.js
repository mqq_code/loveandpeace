import React, { useEffect, useState } from 'react'
import axios from 'axios'
import style from './hot.module.scss'
import Film from './Film'

const Hot = (props) => {
  // props 中包含当前路由信息
  const [list, setList] = useState([])

  const getList = async () => {
    const res = await axios.get('/hot/movie', {
      params: {
        pagesize: 10,
        pagenum: 1
      }
    })
    if (res.data.status === 0) {
      setList(res.data.data.films)
    } else {
      alert(res.data.msg)
    }
  }
  useEffect(() => {
    getList()
  }, [])

  return (
    <div className={style.hot}>
      {list.map(item =>
        <Film key={item.filmId} film={item} />
      )}
    </div>
  )
}

export default Hot