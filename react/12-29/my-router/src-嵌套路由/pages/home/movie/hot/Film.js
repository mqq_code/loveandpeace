import {
  withRouter,
  useHistory,
  useLocation
} from 'react-router-dom'

const Film = (props) => {

  // 使用路由包中提供的自定义hook获取路由信息
  const history = useHistory()
  const location = useLocation()

  const goDetail = () => {
    console.log('跳详情', history)
    history.push('/detail')
  }

  return (
    <dl onClick={goDetail}>
      <dt><img src={props.film.poster} alt="" /></dt>
      <dd>
        <h3>{props.film.name}</h3>
        <p>{props.film.category}</p>
      </dd>
    </dl>
  )
}

export default Film

// withRouter: 高阶组件
// 给组件的 props 添加路由信息
// export default withRouter(Film)