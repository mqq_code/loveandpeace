import Home from '../pages/home/Home'
import City from '../pages/city/City'
import Detail from '../pages/detail/Detail'
import Login from '../pages/login/Login'
import NotFound from '../pages/404'

import Movie from '../pages/home/movie/Movie'
import Cinema from '../pages/home/cinema/Cinema'
import News from '../pages/home/news/News'
import Mine from '../pages/home/mine/Mine'

import Hot from '../pages/home/movie/hot/Hot'
import Coming from '../pages/home/movie/coming/Coming'

const routes = [
  { path: '/login', component: Login, exact: true },
  { path: '/city', component: City, exact: true },
  { path: '/detail', component: Detail, exact: true },
  { path: '/404', component: NotFound, exact: true },
  {
    path: '/', component: Home, children: [
      {
        path: '/movie', component: Movie, children: [
          { path: '/movie/hot', component: Hot, exact: true },
          { path: '/movie/coming', component: Coming, exact: true },
          { from: '/movie', to: '/movie/hot', exact: true },
          { to: '/404' }
        ]
      },
      { path: '/cinema', component: Cinema, exact: true },
      { path: '/news', component: News, exact: true },
      { path: '/mine', component: Mine, exact: true },
      { from: '/', to: '/movie', exact: true },
      { to: '/404' }
    ]
  }
]

export default routes