import { Switch, Route, Redirect } from 'react-router-dom'

const Routerview = (props) => {
  const routes = props.routes.filter(v => v.component)
  const redirect = props.routes.filter(v => v.to)

  return (
    <Switch>
      {routes.map(item =>
        <Route
          key={item.path}
          exact={item.exact}
          path={item.path}
          render={routeInto => {
            const Com = item.component
            return <Com {...routeInto}>
              {item.children && <Routerview routes={item.children} />}
            </Com>
          }}
        />
      )}
      {redirect.map(item =>
        <Redirect key={item.to} from={item.from} to={item.to} exact={item.exact} />
      )}
    </Switch>
  )
}

export default Routerview