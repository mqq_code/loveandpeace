import axios, { AxiosResponse } from 'axios'

axios.defaults.baseURL = 'https://zyxcl-music-api.vercel.app'

export const getToplist = () => {
  return axios.get<any, AxiosResponse<Itoplist, any>>('/toplist')
}

export const playlistDetail = (id: string) => {
  return axios.get<any, AxiosResponse<IPlaylistDetail>>('/playlist/detail', {
    params: {
      id
    }
  })
}