// 排行榜接口
interface Itoplist {
  artistToplist: {
    coverUrl: string;
    name: string;
    position: string;
    upateFrequency: string;
    updateFrequency: string;
  };
  code: number;
  list: {
    ToplistType: string;
    adType: number;
    anonimous: boolean;
    artists: string | null;
    backgroundCoverId: number;
    backgroundCoverUrl: string | null;
    cloudTrackCount: number;
    commentThreadId: string;
    coverImgId: number;
    coverImgId_str: string;
    coverImgUrl: string;
    createTime: number;
    creator: string | null;
    description: string;
    englishTitle: string | null;
    highQuality: boolean;
    id: number;
    name: string;
    newImported: boolean;
    opRecommend: boolean;
    ordered: boolean;
    playCount: number;
    privacy: number;
    recommendInfo: any;
    socialPlaylistCover: string | null;
    specialType: number;
    status: number;
    subscribed: string | null;
    subscribedCount: number;
    subscribers: any[];
    tags: any[];
    titleImage: number;
    titleImageUrl: string | null;
    totalDuration: number;
    trackCount: number;
    trackNumberUpdateTime: number;
    trackUpdateTime: number;
    tracks: any;
    updateFrequency: string;
    updateTime: number;
    userId: number;
  }[]
}

// 歌单详情接口
interface IPlaylistDetail {
  code: number;
  playlist: {
    coverImgUrl: string;
    description: string;
    name: string;
    id: number;
    playCount: number;
    tracks: {
      al: {
        id: number;
        name: string;
        picUrl: string;
      };
      ar: {
        id: number;
        name: string;
      }[];
      id: number;
      name: string;
    }[]
  }
}