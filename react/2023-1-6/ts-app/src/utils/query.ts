
const query = <T,>(search: string): T => {
  const obj: { [key: string]: string } = {}
  const arr = search.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key as string] = val
  })
  return obj as T
}

export default query