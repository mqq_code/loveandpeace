import { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { getToplist } from '../api'

interface IProps extends RouteComponentProps {
  title?: string;
}

// const Home: React.FC<IProps & RouteComponentProps> = (props) => {
const Home: React.FC<IProps> = (props) => {

  const [list, setList] = useState<Itoplist['list']>([])

  useEffect(() => {
    getToplist().then(res => {
      setList(res.data.list)
    })
  }, [])

  const goDetail = (id: number) => {
    // props.history.push('/detail?a=100&b=200&id=' + id)
    props.history.push(`/detail/${id}`)
  }

  return (
    <div className="home">
      <ul>
        {list.map(item =>
          <li key={item.id} onClick={() => goDetail(item.id)}>
            <img src={item.coverImgUrl} alt="" />
            <p>{item.name}</p>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home