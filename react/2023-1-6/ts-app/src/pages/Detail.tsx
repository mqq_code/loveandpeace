import React, { useEffect, useState } from 'react'
import {
  RouteComponentProps,
  useLocation,
  useParams
} from 'react-router-dom'
import { playlistDetail } from '../api'
import query from '../utils/query'


interface IParams {
  id: string;
}

const Detail: React.FC<RouteComponentProps<IParams>> = (props) => {

  // const location = useLocation() // 获取路由信息
  // const params = useParams<IParams>() // 获取动态路由
  const [info, setInfo] = useState<IPlaylistDetail['playlist']>({} as IPlaylistDetail['playlist'])

  useEffect(() => {
    // const qr = query<{a: string, b: string, id: string}>(props.location.search)
    playlistDetail(props.match.params.id).then(res => {
      setInfo(res.data.playlist)
    })
  }, [])


  return (
    <div>
      <img src={info.coverImgUrl} width="100" alt="" />
      <h3>{info.name}</h3>
      <p>{info.description}</p>
      <ul>
        {info.tracks?.map(item =>
          <li key={item.id}>{item.name}</li>
        )}
      </ul>
    </div>
  )
}

export default Detail