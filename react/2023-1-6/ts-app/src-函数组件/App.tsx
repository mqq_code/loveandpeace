import React, { useState, useRef, useEffect } from 'react'
import Child from './components/Child'
import Child1, { IChildRef } from './components/Child1'
import { Provider } from './context/ctx'

const App = () => {

  const [num, setNum] = useState<number>(0)
  const [title, setTitle] = useState('App')
  const [arr, setArr] = useState<string[]>([])
  const inp = useRef<HTMLInputElement>(null)
  const childref = useRef<Child>(null)

  // 获取函数子组件内的数据和方法
  const child1 = useRef<IChildRef>(null)

  const changeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value)
  }
  const add = (e: React.MouseEvent<HTMLButtonElement>) => {
    console.log((e.target as HTMLButtonElement).innerHTML)
    console.log(inp.current?.value)
    setArr([...arr, inp.current!.value])
  }
  useEffect(() => {
    // console.log(childref.current) // 类子组件实例
    console.log(child1.current) // 函数子组件数据
  }, [])


  return (
    <Provider value={{ num, setNum }}>
      <div>
        <h1>{title}</h1>
        <input type="text" value={title} onChange={changeTitle} />
        <div>
          <button onClick={() => setNum(num - 1)}>-</button>
          {num}
          <button onClick={() => setNum(num + 1)}>+</button>
        </div>
        <input type="text" ref={inp} />
        <button onClick={add}>添加</button>
        <ul>
          {arr.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul>
        <hr />
        <Child ref={childref} title={title} />
        <hr />
        <button onClick={() => {
          child1.current?.add()
          console.log(child1.current?.n)
        }}>App +10</button>
        <Child1 ref={child1} title={title} num={num} />
      </div>
    </Provider>
  )
}

export default App
