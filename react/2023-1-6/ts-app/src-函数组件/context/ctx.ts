import { createContext } from 'react'

export interface ICtx {
  num: number;
  setNum: (n: number) => void;
}

const ctx = createContext<ICtx | null>(null)

export const Provider = ctx.Provider

export default ctx