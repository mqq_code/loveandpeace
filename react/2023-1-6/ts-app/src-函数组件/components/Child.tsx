import React, { Component } from 'react'

interface IProps {
  num?: number;
  title: string;
}

interface IState {
  count: number
}

class Child extends Component<IProps, IState> {
  state = {
    count: 100
  }

  changeCount11 = () => {
    console.log('changeCount11')
  }

  render() {
    console.log(this.props)
    return (
      <div>
        <h2>Child</h2>
        <p>num: {this.props.num}</p>
        <p>title: {this.props.title}</p>
      </div>
    )
  }
}

export default Child