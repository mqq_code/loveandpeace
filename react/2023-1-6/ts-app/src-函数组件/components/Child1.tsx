import { useState, forwardRef, useImperativeHandle } from 'react'
import Child2 from './Child2'

interface IProps {
  num: number;
  title: string;
}
export interface IChildRef {
  n: number;
  add: () => void;
}

const Child1: React.ForwardRefRenderFunction<IChildRef, IProps> = (props, ref) => {
  const [n, setN] = useState(0)
  const add = () => {
    setN(n + 10)
  }
  useImperativeHandle(ref, () => {
    // 把return的数据赋值给ref的current属性
    return {
      n,
      add
    }
  }, [n])
  return (
    <div>
      <h2>{props.title}</h2>
      <button onClick={add}>+10</button> {n}
      <hr />
      <Child2 />
    </div>
  )
}

// 转发父组件传过来的ref
export default forwardRef(Child1)


// const Child1: React.FC<IProps> = (props) => {
//   const [n, setN] = useState(0)
//   const add = () => {
//     setN(n + 10)
//   }
//   return (
//     <div>
//       <h2>{props.title}</h2>
//       <button onClick={add}>+10</button> {n}
//     </div>
//   )
// }

// export default Child1