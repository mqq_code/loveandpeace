import React, { useContext } from 'react'
import ctx from '../context/ctx'

const Child2 = () => {

  const ctxVal = useContext(ctx)

  console.log(ctxVal)

  return (
    <div>
      <h3>Child2</h3>
      {ctxVal?.num}
      <button onClick={() => {
        ctxVal?.setNum(ctxVal?.num + 1)
      }}>+</button>
    </div>
  )
}

export default Child2