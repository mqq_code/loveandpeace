import React, { Component, createRef } from 'react'
import Child from './components/Child'

interface IState {
  num: number;
  title: string;
  arr: string[];
}

class App extends Component<{}, IState> {

  state = {
    num: 0,
    title: 'App',
    arr: []
  }
  inp = createRef<HTMLInputElement>()

  changeCount = (n: number) => {
    this.setState({
      num: this.state.num + n
    })
  }
  add = (e: React.MouseEvent<HTMLButtonElement>) => {
    console.log((e.target as HTMLButtonElement).innerHTML)
    console.log(this.inp.current?.value)
    this.setState({
      arr: [...this.state.arr, this.inp.current!.value]
    })
  }
  changeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
    this.setState({
      title: e.target.value
    })
  }

  childref = createRef<Child>()
  componentDidMount(): void {
    // 获取 Child 组件实例
    console.log(this.childref.current)
  }
  render() {
    return (
      <div>
        <h1>{this.state.title}</h1>
        <input type="text" value={this.state.title} onChange={this.changeTitle} />
        <div>
          <button onClick={() => this.changeCount(-1)}>-</button>
          {this.state.num}
          <button onClick={() => this.changeCount(1)}>+</button>
        </div>
        <input type="text" ref={this.inp} />
        <button onClick={this.add}>添加</button>
        <ul>
          {this.state.arr.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul>
        <hr />
        <Child ref={this.childref} title={this.state.title} />
      </div>
    )
  }
}

export default App
