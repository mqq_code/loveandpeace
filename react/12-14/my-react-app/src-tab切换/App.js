import React from 'react'

// react 16.8 版本之前，想定义组件的状态只能使用类组件，函数式组件只用作纯渲染
// 16.8之后 react 新增了 hooks, 函数式组件也拥有了定义状态的能力，可以完全代替类组件的功能
// 类组件
class App extends React.Component {
  state = {
    tablist: [
      {
        title: '许嵩',
        songs: ['素颜', '断桥残雪', '清明雨上', '玫瑰花的葬礼', '你若成风']
      },
      {
        title: '徐良',
        songs: ['坏女孩', '和平分手', '红妆', '犯贱', '客官不可以']
      },
      {
        title: '汪苏泷',
        songs: ['不分手的恋爱', '有点甜', '万有引力', '巴赫旧约', '后会无期']
      }
    ],
    curIndex: 2
  }

  render() {
    const { tablist, curIndex } = this.state
    return (
      <div>
        <nav>
          {tablist.map((item, index) =>
            <span
              key={item.title}
              className={curIndex === index ? 'active' : ''}
              onClick={() => this.setState({ curIndex: index })}
            >
              {item.title}
            </span>
          )}
        </nav>
        <main>
          <ul>
            {tablist[curIndex].songs.map(item => <li key={item}>{item}</li>)}
          </ul>
        </main>
      </div>
    )
  }
}

export default App


// 函数式组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App