import React from 'react'

// react 16.8 版本之前，想定义组件的状态只能使用类组件，函数式组件只用作纯渲染
// 16.8之后 react 新增了 hooks, 函数式组件也拥有了定义状态的能力，可以完全代替类组件的功能
// 类组件
class App extends React.Component {
  // 定义数据
  state = {
    num: 20,
    flag: false,
    arr: []
  }

  sub = () => {
    // 同时调用多次 setState，会合并成一次执行
    // this.setState({ num: this.state.num - 1 })
    // this.setState({ num: this.state.num - 3 })
    // this.setState({ num: this.state.num - 4 })
    // this.setState({ num: this.state.num - 5, flag: true })
    // this.setState({ num: this.state.num - 2 })
    // console.log('点击了减号', this.state)

    this.setState(state => {
      // 如果更新组件时想要获取最新的 state，第一个参数可以传入函数，函数的参数就是最新的数据，把返回值跟原本的state进行合并
      return {
        num: state.num - 1
      }
    })
    this.setState(state => ({ num: state.num - 1 }))
    this.setState(state => ({ num: state.num - 1 }))

    console.log(this.state.num)
  }

  add = () => {
    // react 中不可以直接修改原数据，页面不会自动更新，更新组件需要调用 this.setState 方法
    // setState 会把传入的对象和原来的对象进行合并，然后更新页面
    // this.setState({} | (newState) => {}, callback)
    this.setState({ num: this.state.num + 1 }, () => {
      // 组件更新后的回调函数，可以获取最新的数据，类似 vue 中的 this.$nextTick
      console.log('组件更新成功，获取最新的数据', this.state.num)
    })

    // setState 是异步更新，所以此处无法获取到修改后的数据
    console.log('点击了+', this.state.num)
  }

  render() {
    // 函数中的 this 指向组件实例
    const { num, flag, arr } = this.state
    console.log('render函数')
    return (
      <div>
        <h1>类组件{flag ? 'true' : 'false'}</h1>
        {/* 在react中所有的事件都必须是小驼峰，onClick、onChange、onInput、onKeyDown */}
        {num > 0 &&
          <>
            <button disabled={num <= 0} onClick={this.sub} >-</button>
            {num}
          </>
        }
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}

export default App


// 函数式组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App