import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css'
import logo from './logo.png'

// react中创建 react元素（虚拟dom）
// let box = React.createElement('div', { className: 'box' }, [
//   React.createElement('h1', null, ['开始学习', React.createElement('b', null, ['React!'])]),
//   React.createElement('p', null, ['我是一行描述']),
//   React.createElement('ul', null, [
//     React.createElement('li', null, ['1111111']),
//     React.createElement('li', null, ['2222222']),
//     React.createElement('li', null, ['3333333']),
//     React.createElement('li', null, ['4444444']),
//     React.createElement('li', null, ['5555555'])
//   ])
// ])

// jsx： js + xml，在js文件中使用xml语法，jsx 本质上是 React.createElement 的语法糖
// 通过babel把 jsx 转译成 React.createElement
// 使用jsx简化排版
// let box = <div>
//   <h1>开始学习<b>React!</b></h1>
//   <p>我是一行描述</p>
//   <ul>
//     <li>00001</li>
//     <li>00002</li>
//     <li>00003</li>
//     <li>00004</li>
//     <li>00005</li>
//   </ul>
// </div>
// console.log(box)

const styleObj = {
  color: 'red',
  fontSize: '40px'
}
const title = '开始学习 =======> '
const sum = (a, b) => {
  return (a + b).toFixed(2)
}
const num = 100

const renderNum = (n) => {
  if (n === 100) {
    return <b>满分💯</b>
  }
  return <i>差点满分</i>
}
const numDom = num === 100 ? <b>满分💯</b> : <i>差点满分</i>

const arr = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
const arr1 = [
  <li key="a">a</li>,
  <li key="b">b</li>,
  <li key="c">c</li>,
  <li key="d">d</li>
]

const root = ReactDOM.createRoot(document.getElementById('root'));
// 注意事项
// 所有的 class 改成 className, for 改成 htmlFor
// style 必须写对象
// 在 jsx 中使用 js 表达式，必须使用 {} 包起来
// 在 react 中不可以直接渲染对象
root.render(
  <div className='box'>
    <h1 style={styleObj}>{title}<b>React!</b></h1>
    <p style={{ color: 'orange' }}>我是一行描述</p>
    <div>string: {'123abc'}</div>
    <div>number: {100}</div>
    <div>boolean: {false}</div>
    <div>null: {null}</div>
    <div>undefined: {undefined}</div>
    <div>array: {[1,2,3,4,5,6,7]}</div>
    {/* <div>object: {{ name: '123', age: 100 }}</div> */}
    <div>函数返回值：{sum(1, 2)}</div>
    <hr />
    <h3>使用本地图片</h3>
    <img src={logo} alt="" />
    <hr />
    <h3>条件判断</h3>
    <div>三元：{num === 100 ? <b>满分💯</b> : <i>差点满分</i>}</div>
    <div>三元: {numDom}</div>
    <div>函数中判断：{renderNum(num)}</div>
    <hr />
    <h3>循环</h3>
    <ol>
      {arr.map(item =>
        <li key={item}>{item}</li>
      )}
    </ol>
    <ul>
      {arr1}
    </ul>

    {/* <ul>
      <li>00001</li>
      <li>00002</li>
      <li>00003</li>
      <li>00004</li>
      <li>00005</li>
    </ul>
    <label htmlFor="checkAll">全选</label>
    <input type="checkbox" id='checkAll' /> */}
  </div>
);
