import React, { createRef } from 'react'


// 类组件
class App extends React.Component {
  state = {
    num: 0
  }

  title = null
  inp = createRef() // 创建一个ref对象

  submit = () => {
    // console.log(this.title)
    console.log(this.inp.current.value)
  }

  render() {
    return (
      <div>
        {/* 获取dom方式一： 每次组件更新都会执行一次函数赋值 */}
        <h1 ref={el => this.title = el}>标题</h1>
        {/* 获取dom方式二： 组件更新去改变ref对象的current属性 */}
        <input type="text" ref={this.inp} />
        <button onClick={this.submit}>获取dom</button>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>+</button> {this.state.num}
      </div>
    )
  }
}

export default App


// 函数式组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App