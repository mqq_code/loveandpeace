import React, { createRef } from 'react'


// 类组件
class App extends React.Component {
  state = {
    form: {
      user: '小明',
      age: 100,
      sex: '男',
      hobby: '吃饭',
      address: '北京',
      nation: '汉',
      remark: '备注备注'
    }
  }

  changeForm = (key, e) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: e.target.value
      }
    })
  }

  render() {
    const { form } = this.state
    return (
      <div>
        {/* 受控组件：受数据控制的组件 */}
        <p>姓名: <input type="text" value={form.user} onChange={e => this.changeForm('user', e)}/></p>
        <p>年龄: <input type="text" value={form.age} onChange={e => this.changeForm('age', e)}/></p>
        <p>性别: <input type="text" value={form.sex} onChange={e => this.changeForm('sex', e)}/></p>
        <p>爱好: <input type="text" value={form.hobby} onChange={e => this.changeForm('hobby', e)}/></p>
        <p>地址: <input type="text" value={form.address}onChange={e => this.changeForm('address', e)} /></p>
        <p>民族: <input type="text" value={form.nation} onChange={e => this.changeForm('nation', e)}/></p>
        <p>备注: <input type="text" value={form.remark} onChange={e => this.changeForm('remark', e)}/></p>
        {JSON.stringify(this.state.form)}
      </div>
    )
  }
}

export default App


// 函数式组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App