import React from 'react'

// react 16.8 版本之前，想定义组件的状态只能使用类组件，函数式组件只用作纯渲染
// 16.8之后 react 新增了 hooks, 函数式组件也拥有了定义状态的能力，可以完全代替类组件的功能
// 类组件
class App extends React.Component {
  state = {
    num: 20,
    flag: false,
    arr: ['a', 'b', 'c']
  }
  inp = null
  changeCount = (n) => {
    console.log('修改num', n)
    this.setState({ num: this.state.num + n })
  }
  add = () => {
    this.setState({
      arr: [...this.state.arr, this.inp.value]
    })
    this.inp.value = ''
  }
  remove = (index) => {
    this.setState({
      arr: this.state.arr.filter((v, i) => i !== index)
    })
  }

  render() {
    const { num, flag, arr } = this.state
    return (
      <div>
        <h1>类组件{flag ? 'true' : 'false'}</h1>
        <div>
          {num > 0 &&
            <>
              <button disabled={num <= 0} onClick={() => this.changeCount(-1)} >-</button>
              {num}
            </>
          }
          {/* 想要给事件的函数传参数，必须套一层箭头函数 */}
          <button onClick={() => this.changeCount(1)}>+</button>
        </div>
        <input type="text" ref={el => this.inp = el} />
        <button onClick={this.add}>添加数据</button>
        <ul>
          {arr.map((item, index) =>
            <li key={item}>{item} <button onClick={() => this.remove(index)}>删除</button></li>
          )}
        </ul>
      </div>
    )
  }
}

export default App


// 函数式组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App