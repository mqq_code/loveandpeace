import { Redirect } from 'react-router-dom'

export default (Com) => {

  return (props) => {
    let token = localStorage.getItem('token')
    if (token) {
      return <Com {...props} />
    }

    return <Redirect to={`/login?redirectPath=${encodeURIComponent(window.location.href)}`} />
  }

}
