import React, { lazy, Suspense } from 'react'
// import Text from './Text'

// 异步加载组件，必须配合 Suspense 使用
const Text = lazy(() => import(/* webpackChunkName: 'text' */'./Text'))

const Cinema = () => {
  return (
    /* fallback： Suspense内的异步未加载成功时显示的后备内容 */
    <div style={{ background: 'orange' }}>
      <h1>影院</h1>
      <Suspense fallback={<div>加载中。。。</div>}>
        <Text />
      </Suspense>
    </div>
  )
}

export default Cinema