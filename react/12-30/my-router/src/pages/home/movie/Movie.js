import React, { useState } from 'react'
import style from './movie.module.scss'
import classNames from 'classnames'
import { NavLink, Link } from 'react-router-dom'
import { Provider } from '../../../context/movieCtx'

const Movie = (props) => {
  const [isDown, setIsDown] = useState(false)
  const [show, setShow] = useState(false)
  const scroll = (e) => {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    if (scrollTop + clientHeight + 1 >= scrollHeight) {
      setIsDown(true)
    }
    if (scrollTop > 400) {
      setShow(true)
    } else {
      setShow(false)
    }
  }

  return (
    <Provider value={{ isDown, setIsDown }}>
      <div className={classNames('page', style.movie)} onScroll={scroll}>
        <nav>
          <NavLink activeClassName={style.active} exact to="/movie/hot">正在热映</NavLink>
          <NavLink activeClassName={style.active} exact to="/movie/coming">即将上映</NavLink>
        </nav>
        {props.children}
        <div className={classNames(style.fixedHeader, { [style.show]: show })}>
          <div className={style.title}>
            <div className={style.city}>
              <Link to="/city">北京</Link>
            </div>
            <h2>电影</h2>
          </div>
          <nav>
            <NavLink activeClassName={style.active} exact to="/movie/hot">正在热映</NavLink>
            <NavLink activeClassName={style.active} exact to="/movie/coming">即将上映</NavLink>
          </nav>
        </div>
      </div>
    </Provider>
  )
}

export default Movie