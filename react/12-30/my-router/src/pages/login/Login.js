import React, { useState } from 'react'
import axios from 'axios'

const query = str => {
  const arr = str.slice(1).split('&')
  const obj = {}
  for (let val of arr) {
    const [key, v] = val.split('=')
    obj[key] = decodeURIComponent(v)
  }
  return obj
}

const Login = (props) => {
  const [form, setForm] = useState({ user: '', pwd: '' })
  const qs = query(props.location.search)
  const change = (key, e) => {
    setForm({
      ...form,
      [key]: e.target.value
    })
  }
  const login = async () => {
    const res = await axios.post('/api/login', form)
    if (res.data.status === 0) {
      localStorage.setItem('token', res.data.data.token)
      window.location.href = qs.redirectPath || '/'
    } else {
      alert(res.data.msg)
    }
  }
 

  return (
    <div>
      <p><input type="text" placeholder="用户名" value={form.user} onChange={e => change('user', e)} /></p>
      <p><input type="password" placeholder="密码" value={form.pwd} onChange={e => change('pwd', e)} /></p>
      <button onClick={login}>登陆</button>
    </div>
  )
}

export default Login