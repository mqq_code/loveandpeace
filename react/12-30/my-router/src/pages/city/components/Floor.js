import React, { useRef, useState, useEffect } from 'react'
import style from '../city.module.scss'
import classNames from 'classnames'

const Floor = ({ floor }) => {

  const [tip, setTip] = useState('') // 提示跳转的楼层
  const timer = useRef()
  const floorRef = useRef() // 楼层父元素

  // 跳转楼层
  const jump = (index, type) => {
    floorRef.current.scrollTop = floorRef.current.children[index].offsetTop
    setTip(type)
    if (timer.current) clearTimeout(timer.current)
    timer.current = setTimeout(() => {
      setTip('')
    }, 2000)
  }

  return (
    <div className={style.cities}>
      <div className={style.floor} ref={floorRef}>
        {floor.map(item =>
          <div key={item.type} className={classNames(style.floorItem, { [style.hot]: item.isHot === 1 })}>
            <h4>{item.type}</h4>
            <ul>
              {item.list.map(val =>
                <li key={val.cityId}>{val.name}</li>
              )}
            </ul>
          </div>
        )}
      </div>
      <div className={style.floorNav}>
        {floor.map((item, index) =>
          index > 0 ? <p key={item.type} onClick={() => jump(index, item.type)}>{item.type}</p> : null
        )}
      </div>
      <div className={classNames(style.tip, { [style.showTip]: tip })}>{tip}</div>
    </div>
  )
}

export default Floor