import { useState, useEffect } from 'react'



const App = () => {
  const [num, setNum] = useState(0)

  const getCode = () => {
    setNum(60)
    const timer = setInterval(() => {
      setNum(n => {
        if (n - 1 <= 0) {
          clearInterval(timer)
        }
        return n - 1
      })
    }, 1000)
  }

  const test = () => {
    console.log(num)
  }

  return (
    <div className="App">
      <button disabled={num > 0} onClick={getCode}>{num > 0 ? `${num}s后重新获取` : '获取短信验证码'}</button>
      <button onClick={test}>test</button>
    </div>
  );
}
export default App;

// 定时器中使用state变量
// const App = () => {
//   const [num, setNum] = useState(0)

//   useEffect(() => {
//     setInterval(() => {
//       setNum(n => n + 1)
//     }, 1000)
//   }, [])

//   return (
//     <div className="App">
//       定时器：{num}
//       {/* <button onClick={() => setNum(num + 1)}>+</button> */}
//     </div>
//   );
// }

// export default App;

