import React, { useEffect, useRef } from 'react'

// 使用useRef操作dom
const App = () => {
  const title = useRef(null)
  const inp = useRef(null)
  useEffect(() => {
    console.log(title.current)
  }, [])
  const search = () => {
    console.log(inp.current.value)
  }

  return (
    <div>
      <h1 ref={title}>App</h1>
      <input type="text" ref={inp} />
      <button onClick={search}>搜索</button>
    </div>
  )
}

export default App