import { useState, useEffect } from 'react'

// useEffect: 处理组件中的副作用操作（调接口、定时器、操作dom）
// 可以实现类似 class 中生命周期的功能

const Child = (props) => {

  useEffect(() => {
    const listener =  () => {
      console.log('Child click')
    }
    document.addEventListener('click', listener)
    console.log('Child 组件显示了')

    return () => {
      // 组件销毁时执行，类似 class 中的 componentWillUnmount
      document.removeEventListener('click', listener)
      console.log('Child 组件销毁了')
    }
  }, [])

  useEffect(() => {
    console.log('props.parentNum 改变了', props.parentNum)
  }, [props.parentNum])

  return <div>
    <h2>Child组件</h2>
  </div>
}


const App = () => {

  const [num, setNum] = useState(0)
  const [show, setShow] = useState(true)
  const [obj, setObj] = useState({ name: '小明', age: 20 })

  // 依赖项传入空数组，实现类似 componentDidMount 功能，只执行一次
  useEffect(() => {
    // 可以调用接口、操作dom、定时器
    console.log('类似 componentDidMount')
    console.log(document.querySelector('b'))
  }, [])

  // 不传入依赖项，页面更新就会执行，类似 componentDidUpdate
  useEffect(() => {
    console.log('没传入第二个参数')
    console.log(document.querySelector('b'))
  })

  // 传入依赖项，依赖项更新时执行函数
  useEffect(() => {
    console.log('num 更新了', num)
    console.log('show 更新了', show)
  }, [num, show])

  useEffect(() => {
    console.log('obj.name 更新了', obj)
  }, [obj])


  return (
    <div className="App">
      <button onClick={() => setNum(num + 1)}>+</button>
      <b>{num}</b>
      <hr />
      <button onClick={() => setShow(!show)}>显示隐藏</button>
      {show && <Child parentNum={num} />}
      <hr />
      <div>
        <p>
          姓名: {obj.name}
          <input type="text" value={obj.name} onChange={e => setObj({ ...obj, name: e.target.value })} />
        </p>
        <p>
          年龄: {obj.age}
          <input type="text" value={obj.age} onChange={e => setObj({ ...obj, age: e.target.value })} />
        </p>
      </div>
    </div>
  );
}

export default App;

