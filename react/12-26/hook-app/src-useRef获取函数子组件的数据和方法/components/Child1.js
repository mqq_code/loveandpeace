import React, { useState, forwardRef, useImperativeHandle } from 'react'

const Child1 = (props, ref) => {
  const [num, setNum] = useState(0)

  // ref: 接收父组件传入的ref
  // useImperativeHandle: 给传过来的ref对象添加内容
  useImperativeHandle(ref, () => {
    // 此函数返回值会赋值给 ref
    return {
      num,
      add
    }
  }, [num])

  const add = () => {
    setNum(num + 1)
  }
  return (
    <div>
      <h1>Child1 - {num}</h1>
      <button onClick={add}>+</button>
    </div>
  )
}

// 把父组件传过来的ref转发给Child1
export default forwardRef(Child1)
