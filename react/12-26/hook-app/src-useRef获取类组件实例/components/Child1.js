import React, { Component } from 'react'

export default class Child1 extends Component {

  state = {
    num: 0
  }

  add = () => {
    this.setState({
      num: this.state.num + 1
    })
  }

  render() {
    return (
      <div>
        <h1>Child1 - {this.state.num}</h1>
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}
