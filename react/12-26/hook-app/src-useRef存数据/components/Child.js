import React, { useEffect, useRef, createRef, useState } from 'react'

// 使用useRef 存和页面渲染无关的数据
const Child = () => {
  const [flag, setFlag] = useState(false)

  // ref 创建的对象在整个生命周期内始终指向同一个地址，可以存不影响页面更新的数据
  // ref.current 改变不会引起组件的更新，类似class中的实例属性
  const count = useRef(0)

  useEffect(() => {
    setInterval(() => {
      console.log(1)
      count.current++
    }, 1000)
  }, [])

  console.log('App更新了')

  return (
    <div>
      <button onClick={() => {
        alert(count.current)
      }}>按钮点击的次数</button>
      <button onClick={() => setFlag(!flag)}>{flag ? '显示' : '隐藏'}</button>
    </div>
  )
}

export default Child




// import React, { Component } from 'react'

// export default class Child extends Component {
//   state = {
//     num: 0
//   }
//   count = 10
//   render() {
//     return (
//       <div>Child</div>
//     )
//   }
// }
