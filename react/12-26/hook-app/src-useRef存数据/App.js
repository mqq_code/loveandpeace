import React, { useState } from 'react'
import Child from './components/Child'

const App = () => {
  const [show, setShow] = useState(false)

  return (
    <div>
      <h1>App</h1>
      <hr />
      <Child />
      <hr />
      <button onClick={() => setShow(!show)}>show Child2</button>
      {show && <Child />}
    </div>
  )
}

export default App