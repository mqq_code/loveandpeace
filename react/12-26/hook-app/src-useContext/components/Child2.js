import React, { useContext } from 'react'
import listCtx from '../context/list'

const Child2 = () => {
  // 获取父级组件context的数据
  const list = useContext(listCtx)
  
  return (
    <div style={{ border: '5px solid tomato', padding: '20px', color: 'orange' }}>
      Child2
      <ul>
        {list.map(item => <li key={item}>{item}</li>)}
      </ul>
    </div>
  )
}

export default Child2