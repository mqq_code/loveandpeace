import React, { useContext } from 'react'
import Child2 from './Child2'
import listCtx from '../context/list'

const Child1 = () => {

  const value = useContext(listCtx)

  return (
    <div style={{ border: '5px solid tomato', padding: '20px', color: 'tomato' }}>
      <h2>Child1</h2>
      <ol>
        {value.map(item => <li key={item}>{item}</li>)}
      </ol>
      <Child2 />
    </div>
  )
}

export default Child1