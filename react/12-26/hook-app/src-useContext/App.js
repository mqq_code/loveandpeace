import { useState } from 'react'
import Child1 from './components/Child1'
import { Provider } from './context/list'

const App = () => {
  const [arr, setArr] = useState([0, 1])
  const add = () => {
    setArr([...arr, arr.length])
  }

  return (
    <Provider value={arr}>
      <div className="App">
        <h1>App</h1>
        <button onClick={add}>add</button>
        <ul>
          {arr.map(item => <li key={item}>{item}</li>)}
        </ul>
        <Child1 />
      </div>
    </Provider>
  );
}
export default App
