import React, { useEffect, useRef, createRef, useState } from 'react'

// useRef 和 createRef的区别
const App = () => {
  const [num, setNum] = useState(0)

  const useref = useRef(null) // 组件初始化时创建一个ref对象，后续组件更新时 ref 的指向不变
  const createref = createRef(null) // 组件每更新一次就会创建一个新的ref对象

  useEffect(() => {
    useref.current = 'useref'
    createref.current = 'createref'
    console.log('useref', useref)
    console.log('createref', createref)
  }, [])

  return (
    <div>
      <button onClick={() => setNum(num + 1)}>+</button> {num}

      <button onClick={() => {
        console.log('useref', useref)
        console.log('createref', createref)
      }}>打印ref对象</button>
    </div>
  )
}

export default App