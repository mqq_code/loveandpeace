import React, { Component } from 'react'
import './App.scss'
import axios from 'axios'
import Header from './components/header/Header'
import Mark from './components/mark/Mark'
import Bottle from './components/bottle/Bottle'

class App extends Component {
  state = {
    list: [], // 存所有数据
    headerList: [], // 选中的数据
    showMark: false
  }
  async componentDidMount () {
    const res = await axios.get('/list')
    this.setState({ list: res.data })
  }
  change = (title) => {
    let headerList = [...this.state.headerList] // 拷贝头部数据
    const list = [...this.state.list] // 拷贝列表数据
    const item = list.find(v => v.title === title) // 根据title查找当前点击的对象
    // 选中的数量大于等于3并且当前点击的瓶子还未选中就停止执行
    if (headerList.length >= 3 && !item.active) {
      return
    }
    item.active = !item.active // 修改当前高亮
    if (item.active) { // 如果选中push到header
      headerList.push(item)
    } else { // 如果取消选中，就从header中删除
      headerList = headerList.filter(v => v.title !== title)
    }
    this.setState({ list, headerList }) // 更新页面
  }
  changeMark = (show) => {
    this.setState({
      showMark: show
    })
  }
  render() {
    const { list, headerList, showMark } = this.state
    return (
      <div className="wrap">
        <main>
          {list.map((item, index) =>
            <Bottle key={item.title} {...item} change={this.change} />
          )}
        </main>
        {headerList.length > 0 && <Header list={headerList} remove={this.change} compare={this.changeMark} />}
        {showMark && <Mark list={headerList} close={() => this.changeMark(false)} />}
      </div>
    )
  }
}

export default App
