import React, { Component } from 'react'
import classNames from 'classnames'
import style from './index.module.scss' // 使用 css-module 让组件内样式不影响全局样式

class Bottle extends Component {
  render() {
    return (
      <div className={classNames(style.bottle, { [style.active]: this.props.active })}>
        <i
          className={classNames(style.icon, 'iconfont', this.props.active ? 'icon-duigou'  : 'icon-jiajianzujianjiahao')}
          onClick={() => this.props.change(this.props.title)}
        ></i>
        <img src={this.props.src} alt="" />
        <h3>{this.props.title}</h3>
        <p>{this.props.price}</p>
        <div className={style.btn}>add to cart</div>
      </div>
    )
  }
}

export default Bottle
