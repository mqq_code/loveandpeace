import React, { Component } from 'react'
import style from './index.module.scss'
import classNames from 'classnames'
console.log(style)

export default class Header extends Component {
  render() {
    const { list, remove, compare } = this.props
    return (
      <header className={style.header}>
        <ul>
          {list.map(item =>
            <li key={item.title}>
              <img src={item.src} />
              <i className={classNames('iconfont icon-icon_close', style.icon)}onClick={() => remove(item.title)}></i>
            </li>  
          )}
          {list.length < 3 && <li></li>}
        </ul>
        <button disabled={list.length < 2} onClick={() => compare(true)}>
          <i className='iconfont icon-duigou'></i>
          compare
        </button>
      </header>
    )
  }
}
