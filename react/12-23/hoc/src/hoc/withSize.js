import React, { Component, forwardRef } from 'react'

const withSize = (ReatComponent) => {
  class WithSize extends Component {
    state = {
      size: 'big'
    }
  
    getSize = () => {
      const clientWidth = document.documentElement.clientWidth
      // console.log(clientWidth)
      if (clientWidth >= 1000) {
        this.setState({ size: 'big' })
      } else if (clientWidth >= 500 && clientWidth < 1000) {
        this.setState({ size: 'middle' })
      } else {
        this.setState({ size: 'small' })
      }
    }
  
    componentDidMount () {
      window.addEventListener('resize', this.getSize)
      this.getSize()
    }
  
    componentWillUnmount () {
      window.removeEventListener('resize', this.getSize)
    }

    render() {
      // 接收 forwardRef 传过来的ref对象，透传给 ReatComponent
      const { forwardRef, ...other } = this.props
      return (
        <div style={{ background: 'red' }}>
          <ReatComponent ref={forwardRef} size={this.state.size} {...other} />
        </div>
      )
    }
  }

  // 获取ref对象，转发给给高阶组件
  return forwardRef((props, ref) => {
    return <WithSize forwardRef={ref} {...props} />
  })
}

// 处理高阶组件中的ref
// 1. 被高阶组件包裹的组件无法通过 ref 获取组件实例对象
// 2. 需要在高阶组件中获取 ref 对象传过去
// 3. 高阶组件无法直接获取 ref，需要使用 forwardRef 获取 ref 然后再透传

export default withSize