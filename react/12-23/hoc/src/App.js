import React, { Component, createRef } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import './App.scss'

class App extends Component {

  child = createRef()

  componentDidMount() {
    // 通过ref获取组件实例
    console.log(this.child.current)
  }

  render() {
    return (
      <div className='wrap'>
        <h1>App</h1>
        <Child1 ref={this.child} title="你是child1" title1={1111} />
        <Child2 />
      </div>
    )
  }
}

export default App