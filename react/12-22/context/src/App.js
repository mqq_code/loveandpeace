import React, { Component } from 'react'
import './App.scss'
import Left from './components/Left'
import Right from './components/Right'
import { Provider } from './context/ctx'

export default class App extends Component {
  state = {
    num: 100
  }
  add = () => {
    this.setState({
      num: this.state.num + 1
    })
  }
  render() {
    const params = {
      num: this.state.num,
      add: this.add
    }
    return (
      // 提供数据给所有后代组件使用
      <Provider value={params}>
        <div className='wrap'>
          <h1>App</h1>
          <main>
            <Left />
            <Right />
          </main>
        </div>
      </Provider>
    )
  }
}
