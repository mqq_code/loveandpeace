import React, { Component } from 'react'
import { Consumer } from '../context/ctx'
import { Consumer as ColorConsumer } from '../context/color'

export default class LeftChildChild extends Component {

  renderNode = (color) => {
    return (value) => {
      return (
        <div className='leftChild' style={{ borderColor: color }}>
          <h2>LeftChildChild.js</h2>
          <button onClick={value.add}>+</button>
        </div>
      )
    }
  }

  render() {
    return (
      <ColorConsumer>
        {color => (
          <Consumer>
            {this.renderNode(color)}
          </Consumer>
        )}
      </ColorConsumer>
    )
  }
}
