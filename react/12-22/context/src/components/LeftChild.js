import React, { Component } from 'react'
import LeftChildChild from './LeftChildChild'
import { Consumer } from '../context/ctx'
import { Consumer as ColorConsumer } from '../context/color'

export default class LeftChild extends Component {
  render() {
    return (
      <ColorConsumer>
        {color => (
          <Consumer>
            {value => (
              <div className='leftChild' style={{ borderColor: color }}>
                <h2>LeftChild.js</h2>
                <button onClick={value.add}>+</button>
                <LeftChildChild />
              </div>
            )}
          </Consumer>
        )}
      </ColorConsumer>
    )
  }
}
