import React, { Component } from 'react'
import LeftChild from './LeftChild'
import { Consumer } from '../context/ctx'
import { Provider } from '../context/color'

export default class Left extends Component {

  state = {
    color: '#333'
  }
  changeColor = e => {
    this.setState({
      color: e.target.value
    })
  }

  render() {
    return (
      <Provider value={this.state.color}>
        <Consumer>
          {value => (
            <div className='left'>
              <input type="color" value={this.state.color} onChange={this.changeColor} /> {this.state.color}
              <h2>Left.js</h2>
              <button onClick={value.add}>+</button>
              <LeftChild />
            </div>
          )}
        </Consumer>
      </Provider>
    )
  }
}
