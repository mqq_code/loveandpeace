import React, { Component } from 'react'
import { Consumer } from '../context/ctx'

export default class RightChild extends Component {
  shouldComponentUpdate() {
    return false
  }
  render() {
    return (
      // Consumer 内元素不受 shouldComponentUpdate 影响
      <Consumer>
        {value => {
          return (
            <div className='rightchild'>
              <h2>RightChild.js</h2>
              <p>分数: {value.num}</p>
            </div>
          )
        }}
      </Consumer>
    )
  }
}
