import React, { Component } from 'react'
import RightChild from './RightChild'

export default class Right extends Component {
  shouldComponentUpdate() {
    return false
  }
  render() {
    return (
      <div className='right'>
        <h2>Right.js</h2>
        <RightChild />
      </div>
    )
  }
}
