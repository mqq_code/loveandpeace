import React, { Component, createRef } from 'react'
import './App.scss'
import axios from 'axios'
import Keyboard from './components/keyboard/Keyboard'
import Controller from './components/controller/Controller'
export default class App extends Component {

  state = {
    list: [], // 所有数据
    curMusic: {}, // 当前按下的按钮
    flag: true, // power
    bank: false  // bank
  }
  audio = createRef()

  changeMusic = (music) => {
    // 按钮开启允许播放音乐
    if (this.state.flag) {
      this.setState({
        curMusic: music
      })
      this.audio.current.load()
    }
  }
  changeFlag = () => {
    if (this.state.flag) {
      this.setState({
        flag: false,
        curMusic: {}
      })
    } else {
      this.setState({
        flag: true
      })
    }
  }
  changeVolume = (volume) => {
    console.log(volume)
    this.audio.current.volume = volume
  }
  changeBank = () => {
    this.setState({ bank: !this.state.bank, curMusic: {} })
  }

  async componentDidMount () {
    const res = await axios.get('/list')
    this.setState({ list: res.data })
    const list = res.data.map(item => [item.url, item.bankUrl]).flat()
    this.loadAudio(list)
  }
  loadAudio = (data) => {
    data.forEach(item => {
      const audio = new Audio()
      audio.src = item
      audio.addEventListener('canplay', () => {
        console.log('加载成功', item)
      })
    })
  }

  render() {
    const { list, curMusic, flag, bank } = this.state
    const url = bank ? curMusic.bankUrl : curMusic.url
    return (
      <div className='wrap'>
        <Keyboard list={list} flag={flag} changeMusic={this.changeMusic} />
        <Controller
          curMusic={curMusic}
          flag={flag}
          bank={bank}
          changeBank={this.changeBank}
          changeFlag={this.changeFlag}
          changeVolume={this.changeVolume}
        />
        <audio ref={this.audio} src={url} autoPlay></audio>
      </div>
    )
  }
}
