import React, { Component, createRef } from 'react'
import style from './controller.module.scss'
import classNames from 'classnames'

export default class Controller extends Component {
  state = {
    left: '100%'
  }
  volume = createRef()
  flag = false
  mousedown = () => {
    this.flag = true
  }
  mousemove = (e) => {
    if (this.flag) {
      const rect = this.volume.current.getBoundingClientRect()
      const max = rect.width - 12
      let left = e.clientX - rect.left - 6
      if (left <= 0) {
        left = 0
      } else if (left >= max){
        left = max
      }
      this.setState({ left })
      this.props.changeVolume(left / max)
    }
  }
  mouseup = () => {
    this.flag = false
  }
  componentDidMount () {
    document.addEventListener('mousemove', this.mousemove)
    document.addEventListener('mouseup', this.mouseup)
  }
  componentWillUnmount () {
    document.removeEventListener('mousemove', this.mousemove)
    document.removeEventListener('mouseup', this.mouseup)
  }
  render() {
    const { curMusic, flag, changeFlag, bank, changeBank} = this.props
    return (
      <div className={style.controller}>
        <div className={style.power}>
          <b>power</b>
          <p className={classNames({ [style.on]: flag })} onClick={changeFlag}></p>
        </div>
        <div className={style.song_id}>{curMusic.id}</div>
        <div className={style.volume} ref={this.volume}>
          <i style={{ left: this.state.left }} onMouseDown={this.mousedown}></i>
        </div>
        <div className={style.power}>
          <b>bank</b>
          <p className={classNames({ [style.on]: bank })} onClick={changeBank}></p>
        </div>
      </div>
    )
  }
}
