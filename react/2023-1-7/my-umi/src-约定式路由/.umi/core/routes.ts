// @ts-nocheck
import React from 'react';
import { ApplyPluginsType } from '/Users/zhaoyaxiang/Desktop/文明有爱/react/2023-1-7/my-umi/node_modules/umi/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';

export function getRoutes() {
  const routes = [
  {
    "path": "/detail/:id",
    "exact": true,
    "component": require('@/pages/detail/[id].tsx').default,
    "wrappers": [require('@/wrappers/isAuth').default]
  },
  {
    "path": "/login",
    "exact": true,
    "component": require('@/pages/login/index.tsx').default,
    "title": "登陆"
  },
  {
    "path": "/",
    "routes": [
      {
        "path": "/index/child1",
        "exact": true,
        "component": require('@/pages/index/child1.tsx').default
      },
      {
        "path": "/index/child2",
        "exact": true,
        "component": require('@/pages/index/child2.tsx').default
      }
    ],
    "component": require('@/pages/index/_layout.tsx').default
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
