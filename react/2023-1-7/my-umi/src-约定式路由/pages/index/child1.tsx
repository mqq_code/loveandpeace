import React from 'react'

const Child1: React.FC = (props) => {
  console.log('child1', props)
  return (
    <div>Child1</div>
  )
}

export default Child1