import style from './detail.less'

const Detail = () => {
  return (
    <div className={style.detail}>detail</div>
  )
}

Detail.wrappers = ['@/wrappers/isAuth']
export default Detail