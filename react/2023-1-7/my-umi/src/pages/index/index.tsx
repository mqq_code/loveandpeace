import React from 'react';
import styles from './index.scss';
import { Link, NavLink, history } from 'umi'
import { RouteComponentProps } from 'react-router-dom'
import { Button } from 'antd'

const IndexPage: React.FC<RouteComponentProps> = (props) => {

  const goDetail = () => {
    // props.history.push('/detail')
    history.push('/detail')
  }

  return (
    <div className={styles.home}>
      <h1>Page index</h1>
      <nav>
        <NavLink activeClassName={styles.active} to="/child1">child1</NavLink>
        <NavLink activeClassName={styles.active} to="/child2">child2</NavLink>
        <Button onClick={goDetail}>跳转详情</Button>
      </nav>
      <main>
        {/* 给子路由组件传参数 */}
        {/* {React.Children.map(props.children, child => {
          return React.cloneElement(child, { foo: 'bar' });
        })} */}
        {props.children}
      </main>
    </div>
  );
}

export default IndexPage