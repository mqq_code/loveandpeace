import React, { useEffect, useState } from 'react'
import { Pagination, Select } from 'antd';

const Option = Select.Option

const Login = () => {
  const [pageSize, setPageSize] = useState(10)
  const pageSizeOptions = [10, 20, 30, 50]

  return (
    <div>
      <Pagination
        showQuickJumper
        pageSizeOptions={pageSizeOptions}
        showSizeChanger={true}
        defaultCurrent={1}
        total={500}
        locale={{
          items_per_page: '条/页'
        }}
      />
      {/* <Pagination
        showQuickJumper
        pageSize={pageSize}
        pageSizeOptions={pageSizeOptions}
        showSizeChanger={false}
        defaultCurrent={1}
        total={500}
        showTotal={(total, range) => {
          return (
            <div>
              <Select value={pageSize} onChange={v => setPageSize(v)}>
                  {pageSizeOptions.map((v) => (
                      <Option key={v} value={v}>
                          {v}/每页
                      </Option>
                  ))}
              </Select>
            </div>
        )
        }}
      /> */}
    </div>
  )
}

export default Login