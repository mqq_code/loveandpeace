import { Redirect } from 'umi'

const IsAuth: React.FC = (props) => {
  let token = localStorage.getItem('token')
  if (token) {
    return <>{props.children}</>
  }
  return <Redirect to="/login" />
}

export default IsAuth