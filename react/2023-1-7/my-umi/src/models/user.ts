import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface IndexModelState {
  name: string;
  age: number;
}

export interface IndexModelType {
  namespace: 'user';
  state: IndexModelState;
  effects: {
    query: Effect;
  };
  reducers: {
    // changeName: Reducer<IndexModelState>;
    // 启用 immer 之后
    changeName: ImmerReducer<IndexModelState>;
  };
  subscriptions: { setup: Subscription };
}

const IndexModel: IndexModelType = {
  namespace: 'user',

  state: {
    name: '王小明',
    age: 100
  },

  reducers: {
    // 启用 immer 之后
    changeName(state, action) {
      state.name = action.payload
      // return { ...state, name: action.payload };
    },
  },



  effects: {
    *query({ payload }, { call, put }) {

    },
  },

  
  subscriptions: {
    setup({ dispatch, history }) {
      // return history.listen(({ pathname }) => {
      //   if (pathname === '/') {
      //     dispatch({
      //       type: 'query',
      //     });
      //   }
      // });
    },
  },
};

export default IndexModel;