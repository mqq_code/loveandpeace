export default [
  { exact: true, title: '登陆', path: '/login', component: '@/pages/login/Login' },
  {
    exact: true,
    path: '/detail',
    title: '详情',
    component: '@/pages/detail/detail',
    // 高阶组件
    wrappers: ['@/wrappers/isAuth']
  },
  {
    path: '/',
    component: '@/pages/index/index',
    routes: [
      { exact: true, title: 'child1', path: '/child1', component: '@/pages/index/child1/child1', wrappers: ['@/wrappers/isAuth'] },
      { exact: true, title: 'child2', path: '/child2', component: '@/pages/index/child2/child2', wrappers: ['@/wrappers/isAuth'] },
      { redirect: '/child1' }
    ]
  },
]