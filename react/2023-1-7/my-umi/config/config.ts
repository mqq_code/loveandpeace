import { defineConfig } from 'umi';
import routes from './routes'

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  fastRefresh: {},
  history: {
    type: 'browser'
  },
  // 开启dva配置
  dva: {
    immer: true,
    hmr: true,
  },
});
