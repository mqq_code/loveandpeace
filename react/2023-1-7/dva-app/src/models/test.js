
export default {

  namespace: 'test',

  state: {
    testTitle: '测试'
  },

  reducers: {
    changeTitle(state, action) {
      return {...state, testTitle: action.payload}
    }
  },


  effects: {
  },


  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

};
