import { getBanner } from '../services'

export default {

  namespace: 'user',

  // 存数据
  state: {
    username: '王小明',
    age: 20,
    banners: [],
    clickNum: 0
  },

  // 修改state数据
  reducers: {
    setAge(state, action) {
      return {...state, age: state.age + action.payload}
    },
    setBanner(state, action) {
      return {...state, banners: action.payload}
    },
    setClickNum(state, action) {
      return {...state, clickNum: state.clickNum + 1}
    }
  },


  // 处理仓库中的异步
  effects: {
    *getBanner(action, { call, put }) {
      // 通过saga请求接口
      const res = yield call(getBanner)

      // 调用reducer函数修改state数据
      yield put({
        type: 'setBanner',
        payload: res.data.banners
      })
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      document.addEventListener('click', () => {
        dispatch({
          type: 'setClickNum'
        })
      })
    },
  },

};
