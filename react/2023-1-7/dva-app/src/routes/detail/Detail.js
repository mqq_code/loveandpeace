import React, { useEffect } from 'react'
import { connect } from 'dva'

const Detail = (props) => {

  const change = (e) => {
    props.dispatch({
      type: 'test/changeTitle',
      payload: e.target.value
    })
  }

  useEffect(() => {
    props.dispatch({
      type: 'user/getBanner',
      payload: 'aaaaaaa'
    })
  }, [])

  return (
    <div>
      <h1>页面被点击了{props.clickNum}次</h1>
      <hr />
      <h2>{props.testTitle}</h2>
      <input type="text" value={props.testTitle} onChange={change} />
      <p>姓名: {props.username}</p>
      <p>姓名: {props.age}
        <button onClick={() => {
          props.dispatch({
            type: 'user/setAge',
            payload: 1
          })
        }}>+</button>
      </p>
      <ul>
        {props.banners.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width="300" alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}


const mapState = state => {
  return {
    testTitle: state.test.testTitle,
    username: state.user.username,
    age: state.user.age,
    banners: state.user.banners,
    clickNum: state.user.clickNum
  }
}
export default connect(mapState)(Detail)