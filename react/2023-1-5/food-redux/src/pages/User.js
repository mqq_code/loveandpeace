import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const User = () => {
  const songs = useSelector(s => s.songs)
  const dispatch = useDispatch()

  useEffect(() => {
    // dispatch({
    //   type: 'GET_API_SONGS'
    // })
  }, [])

  return (
    <div>
      <ul>
        {JSON.stringify(songs)}
      </ul>
    </div>
  )
}

export default User