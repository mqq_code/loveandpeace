import { takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'


// 获取轮播图
function* getBanners() {
  // 发送异步请求
  const res = yield call(axios.get, 'https://zyxcl-music-api.vercel.app/banner')
  // 获取到数据，发起 action 执行 reducer 函数
  yield put({
    type: 'SET_LIST',
    payload: res.data.banners
  })

  yield put({
    type: 'GET_API_SONGS'
  })
}

// 获取轮播图
function* getSongs() {
  // 发送异步请求
  const res = yield call(axios.get, 'https://zyxcl-music-api.vercel.app/search?keywords=%E6%B5%B7%E9%98%94%E5%A4%A9%E7%A9%BA')
  console.log(res.data.result.songs)
  // 获取到数据，发起 action 执行 reducer 函数
  yield put({
    type: 'SET_SONGS',
    payload: res.data.result.songs
  })
}


function* saga () {
  yield takeEvery('GET_BANNERS', getBanners)
  yield takeEvery('GET_API_SONGS', getSongs)
}

export default saga