import { createStore, applyMiddleware } from 'redux'
// 发起 dispatch 时自动打印仓库数据
import logger from 'redux-logger'
import createSagaMiddleWare from 'redux-saga'
import saga from './saga'

const init = {
  list: [], // banners
  songs: [], // 搜索结果
  name: '',
  age: 100,
  sex: '男',
  goods: []
}

const reducer = (state = init, action) => {
  if (action.type === 'SET_LIST') {
    return { ...state, list: action.payload }
  } else if (action.type === 'SET_SONGS') {
    return { ...state, songs: action.payload }
  } else if (action.type === 'SET_NAME') {
    return { ...state, name: action.payload }
  } else if (action.type === 'SET_AGE') {
    return { ...state, age: action.payload }
  } else if (action.type === 'SET_SEX') {
    return { ...state, sex: action.payload }
  } else if (action.type === 'SET_GOODS') {
    return { ...state, goods: action.payload }
  }
  return state
}

// 创建saga中间件
const sagaMiddleWare = createSagaMiddleWare()

const store = createStore(reducer, applyMiddleware(sagaMiddleWare, logger))

sagaMiddleWare.run(saga)

export default store