import React, { useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Home = () => {
  const list = useSelector(s => s.list)
  const dispatch = useDispatch()
  const inp = useRef()
  const unshfit = () => {
    dispatch({
      type: 'UNSHIFT_LIST',
      payload: Number(inp.current.value)
    })
    inp.current.value = ''
  }
  const push = () => {
    dispatch({
      type: 'PUSH_LIST',
      payload: Number(inp.current.value)
    })
    inp.current.value = ''
  }
  const sort1 = () => {
    dispatch({
      type: 'ascending_sort'
    })
  }
  const sort2 = () => {
    dispatch({
      type: 'descending_sort'
    })
  }
  const remove = id => {
    dispatch({
      type: 'REMOVE_ITEM',
      payload: id
    })
  }

  return (
    <div>
      <input type="number" placeholder="请输入要添加的内容" ref={inp} />
      <button onClick={unshfit}>向前添加</button>
      <button onClick={push}>向后添加</button>
      <button onClick={sort1}>升序</button>
      <button onClick={sort2}>降序</button>
      <ul>
        {list.map(item =>
          <li key={item.id}>
            {item.title} <button onClick={() => remove(item.id)}>删除</button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home