import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

const User = () => {
  const user = useSelector(s => {
    console.log(s)
    return s.user
  })
  const list = useSelector(s => s.list)
  const dispatch = useDispatch()


  const addAge = () => {
    dispatch({
      type: 'ADD_AGE'
    })
  }
  const subAge = () => {
    dispatch({
      type: 'SUB_AGE'
    })
  }

  return (
    <div>
      <div>
        age:  <button onClick={subAge}>-</button> {user.age} <button onClick={addAge}>+</button>
      </div>
      {JSON.stringify(user)}
      <hr />
      {JSON.stringify(list)}
      <hr />
      {list[1].title * user.age}
    </div>
  )
}

export default User