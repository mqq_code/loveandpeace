const init = [
  {
    id: 0,
    title: 100
  },
  {
    id: 1,
    title: 50
  }
]

const controller = {
  UNSHIFT_LIST(state, action) {
    return [
      {
        id: Date.now(),
        title: action.payload
      },
      ...state
    ]
  },
  PUSH_LIST(state, action) {
    return [
      ...state,
      {
        id: Date.now(),
        title: action.payload
      }
    ]
  },
  ascending_sort(state, action) {
    const list = [...state]
    list.sort((a, b) => a.title - b.title)
    return list
  },
  descending_sort(state, action) {
    const list = [...state]
    list.sort((a, b) => b.title - a.title)
    return list
  },
  REMOVE_ITEM(state, action) {
    return state.filter(v => v.id !== action.payload)
  },
  ADD_AGE(state, action) {
    const list = [...state]
    list[1].title += 1
    return list
  }
}

const list = (state = init, action) => {
  if (controller[action.type]) {
    return controller[action.type](state, action)
  }
  return state
}

export default list