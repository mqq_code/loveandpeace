import { combineReducers } from 'redux'
import list from './list'
import user from './user'

// 合并reducer
export default combineReducers({
  user,
  list
})
