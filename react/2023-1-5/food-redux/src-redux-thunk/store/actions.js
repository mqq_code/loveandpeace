import axios from 'axios'


// 存仓库用到的所有的 action 类型
export const SET_LIST = 'SET_LIST' 
export const SET_NAME = 'SET_NAME'
export const SET_AGE = 'SET_AGE'
export const SET_SEX = 'SET_SEX'
export const SET_GOODS = 'SET_GOODS'


export const setListAction = (dispatch) => {
  axios.get('https://zyxcl-music-api.vercel.app/banner').then(res => {
    dispatch({
      type: SET_LIST,
      payload: res.data.banners
    })
  })
}

export const setNameAction = (payload) => {
  return {
    type: SET_NAME,
    payload
  }
}