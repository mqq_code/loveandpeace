import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setListAction, setNameAction } from '../store/actions'

const User = () => {
  const list = useSelector(s => s.list)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setListAction)
    dispatch(setNameAction(' AAAAAAAAAA'))
  }, [])

  return (
    <div>
      <ul>
        {list.map(item =>
          <li key={item.targetId}>
            {item.typeTitle}
          </li>
        )}
      </ul>
    </div>
  )
}

export default User