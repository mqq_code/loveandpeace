import React from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import './App.scss'

const App = () => {
  return (
    <div className='app'>
      <Child1 />
      <Child2 />
    </div>
  )
}

export default App