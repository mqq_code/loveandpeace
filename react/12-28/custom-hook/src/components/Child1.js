import React, { useEffect, useState } from 'react'
import usePos from '../hooks/usePos'

const Child1 = () => {
  const [num, setNum] = useState(0)
  const pos = usePos(num)

  return (
    <div className='child'>
      <h1>Child1 组件</h1>
      <h2>鼠标当前的位置：{JSON.stringify(pos)}</h2>
      <button onClick={() => setNum(num - 1)}>-</button>
      {num}
      <button onClick={() => setNum(num + 1)}>+</button>
    </div>
  )
}

export default Child1