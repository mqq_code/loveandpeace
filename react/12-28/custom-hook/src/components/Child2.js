import React, { useState, useEffect, useMemo, useRef } from 'react'
import usePos from '../hooks/usePos'

const Child2 = () => {
  const wrap = useRef(null)

  const pos = usePos()

  const boxStyle = useMemo(() => {
    let left = pos.x - 50
    let top = pos.y - 50
    const { width, top: top1, bottom } = wrap.current ? wrap.current.getBoundingClientRect() : {}
    if (left <= 0) left = 0
    if (left >= width - 100) left = width - 100
    if (top <= top1) top = top1
    if (top >= bottom - 100) top = bottom - 100
    return {
      left,
      top
    }
  }, [pos])

  return (
    <div className='child' ref={wrap}>
      <h1>Child2</h1>
      <div className="box" style={boxStyle}></div>
    </div>
  )
}

export default Child2