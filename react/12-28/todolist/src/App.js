import React, { useState, useMemo, useEffect, useLayoutEffect, useCallback } from 'react'
import './App.scss'
import Header from './components/header/Header'
import Form from './components/form/Form'
import List from './components/list/List'
import Footer from './components/footer/Footer'

const App = () => {
  const [status, setStatus] = useState(0) // 0: 全部，1: 进行中，2: 已完成
  const [list, setList] = useState(JSON.parse(localStorage.getItem('list')) || [])
  // 添加
  const add = useCallback(text => {
    setList(prevList => [
      ...prevList,
      {
        id: Date.now(),
        checked: false,
        text
      }
    ])
  }, [])
  // 修改选中
  const changeChecked = useCallback(id => {
    setList(prevList => prevList.map(item => {
      if (item.id === id) {
        return {...item, checked: !item.checked}
      }
      return item
    }))
  }, [])
  // 删除数据
  const remove = useCallback(id => {
    setList(prevList => prevList.filter(v => v.id !== id))
  }, [])
  // 清除已完成
  const clearDone = useCallback(() => {
    setList(prevList => prevList.filter(v => !v.checked))
  }, [])
  // 监听列表修改存到本地
  useEffect(() => {
    localStorage.setItem('list', JSON.stringify(list))
  }, [list])
  // 计算已完成和未完成数量
  const listCount = useMemo(() => {
    const doneCount = list.filter(v => v.checked).length
    return {
      done: doneCount,
      todo: list.length - doneCount
    }
  }, [list])

  useLayoutEffect(() => {
    if (listCount.done === 0 || listCount.todo === 0) {
      setStatus(0)
    }
  }, [listCount])

  // 拷贝渲染的列表
  const renderList = useMemo(() => {
    if (status === 0) return list
    return list.filter(v => status === 1 ? !v.checked : v.checked)
  }, [list, status])

  return (
    <div className="app">
      <Header />
      <Form add={add} />
      {list.length > 0 ?
        <>
          <List list={renderList} onChange={changeChecked} onDel={remove} />
          <Footer status={status} {...listCount} onClear={clearDone} onChange={setStatus} />
        </>
      : <div className="empty-todos">Congrat, you have no more tasks to do</div>}
    </div>
  )
}

export default App