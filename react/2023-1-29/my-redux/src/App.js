import React from 'react'
import { connect, useSelector, useDispatch } from 'react-redux'


// const App = (props) => {
//   const num = useSelector(state => state.num)
//   const dispatch = useDispatch()
//   return (
//     <div>
//       {num}
//       <button onClick={() => {
//         dispatch({
//           type: 'ADD',
//           payload: 2
//         })
//       }}>+</button>
//     </div>
//   )
// }
// export default App

const App = (props) => {
  console.log(props)
  return (
    <div>
      {props.num}
      <button onClick={() => props.add()}>+</button>
    </div>
  )
}

const mapState = (state) => {
  return state
}
const mapDispatch = (dispatch) => {
  return {
    add() {
      dispatch({
        type: 'ADD',
        payload: 2
      })
    }
  }
}
export default connect(mapState, mapDispatch)(App)
