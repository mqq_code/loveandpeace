import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import axios from 'axios'

export interface IndexModelState {
  list: any[];
  name: string;
}

export interface IndexModelType {
  namespace: 'user';
  state: IndexModelState;
  effects: {
    getBanner: Effect;
  };
  reducers: {
    // changename: Reducer<IndexModelState>;
    // setlist: Reducer<IndexModelState>;
    // 启用 immer 之后
    changename: ImmerReducer<IndexModelState>;
    setlist: ImmerReducer<IndexModelState>;
  };
  subscriptions: { setup: Subscription };
}

const IndexModel: IndexModelType = {
  namespace: 'user',

  state: {
    list: [],
    name: '晓明'
  },

  reducers: {
    changename(state, action) {
      state.name = action.payload
    },
    setlist(state, action) {
      state.list = action.payload
    }
  },

  effects: {
    *getBanner({ payload }, { call, put }) {
      // 使用 call 调用异步方法
      const res = yield call(axios.get, 'https://zyxcl-music-api.vercel.app/banner')
      // 获取到结果使用 put 调用reducer函数
      yield put({
        type: 'setlist',
        payload: res.data.banners
      })
    },
  },


  subscriptions: {
    setup({ dispatch, history }) {
    },
  },
};

export default IndexModel;