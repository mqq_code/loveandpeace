import React, { memo } from 'react'

interface IProps {
  num: number;
  fn: (n: number) => void;
}

const Child: React.FC<IProps> = (props) => {

  console.log('child更新了', props.fn)

  return (
    <div>
      <h2>Child</h2>
      {props.num}
    </div>
  )
}

// memo: 优化性能的高阶组件
export default memo(Child)