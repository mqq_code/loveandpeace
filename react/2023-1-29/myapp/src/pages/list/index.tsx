import React, { useEffect } from 'react'
import { useSelector, connect, useDispatch } from 'umi'


const index = (props: any) => {
  // useSelector 获取仓库数据
  const user = useSelector((state: any) => state.user)
  const dispatch = useDispatch()

  // console.log(user)
  // console.log(props)

  const changeName = (e: any) => {
    dispatch({
      type: 'user/changename',
      payload: e.target.value
    })
  }

  useEffect(() => {
    dispatch({
      type: 'user/getBanner'
    })
  }, [])

  return (
    <div>
      <input type="text" value={user.name} onChange={changeName} />
      <h2>name: {user.name}</h2>
      <p>list: {JSON.stringify(user.list)}</p>
    </div>
  )
}

const mapState = (state: any) => {
  return {
    user: state.user
  }
}
// connect 链接仓库获取数据传给组件的props
export default connect(mapState)(index)