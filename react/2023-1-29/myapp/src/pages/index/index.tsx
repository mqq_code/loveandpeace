import React, { Component, createRef } from 'react'

interface Item {
  id: number;
  text: string;
}

interface IState {
  list: Item[];
  inp: string;
  count: number;
}

class Index extends Component<{}, IState> {

  state = {
    list: [] as Item[],
    inp: '',
    count: 0
  }
  changeInp = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      inp: e.target.value
    })
  }

  addList = () => {
    // setState 会把传入的对象和之前的对象进行合并
    // React v18 之前在生命周期和合成事件中是异步执行，在原生事件和setTimeout中是同步执行的，v18之后都是异步执行
    // 当更新页面需要依赖最新的state时使用函数获取最新值
    this.setState({
      list: [...this.state.list, {
        id: Date.now(),
        text: this.state.inp
      }],
      inp: ''
    })

    console.log(this.inpRef.current)
  }

  keydonw = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.keyCode === 13) {
      this.addList()
    }
  }

  addCount = () => {
    // 当更新页面需要依赖最新的state时使用函数获取最新值
    this.setState(state => {
      console.log('state1', state.count)
      return { count: state.count + 1 }
    })
    this.setState(state => {
      console.log('state2', state.count)
      return { count: state.count + 1 }
    })
    this.setState(state => {
      console.log('state3', state.count)
      return { count: state.count + 1 }
    }, () => {
      console.log('页面更新后的回调函数', this.state.count)
    })
    console.log('state0', this.state.count)
  }

  inpRef = createRef<HTMLInputElement>()

  render() {
    const { inp, list, count } = this.state
    console.log(this.props)
    return (
      <div>
        <header>
          <input type="text" ref={this.inpRef} value={inp} onChange={this.changeInp} onKeyDown={this.keydonw} />
          <button onClick={this.addList}>add</button>
        </header>
        <ul>
          {list.map(item =>
            <li key={item.id}>{item.text}</li>
          )}
        </ul>
        <hr />
        <button onClick={this.addCount}>+</button> {count}
      </div>
    )
  }
}

export default Index
