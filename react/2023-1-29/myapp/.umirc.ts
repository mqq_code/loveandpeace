import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/index/index' },
    { path: '/detail', component: '@/pages/detail/index' },
    { path: '/list', component: '@/pages/list/index' }
  ],
  fastRefresh: {},
  dva: {
    immer: true,
    hmr: false,
  },
});
