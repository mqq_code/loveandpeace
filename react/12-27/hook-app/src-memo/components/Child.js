import { useState, memo } from 'react'

const Child = (props) => {
  const [flag, setFlag] = useState(false)
  console.log('child更新了', props.obj)
  return (
    <div>
      <h3>Child 组件 - age: {props.obj.info.age}</h3>
      <button onClick={() => setFlag(!flag)}>{flag ? '显示' : '隐藏'}</button>
    </div>
  )
}

// memo: 高阶组件
// 作用: 浅比较组件中的props是否有更新
export default memo(Child, (prevProps, props) => {
  // console.log('prevProps', prevProps.obj.info)
  // console.log('props', props.obj.info)
  if (prevProps.obj.info.age !== props.obj.info.age) {
    // 更新前和更新后数据发生改变，组件更新，不缓存
    return false
  }
  // 更新前和更新后数据没有变化，组件不更新，缓存组件
  return true
})