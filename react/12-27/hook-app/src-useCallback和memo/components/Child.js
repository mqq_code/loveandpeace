import { useState, memo } from 'react'

const Child = (props) => {
  const [flag, setFlag] = useState(false)

  console.log('Child组件更新了')

  return (
    <div>
      <h3>Child 组件 - {props.num}</h3>
      <button onClick={() => setFlag(!flag)}>{flag ? '显示' : '隐藏'}</button>
    </div>
  )
}

// memo: 高阶组件
// 作用: 浅比较组件中的props是否有更新
export default memo(Child)