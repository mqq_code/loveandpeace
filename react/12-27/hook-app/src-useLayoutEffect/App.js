import React, { useState, useEffect, useLayoutEffect } from 'react'



const App = () => {

  const [num, setNum] = useState(-1)

  // 页面渲染后执行
  useEffect(() => {
    if (num === '000000000000000000000000000000000000000000000000000000000000') {
      setNum(Math.random())
    }
  }, [num])

  // dom更新后，页面渲染前执行
  // useLayoutEffect(() => {
  //   if (num === '000000000000000000000000000000000000000000000000000000000000') {
  //     setNum(Math.random())
  //   }
  // }, [num])


  return (
    <div>
      <h1>App</h1>
      <button onClick={() => setNum('000000000000000000000000000000000000000000000000000000000000')}>修改num</button>
      {num}
    </div>
  )
}

export default App