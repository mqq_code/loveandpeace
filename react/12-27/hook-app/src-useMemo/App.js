import { useMemo, useState } from 'react'
import './App.css'

const goodsList = [
  {
    title: '苹果',
    price: 20,
    count: 0
  },
  {
    title: '香蕉',
    price: 22,
    count: 0
  },
  {
    title: '梨',
    price: 10,
    count: 0
  },
  {
    title: '菠萝',
    price: 14,
    count: 0
  },
  {
    title: '榴莲',
    price: 50,
    count: 0
  },
  {
    title: '车厘子',
    price: 90,
    count: 0
  }
]

function App() {
  const [title, setTitle] = useState('标题')
  const [list, setList] = useState(goodsList)
  const changeCount = (index, n) => {
    const list1 = [...list]
    list1[index].count += n
    setList(list1)
  }

  const getTotal = () => {
    let total = 0
    list.forEach(item => {
      total += item.count * item.price
      console.log('getTotal')
    })
    return total
  }

  // 类似 vue 中的计算属性，返回一个缓存的值，依赖项更新自动重新计算，否则从缓存中读取
  const total = useMemo(() => {
    let total = 0
    list.forEach(item => {
      total += item.count * item.price
      console.log('useMemo')
    })
    return total
  }, [list])

  return (
    <div className="App">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <ul>
        {list.map((item, index) =>
          <li key={item.title}>
            <h2>{item.title}</h2>
            <p>价格：¥{item.price}</p>
            <button onClick={() => changeCount(index, -1)}>-</button>
            {item.count}
            <button onClick={() => changeCount(index, 1)}>+</button>
          </li>
        )}
      </ul>
      <footer>总价：¥{getTotal()}</footer>
      <footer>总价: ¥{total}</footer>
    </div>
  );
}

export default App;
