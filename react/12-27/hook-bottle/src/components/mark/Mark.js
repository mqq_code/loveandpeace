import React, { useState } from 'react'
import classNames from 'classnames'
import style from './index.module.scss'

const Mark = (props) => {
  const [out, setOut] = useState(false)

  const close = () => {
    setOut(true)
  }
  const end = () => {
    if (out) {
      props.close()
    }
  }

  return (
    <div className={style.mark}>
      {props.list.map(item =>
        <div key={item.title} className={classNames(style.mark_item, { [style.out]: out })} onAnimationEnd={end}>
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.alcohol}</p>
          <p>{item.price}</p>
          <p>{item.region}</p>
          <p>{item.varietal}</p>
          <p>{item.year}</p>
          <div className={style.btn}><i className='iconfont icon-gouwucheman'></i>add to cart</div>
        </div>
      )}
      {!out && <div className={classNames("iconfont icon-icon_close", style.close)} onClick={close}></div>}
    </div>
  )
}

export default Mark