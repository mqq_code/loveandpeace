import React, { Component, useState, useEffect } from 'react'
import './App.scss'
import axios from 'axios'
import Header from './components/header/Header'
import Mark from './components/mark/Mark'
import Bottle from './components/bottle/Bottle'

const App = () => {
  const [list, setList] = useState([]) // 存所有数据
  const [headerList, setHeaderList] = useState([]) // 选中的数据
  const [showMark, setShowMark] = useState(false) // 显示遮罩

  const getList = async () => {
    const res = await axios.get('/list')
    setList(res.data)
  }
  const change = (title) => {
    let headerList1 = [...headerList]
    const item = list.find(v => v.title === title) // 根据title查找当前点击的对象
    // 选中的数量大于等于3并且当前点击的瓶子还未选中就停止执行
    if (headerList.length >= 3 && !item.active) {
      return
    }
    item.active = !item.active // 修改当前高亮
    if (item.active) { // 如果选中push到header
      headerList1.push(item)
    } else { // 如果取消选中，就从header中删除
      headerList1 = headerList1.filter(v => v.title !== title)
    }
    // 更新页面
    setList([...list])
    setHeaderList(headerList1)
  }

  useEffect(() => {
    getList()
  }, [])

  return (
    <div className="wrap">
      <main>
        {list.map(item =>
          <Bottle key={item.title} {...item} change={change} />
        )}
      </main>
      {headerList.length > 0 && <Header list={headerList} remove={change} compare={setShowMark} />}
      {showMark && <Mark list={headerList} close={() => setShowMark(false)} />}
    </div>
  )
}

export default App
