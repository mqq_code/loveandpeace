import React, { useState, useEffect } from 'react'
import style from './keyboard.module.scss'
import classNames from 'classnames'


const Keyboard = ({ list, changeMusic, flag }) => {
  const [curId, setCurId] = useState('') // 当前高亮按钮的id

  useEffect(() => {
    const keydown = (e) => {
      // 去所有列表中查找是否存在当前按下的keyCode
      const item = list.find(v => v.keyCode === e.keyCode)
      if (item) {
        changeMusic(item)
        setCurId(item.id)
      }
    }
    const keyup = () => {
      setCurId('')
    }

    document.addEventListener('keydown', keydown)
    document.addEventListener('keyup', keyup)
    return () => {
      document.removeEventListener('keydown', keydown)
      document.removeEventListener('keyup', keyup)
    }
  }, [list])


  return (
    <div className={style.keyboard}>
      {list.map(item =>
        <p
          key={item.id}
          className={classNames({
            [style.active]: curId === item.id, // 高亮
            [style.off]: !flag // 关闭后的高亮
          })}
          onMouseDown={() => {
            setCurId(item.id)
            changeMusic(item)
          }}
          onMouseUp={() => setCurId('')}
        >{item.keyTrigger}</p>
      )}
    </div>
  )
}

export default Keyboard
