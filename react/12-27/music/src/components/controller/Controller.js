import React, { useRef, useState, useEffect } from 'react'
import style from './controller.module.scss'
import classNames from 'classnames'

const Controller = ({ curMusic, flag, changeFlag, bank, changeBank, changeVolume }) => {
  const [left, setLeft] = useState('100%')
  const volume = useRef(null)
  const isDown = useRef(false)

  const mousedown = () => {
    isDown.current = true
  }
  
  useEffect(() => {
    const mouseup = () => {
      isDown.current = false
    }
    const mousemove = (e) => {
      if (isDown.current) {
        const rect = volume.current.getBoundingClientRect()
        const max = rect.width - 12
        let left = e.clientX - rect.left - 6
        if (left <= 0) {
          left = 0
        } else if (left >= max){
          left = max
        }
        setLeft(left)
        changeVolume(left / max)
      }
    }
    document.addEventListener('mousemove', mousemove)
    document.addEventListener('mouseup', mouseup)
    return () => {
      document.removeEventListener('mousemove', mousemove)
      document.removeEventListener('mouseup', mouseup)
    }
  }, [])

  return (
    <div className={style.controller}>
      <div className={style.power}>
        <b>power</b>
        <p className={classNames({ [style.on]: flag })} onClick={changeFlag}></p>
      </div>
      <div className={style.song_id}>{curMusic.id}</div>
      <div className={style.volume} ref={volume}>
        <i style={{ left }} onMouseDown={mousedown}></i>
      </div>
      <div className={style.power}>
        <b>bank</b>
        <p className={classNames({ [style.on]: bank })} onClick={changeBank}></p>
      </div>
    </div>
  )
}

export default Controller
