import React from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import Child3 from './components/Child3'


class App extends React.Component {

  state = {
    num: 0
  }
  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  obj = {
    a: 100,
    b: 200,
    c: 300,
    d: 400,
    e: 500,
    f: 600,
    g: 700
  }

  render() {
    const { num } = this.state
    return (
      <div>
        <h1>App组件</h1>
        <button onClick={() => this.changeNum(-1)}>-</button>
        {num}
        <button onClick={() => this.changeNum(1)}>+</button>
        <hr />
        {/* {...this.obj} 把obj的所有属性都传给Child1  */}
        <Child1
          num={num}
          title="类组件"
          changeNum={this.changeNum}
          {...this.obj}
          arr={[1,2,3,5]}
          left={<div style={{background: 'red'}}>left</div>}
          right={<div style={{background: 'orange', fontSize: '50px'}}>right</div>}
          btm={<Child2 num={num} title="函数组件1" changeNum={this.changeNum} />}
        >
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
            <li>6</li>
            <li>7</li>
            <li>8</li>
            <li>9</li>
            <li>10</li>
          </ul>
        </Child1>
        <hr />
        <Child2 num={num} title="函数组件1" changeNum={this.changeNum} />
        <hr />
        <Child3 num={num} title="函数组件2" changeNum={this.changeNum}>
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
            <li>6</li>
            <li>7</li>
            <li>8</li>
            <li>9</li>
            <li>10</li>
          </ul>
          <b>bbbbbbb</b>
          <p>ppppppp</p>
        </Child3>
      </div>
    )
  }

}
export default App
