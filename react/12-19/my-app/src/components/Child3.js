import { Children } from 'react'

console.log(Children)

const Child3 = ({ num, title, changeNum, children }) => {
  // props: 接收父组件传过来的参数
  console.log(Children.count(children))
  console.log(Children.toArray(children))
  // console.log(Children.only(children)) // 限制子元素数量，有且只能有一个子元素，否则报错
  return (
    <div>
      <h2>Child3 - {title}</h2>
      <button onClick={() => changeNum(-3)}>-</button>
      {num}
      <button onClick={() => changeNum(3)}>+</button>
      <div>
        {Children.map(children, item =>
          <div key={item.type} style={{ border: '5px solid red' }}>
            <h3>{item.type}</h3>
            {item}
          </div>
        )}
      </div>
    </div>
  )
}

export default Child3