

function Child2({ num, title, changeNum }) {
  // props: 接收父组件传过来的参数
  return (
    <div>
      <h2>Child2 - {title}</h2>
      <button onClick={() => changeNum(-2)}>-</button>
      {num}
      <button onClick={() => changeNum(2)}>+</button>
    </div>
  )
}

export default Child2