import React from 'react'; // react核心语法，例如：创建虚拟dom、创建组件、hook
import ReactDOM from 'react-dom/client'; // dom操作相关：渲染虚拟dom到页面

// const box = React.createElement('div', {}, ['开始学习react'])
// console.log(box)

// jsx: js + xml，js中可以使用xml语法，React.createElement 的语法糖，
// 通过 babel 转译成 React.createElement
// const box = <div>开始学习react</div>
// console.log(box)

const title = 'react - jsx'
const box1 = 'box111111'
const classs = ['box', box1].join(' ')
const getSum = (a, b) => a + b
const renderBox = (n) => {
  if (n > 90) {
    // style只能传入对象
    return <b style={{ color: 'green' }}>及格了</b>
  }
  return <i style={{ color: 'red' }}>不及格</i>
}

const arr = ['a', 'b', 'c', 'd']
const arr1 = [
  <li key='a'>a</li>,
  <li key='b'>b</li>,
  <li key='c'>c</li>,
  <li key='d'>d</li>
]
const list = [
  {
    title: '徐良',
    songs: ['客官不可以', '坏女孩', '红妆']
  },
  {
    title: '许嵩',
    songs: ['断桥残雪', '灰色头像', '素颜']
  },
  {
    title: '汪苏泷',
    songs: ['不分手的恋爱', '有点甜', '一笑倾城']
  }
]
const num = 60
const renderNum = (n) => {
  if (n < 60) {
    return <div>小于60分了</div>
  } else if (n >= 60 && n < 90) {
    return <div>及格了</div>
  }
  return <div>90分以上了</div>
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div className={classs}>
    <h1>{title}</h1>
    <div>string: {'字符串'}</div>
    <div>number: {100}</div>
    <div>boolean: {true}</div>
    <div>null: {null}</div>
    <div>undefined: {undefined}</div>
    <div>调用函数: {getSum(1, 2)}</div>
    {renderBox(80)}
    <div>数组：{[1,2,3,4,5,6,7,8]}</div>
    {/* jsx中不可以直接渲染对象，可以转成字符串再渲染 */}
    <div>对象:{JSON.stringify({ name: '123' })}</div>
    <hr/>
    <h2>数组渲染</h2>
    <ol>
      {arr.map(item => <li key={item}>{item}</li>)}
    </ol>
    <ul>
      {arr1}
    </ul>
    <div>
      {list.map(item =>
        <dl key={item.title}>
          <dt>{item.title}</dt>
          <dd>
            {item.songs.map(val => <b style={{ marginRight: '20px' }} key={val}>{val}</b>)}
          </dd>
        </dl>
      )}
    </div>
    <hr/>
    <h2>条件判断</h2>
    <div>三元表达式: 
      {num > 90 ?
        <div>
          <b>我及格了</b>
          <p>aaaaa</p>
          <dl>
            <dt>dtdt</dt>
            <dd>dddddd</dd>
          </dl>
        </div>
      : <i>我没及格</i>}
    </div>
    <div>函数中if判断:{renderNum(num)}</div>
    <div>使用 &&: {num > 90 && <div>90分够了</div>}</div>
  </div>
);




// 链表
// let obj = {
//   val: 1,
//   next: {
//     val: 2,
//     next: {
//       val: 3
//     }
//   } 
// }
// 二叉树
// let obj = {
//   val: 1,
//   left: {
//     val: 2,
//     left: {

//     },
//     right: {

//     }
//   },
//   right: {
//     val: 2,
//     left: {
//       val: 2,
//       left: {

//       },
//       right: {
        
//       }
//     },
//     right: {
      
//     }
//   }
// }






