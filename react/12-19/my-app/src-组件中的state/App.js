import React from 'react'

// 类组件, react 16.8 版本之前主要使用类组件，可以定义组件的状态和生命周期
class App extends React.Component {
  // 存组件数据，可以调用个setState修改数据，更新页面
  state = {
    num: 10
  }

  // 实例属性，可以存数据，但是改变不会触发页面更新，最好存放跟组件更新无关的数据
  abc = '测试标题'
  changeAbc = () => {
    // 修改实例属性，组件不会更新
    this.abc = 'abc'
    console.log('修改abc', this.abc)
    // 可以调用此方法进行更新，this.setState({})
    // this.forceUpdate() // 强制更新
  }

  changeNum = (n) => {
    console.log('changeNum 执行了', n)
    // react 中更新页面必须手动调用 this.setState 触发更新
    // 把传入的对象和原本的state进行合并
    this.setState({
      num: this.state.num + n
    })
  }

  renderBtn = (num) => {
    if (num > 0) {
      return <>
        <button disabled={num <= 0} onClick={() => this.changeNum(-1)}>-</button>
        <b>{num}</b>
      </>
    }
    return null
  }

  // 类组件中必须有render函数，返回虚拟dom，调用组件或者组件数据更新时会自动调用rander函数
  render() {
    console.log('render', this)
    const { num } = this.state

    return (
      <div>
        <h1>class 组件 {this.abc}</h1>
        <button onClick={this.changeAbc}>修改标题</button>
        <div>
          {this.renderBtn(num)}
          {/* 绑定事件必须小驼峰, 给函数传参数必须包一层箭头函数 */}
          <button onClick={() => this.changeNum(1)}>+</button>
        </div>
      </div>
    )
  }

}
export default App



// 函数式组件，16.8之前主要用作纯渲染，16.8之后可以使用hooks定义组件状态，可以完全代替类组件
// import React from 'react'
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }
// export default App
