import React, { createRef } from 'react'

class App extends React.Component {

  title = null
  getTitle = el => {
    this.title = el
  }
  inp = createRef() // 创建raf对象

  render() {
    return (
      <div>
        {/* 获取dom方式一 */}
        <h1 ref={this.getTitle}>标题</h1>

        {/* 获取dom方式二 */}
        <input type="text" ref={this.inp} />
        <button onClick={() => {
          console.log(this.title)
          console.log(this.inp.current)
        }}>获取title</button>
      </div>
    )
  }

}
export default App
