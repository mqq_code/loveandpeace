import React, { createRef } from 'react'

class App extends React.Component {

  state = {
    user: '小明',
    flag: true
  }
  chaneUser = e => {
    this.setState({ user: e.target.value })
  }
  changeFlag = e => {
    this.setState({ flag: e.target.checked })
  }
  inp = createRef()
  flag = createRef()

  render() {
    const { user, flag } = this.state
    return (
      <div>
        <h2>受控组件</h2>
        {/* 受控组件 */}
        <input type="text" value={user} onChange={this.chaneUser} />
        <input type="checkbox" checked={flag} onChange={this.changeFlag} />

        <button onClick={() => {
          this.setState({ user: Math.random(), flag: !this.state.flag })
        }}>修改user</button>
        {JSON.stringify(this.state)}

        <hr />
        <h2>非受控组件</h2>
        {/* 非受控组件 */}
        <input type="text" ref={this.inp} defaultValue="默认值" />
        <input type="checkbox" ref={this.flag} defaultChecked={true} />
        
        <button onClick={() => {
          console.log(this.inp.current.value)
          console.log(this.flag.current.checked)
        }}>获取表单</button>
      </div>
    )
  }

}
export default App
