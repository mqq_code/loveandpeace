import React, { createRef } from 'react'

class App extends React.Component {

  state = {
    user: '小明',
    age: 10,
    sex: 'women',
    address: '上海',
    flag: true,
    remark: '默认',
    hobby: ['唱歌', '跳舞']
  }

  changeForm = (key, val) => {
    this.setState({ [key]: val })
  }
  list = ['唱歌', '跳舞', '打篮球']
  changeHobby = (e) => {
    const { checked, value } = e.target
    if (checked) {
      this.setState({
        hobby: [...this.state.hobby, value]
      })
    } else {
      this.setState({
        hobby: this.state.hobby.filter(v => v !== value)
      })
    }
  }

  render() {
    const { user, age, sex, address, remark, flag, hobby } = this.state
    return (
      <div>
        <div>姓名：<input type="text" value={user} onChange={(e) => this.changeForm('user', e.target.value)} /></div>
        <div>年龄：<input type="text" value={age} onChange={(e) => this.changeForm('age', e.target.value)} /></div>
        <div>性别：
          <input type="radio" name="sex" id="men" value="men" checked={sex === 'men'} onChange={(e) => this.changeForm('sex', e.target.value)} /> <label htmlFor='men'>男</label>
          <input type="radio" name="sex" id="women" value="women" checked={sex === 'women'} onChange={(e) => this.changeForm('sex', e.target.value)} /> <label htmlFor='women'>女</label>
        </div>
        <div>地址：
          <select value={address} onChange={(e) => this.changeForm('address', e.target.value)}>
            <option value="">请选择</option>
            <option value="北京">北京</option>
            <option value="上海">上海</option>
            <option value="广州">广州</option>
            <option value="深圳">深圳</option>
          </select>
        </div>
        <div>记住信息：<input type="checkbox" checked={flag} onChange={(e) => this.changeForm('flag', e.target.checked)} /></div>
        <div>备注：<textarea value={remark} onChange={(e) => this.changeForm('remark', e.target.value)}></textarea></div>
        <div>
          爱好：
            <ul>
              {this.list.map(item =>
                <li key={item}>{item} <input type="checkbox" checked={hobby.includes(item)} value={item} onChange={this.changeHobby}/></li>
              )}
            </ul>
        </div>
        <hr />
        {JSON.stringify(this.state)}
      </div>
    )
  }

}
export default App
