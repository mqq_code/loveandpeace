import React, { Component } from 'react'

class App extends Component {

  constructor(props) {
    super(props)

    // 绑定函数的this指向，始终指向组件实例
    this.add = this.add.bind(this)
  }

  state = {
    num: 0
  }

  add(){
    console.log(this)
    this.setState({ num: this.state.num + 1 })
  }

  add1(){
    console.log(this)
    this.setState({ num: this.state.num + 1 })
  }
  
  add2 = () => {
    console.log(this)
    this.setState({ num: this.state.num + 1 })
  }

  test() {
    console.log(this)
  }

  render() {
    return (
      <div>
        <h1>App - {this.state.num}</h1>
        <button onClick={this.add}>+</button>
        <button onClick={this.add1.bind(this)}>+</button>
        <button onClick={() => this.add1()}>+</button>
        <button onClick={this.add2}>+</button>
        {this.state.num}
        <hr />
        {/* 
          合成事件：
            react中的事件并不是直接绑定到具体的dom元素，
            利用事件冒泡的原理统一绑定到根元素（react18之前是 document，之后是 root 元素）,
            触发事件时根据触发的元素去事件池中调用对应的函数
        */}
        {React.createElement('button', {
          onClick: this.test
        }, ['测试按钮'])}
      </div>
    )
  }
}

export default App
