import React, { Component } from 'react'
import Child from './components/Child'

class App extends Component {

  state = {
    num: 0,
    childNum: 0
  }
  add = () => {
    this.setState({ childNum: this.state.childNum + 1 })
  }

  componentDidMount () {
    setInterval(() => {
      this.setState({ num: this.state.num + 1 })
    }, 1000)
  }

  render() {
    const { num, childNum } = this.state
    // console.log('app render')
    return (
      <div>
        <h1>App - {num}</h1>
        <button onClick={this.add}>+</button> {childNum}
        <hr />
        <Child num={this.state.childNum} />
      </div>
    )
  }
}

export default App
