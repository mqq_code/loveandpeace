import React, { Component } from 'react'
import classNames from 'classnames'
import axios from 'axios'
import './App.scss'

class App extends Component {
  state = {
    hide: true,
    out: false
  }
  toggle = () => {
    if (this.state.hide) {
      this.setState({
        hide: false,
        out: false
      })
    } else {
      this.setState({ out: true })
    }
  }
  end = () => {
    console.log('动画结束了')
    if (this.state.out) {
      this.setState({ hide: true })
    }
  }
  async componentDidMount() {
    const res = await axios.get('/api/list')
    console.log(res.data)
  }
  render() {
    const { hide, out } = this.state
    return (
      <div>
        <button onClick={this.toggle}>{hide ? 'show' : 'hide'}</button>
        <div
          className={classNames('box', 'animate__animated', {
            hide, 
            animate__backOutRight: out,
            animate__backInRight: !out
          })}
          onAnimationEnd={this.end}
        ></div>
        <div>11111111111</div>
      </div>
    )
  }
}

export default App