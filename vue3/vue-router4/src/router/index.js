import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import Home from '../views/home/Home.vue'
import Detail from '../views/detail/Detail.vue'
import Login from '../views/login/Login.vue'
import Movie from '../views/home/movie/Movie.vue'
import Cinema from '../views/home/cinema/Cinema.vue'
import Mine from '../views/home/mine/Mine.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/movie',
      children: [
        {
          path: '/movie',
          name: 'movie',
          component: Movie
        },
        {
          path: '/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/mine',
          name: 'mine',
          component: Mine
        },
      ]
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})

export default router
