import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', {
  state: () => ({
    name: '晓明',
    age: 100
  }),
  actions: {
    addAge(n) {
      this.age += n
    }
  }
})
