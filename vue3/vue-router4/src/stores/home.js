import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'

export const useHomeStore = defineStore('home', () => {
  
  const banners = ref([])

  const len = computed(() => banners.length)

  const getBanner = async () => {
    const res = await axios.get('https://zyxcl-music-api.vercel.app/banner')
    banners.value = res.data.banners
  }

  return { banners, getBanner }
})
