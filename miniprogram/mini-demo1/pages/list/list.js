// pages/list/list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [
      {
        name: '小明1',
        tel: '14122223333'
      },{
        name: '小明2',
        tel: '14122223333'
      },{
        name: '小明3',
        tel: '14122223333'
      },{
        name: '小明4',
        tel: '14122223333'
      },{
        name: '小明5',
        tel: '14122223333'
      }
    ],
    title: '默认值',
    obj: {
      name: '王小明',
      age: 20,
      sex: '男'
    }
  },

  changeName (e) {
    // e.detail.value 获取input值
    console.log(e.detail.value)
    this.setData({
      obj: { ...this.data.obj, name: e.detail.value }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})