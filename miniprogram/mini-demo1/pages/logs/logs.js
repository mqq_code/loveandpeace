// logs.js
Page({
  // 定义变量
  data: {
    title: '标题',
    num: 0,
    arr: [1, 2, 3, 4, 5, 6, 7],
    list: [
      {
        name: '晓明哥',
        hobby: ['唱歌', '跳舞']
      },
      {
        name: '小花姐',
        hobby: ['吃饭', '喝饮料']
      }
    ],
    obj: {
      name: '小明',
      age: 20
    }
  },
  add () {
    // js中使用数据 this.data
    console.log('点击了+改之前', this.data.num)
    // 小程序中修改数据需要调用 this.setData() 手动触发页面更新
    this.setData({
      num: this.data.num + 1
    })
    console.log('点击了+改之后', this.data.num)
  },
  sub () {
    console.log('点击了减号')
    this.setData({
      num: this.data.num - 1
    })
  }
})
