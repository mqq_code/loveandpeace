import request from './request'
// 轮播图
export const getBanner = () => {
  return request.get('/home/swiperdata')
}
// 导航
export const getNav = () => {
  return request.get('/home/catitems')
}
// 楼层
export const getFloor = () => {
  return request.get('/home/floordata')
}
// 分类
export const getCategories = () => {
  return request.get('/categories')
}
// 列表搜索
export const goodsSearch = (params = {}) => {
  return request.get('/goods/search', params)
}
// 商品详情
export const goodsDetail = (goods_id = '') => {
  return request.get('/goods/detail', { goods_id })
}
// 搜索商品
export const qsearch = (query = '') => {
  return request.get('/goods/qsearch', { query })
}
