// pages/list/list.js
import { goodsSearch } from '../../api/index'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    _total: null,
    _pagenum: 1
  },
  goDetail (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id,
    })
  },
  async getList () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })    
    const res = await goodsSearch({
      cid: this.cid,
      pagenum: this.data._pagenum,
      pagesize: 10
    })
    this.setData({
      list: [...this.data.list, ...res.message.goods],
      _total: res.message.total,
      _pagenum: res.message.pagenum * 1 + 1
    })
    wx.hideLoading()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log('url参数', options);
    this.cid = options.id
    this.getList()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('页面滚动到最底部了，开始加载下一页数据');
    if (this.data._total === null || this.data.list.length < this.data._total) {
      this.getList()
    } else {
      wx.showToast({
        title: '没有更多数据了',
      })
    }
  }
})