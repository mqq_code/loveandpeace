// pages/search/search.js
import { qsearch } from '../../api/index'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },

  search (e) {
    // console.log(e.detail.value)
    // 添加防抖：连续多次调用某一个函数，只执行最后一次
    if (this.timer) clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      this.start(e.detail.value)
      this.timer = null
    }, 1000)
  },

  async start (text) {
    const res = await qsearch(text)
    this.setData({
      list: res.message
    })
  },
  goDetail (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})