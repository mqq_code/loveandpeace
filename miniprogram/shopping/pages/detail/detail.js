// pages/detail/detail.js
import { goodsDetail } from '../../api/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners: [],
    price: '',
    name: '',
    desc: '',
    goodsInfo: {},
    active: false
  },
  changeCollection () {
    this.setData({
      active: !this.data.active
    })
    // 把收藏的数据存到本地
    let collection = wx.getStorageSync('collection') || []
    if (this.data.active) {
      collection.push(this.data.goodsInfo)
    } else {
      collection = collection.filter(v => v.goods_id !== this.data.goodsInfo.goods_id)
    }
    wx.setStorageSync('collection', collection)
  },
  getActive () {
    const collection = wx.getStorageSync('collection') || []
    if (collection.find(v => v.goods_id === this.id)) {
      this.setData({ active: true })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    this.id = Number(options.id)
    this.getActive()
    const res = await goodsDetail(options.id)
    this.setData({
      banners: res.message.pics,
      price: res.message.goods_price,
      name: res.message.goods_name,
      desc: res.message.goods_introduce,
      goodsInfo: res.message
    })
  },

  addCart () {
    // console.log(this.data.goodsInfo);
    // 获取本地购物车数据
    const cartlist = wx.getStorageSync('cartlist') || []
    // 判断数据中是否存在此商品
    const index = cartlist.findIndex(v => v.goods_id === this.data.goodsInfo.goods_id)
    if (index > -1) {
      cartlist[index].count++
    } else {
      cartlist.push({
        ...this.data.goodsInfo,
        count: 1,
        checked: true
      })
    }
    wx.setStorageSync('cartlist', cartlist)
    wx.showToast({
      title: '添加成功',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.data.name,
      path: "/pages/detail/detail?id=" + this.id,
      imageUrl: this.data.banners[0].pics_big
    }
  }
})