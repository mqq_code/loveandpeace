// pages/index/index.js
import {
  getBanner,
  getNav,
  getFloor
} from '../../api/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners: [],
    nav: [],
    floor: []
  },
  // 轮播图接口
  async getBanner () {
    try {
      const res = await getBanner()
      this.setData({
        banners: res.message
      })
    } catch (e) {
      console.log(e)
    }
  },
  // 导航接口
  async getNav () {
    const res = await getNav()
    this.setData({
      nav: res.message
    })
  },
  // 楼层接口
  async getFloor () {
    const res = await getFloor()
    this.setData({
      floor: res.message
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getBanner()
    this.getNav()
    this.getFloor()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})