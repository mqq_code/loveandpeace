// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },
  showtoast () {
    wx.showToast({
      title: '成功',
      icon: 'success',
      duration: 2000
    })    
  },
  navigateTo () {
    wx.navigateTo({
      url: '/pages/detail/detail?a=100&b=200&url=' + encodeURIComponent('http://www.baidu.com?form=搜索')
    })
  },
  redirectTo () {
    wx.redirectTo({
      url: '/pages/detail/detail'
    })
  },
  switchTab () {
    wx.switchTab({
      url: '/pages/movie/movie'
    })
  },
  reLaunch () {
    wx.switchTab({
      url: '/pages/movie/movie'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })    
    console.log('%conLoad ====> 页面加载', 'color: yellow;font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 可以获取页面元素信息
    console.log('%c onReady ====> 页面渲染完成', 'color: yellow;font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log('%c onShow ====> 页面显示', 'color: yellow;font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log('%c onHide ====> 页面隐藏', 'color: orange;font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('%c onHide ====> 页面销毁', 'color: orange;font-size: 30px');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (params) {
    console.log(params);
    return {
      title: 'mqq小程序',
      path: '/pages/detail/detail?a=100',
      imageUrl: '/icons/cart.png'
    }
  }
})