// components/dialog/dialog.js
import size from '../../behaviors/size.js'

Component({
  // 复用逻辑，类似 mixins
  behaviors: [size],
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多 slot 支持
  },
  // 定义组件的生命周期
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      console.log('组件加载成功');
      this.start()
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
      console.log('组件销毁');
      clearInterval(this.timer)
    },
  },
  // 组件所在页面的生命周期
  pageLifetimes: {
    show: function() {
      // 页面被展示
      console.log('页面显示');
      this.start()
    },
    hide: function() {
      // 页面被隐藏
      console.log('页面被隐藏');
      clearInterval(this.timer)
    },
    resize: function(size) {
      // 页面尺寸变化
    }
  },
  observers: {
    // 监听变量改变，可以同时监听多个变量，以逗号分隔
    'num,a'(newNum, newA) {
      console.log('num和a改变了', newNum, newA);
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    num: 0,
    a: 1
  },

  /**
   * 组件的方法列表
   */
  methods: {
    start () {
      this.timer = setInterval(() => {
        this.setData({
          num: this.data.num + 1,
          a: this.data.num + 2,
        })
        console.log('定时器', this.data.num);
      }, 1000)
    }
  }
})
