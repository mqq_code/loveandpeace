// components/info/info.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    test: {
      type: String,
      value: 1000 // 默认值
    },
    title: {
      type: String,
      value: 10000
    },
    price: {
      type: Number
    },
    count: {
      type: Number,
      // 监听数据变化
      observer: function(newVal, oldVal) {
        console.log('count改变了', newVal, oldVal);
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    change (e) {
      // console.log('info组件中的按钮', e.currentTarget.dataset);
      // 调用父组件的方法修改传入的数据
      this.triggerEvent('changeCount', {
        num: e.currentTarget.dataset.num
      })
    }
  }
})
