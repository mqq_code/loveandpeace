// components/el-tabs/el-tabs.js
Component({
  relations: {
    '../el-tab-pane/el-tab-pane': {
      type: 'child', // 关联的目标节点应为子节点
    }
  },
  methods: {
    _getAllLi: function(){
      // 获取所有子组件
      const nodes = this.getRelationNodes('../el-tab-pane/el-tab-pane')
      // 获取子组件中的数据
      const nav = nodes.map(item => item.__data__)
      // 改变第一个子组件的变量
      nodes[0].setData({
        show: true
      })
      this.setData({
        nav, // 导航
        curName: nav[0].name, // 当前高亮
        children: nodes // 所有子元素
      })
    },
    changeCurName (e) {
      const { name, index } = e.currentTarget.dataset
      this.setData({
        curName: name
      })
      this.data.children.forEach((item, i) => {
        if (index === i) {
          item.setData({
            show: true
          })
        } else {
          item.setData({
            show: false
          })
        }
      })
    }
  },
  ready: function(){
    this._getAllLi()
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    nav: [],
    children: [],
    curName: ''
  }
})
