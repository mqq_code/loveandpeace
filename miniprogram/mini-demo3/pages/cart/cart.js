// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [
      {
        title: '苹果',
        price: 10,
        count: 1
      },
      {
        title: '香蕉',
        price: 20,
        count: 0
      }
    ],
    total: 0
  },

  changeCount (e) {
    // console.log(e.detail); // 获取子组件传入的数据
    // console.log(e.currentTarget.dataset); // 绑定事件的元素上的自定义属性
    const { index } = e.currentTarget.dataset
    const { num } = e.detail
    const key = `list[${index}].count`
    this.setData({
      [key]: this.data.list[index].count + num
    })
    this.getTotal()
  },
  getTotal () {
    this.setData({
      total: this.data.list.reduce((prev, next) => {
        return prev + next.price * next.count
      }, 0)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getTotal()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})