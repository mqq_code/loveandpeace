// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    str: ''
  },
  getuserinfo () {
    wx.getUserProfile({
      desc: '获取用户名和头像',
      success: res => {
        // 存本地
        wx.setStorage({
          key: "userInfo",
          data: res
        })
        this.setData({
          userInfo: res.userInfo,
          str: JSON.stringify(res)
        })
      },
      fail: () => {
        wx.showToast({
          icon: 'error',
          title: '获取用户信息失败'
        })
      }
    })
  },
  start () {
    wx.navigateTo({
      url: '/pages/game/game',
    })
  },
  rank () {
    wx.navigateTo({
      url: '/pages/rank/rank',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 从本地获取用户信息
    wx.getStorage({
      key: 'userInfo',
      success: res => {
        this.setData({
          userInfo: res.data.userInfo
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})