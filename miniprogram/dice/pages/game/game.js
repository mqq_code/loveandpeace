// pages/game/game.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    userMoney: 1000,
    arr: [1, 2, 3],
    chipList: [1, 2, 5, 20],
    chipMoney: 0,
    tipMoney: '',
    tipClass: '',
    btns: [
      {
        text: '大',
        scale: 2
      },
      {
        text: '豹子',
        scale: 24
      },
      {
        text: '小',
        scale: 1
      }
    ]
  },

  diceStart () {
    return new Promise((resolve, reject) => {
      this.timer = setInterval(() => {
        this.setData({
          arr: this.data.arr.map(() => Math.floor(Math.random() * 6 + 1))
        })
      }, 100)
      setTimeout(() => {
        clearInterval(this.timer)
        this.timer = null
        resolve(this.data.arr)
      }, 2000)
    })
  },

  async start (e) {
    if (this.timer) return
    if (this.data.chipMoney === 0) {
      wx.showToast({
        icon: 'error',
        title: '请先下注',
      })
      return
    }
    const { info } = e.currentTarget.dataset
    const res = await this.diceStart()
    const diceRes = this.result(res)
    if (info.text === diceRes) {
      // 赢了
      this.setData({
        userMoney: this.data.userMoney + this.data.chipMoney * info.scale,
        chipMoney: 0,
        tipMoney: '+' + this.data.chipMoney * info.scale,
        tipClass: 'green'
      })
    } else {
      // 输了
      this.setData({
        userMoney: this.data.userMoney - this.data.chipMoney,
        chipMoney: 0,
        tipMoney: -this.data.chipMoney,
        tipClass: 'red'
      })
    }
    setTimeout(() => {
      this.setData({
        tipMoney: '',
        tipClass: ''
      })
    }, 1200)
  },
  result (diceArr) {
    if ([...new Set(diceArr)].length === 1) {
      return '豹子'
    }
    const total = diceArr.reduce((prev, next) => prev + next)
    if (total >= 10) {
      return '大'
    } else {
      return '小'
    }
  },

  add (e) {
    const { money } = e.target.dataset
    this.setData({
      chipMoney: this.data.chipMoney + money
    })
  },
  clearChip () {
    this.setData({
      chipMoney: 0
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 从本地获取用户信息
    wx.getStorage({
      key: 'userInfo',
      success: res => {
        this.setData({
          userInfo: res.data.userInfo
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})